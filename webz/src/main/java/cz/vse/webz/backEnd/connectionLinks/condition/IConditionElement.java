/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.condition;

import com.vaadin.ui.Panel;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Can be elements in condition
 *
 * @author Martin Kozák
 */
public interface IConditionElement {

    /**
     * Indicator that given element is still used.
     *
     * @return
     */
    public boolean isAlive();

    /**
     * This element is no longer alive
     */
    public void endLive();

    public String getID();

    public Panel getPanel();

   
}

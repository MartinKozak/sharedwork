/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.condition;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.rules.Conjunction;
import java.util.Set;

/**
 * Interface that contracts the class to use  in conditions.
 *
 * @author Martin Kozák
 */
public interface IConjunctionMaster{

   
    
    /**
     * Indicator that given element is still used.
     * @return 
     */
    public boolean isAlive();
    
     /**
     * Given master is no longer alive
     */
    public void endLive();
    
    
    
    
    public Set<Conjunction> getAllConjunctions();
   
   
    /**
     * Adds conjunction (AND) and returnd said conjuction
     * 
     * @return the list of literals in given conjunction
     *//*
    public ConditionConjunction addConjunctionToConditions();
   */
       
    /**
     * Adds selected proposition into conditionsList
     *
     * 
     * @return Literal that contains selected proposition 
     */
    public Conjunction addConjunction();
    

    /**
     * 
     *
     * @param conjuction
     */
    public void deleteConjunction(Conjunction conjuction);

    public Panel getPanel();

}

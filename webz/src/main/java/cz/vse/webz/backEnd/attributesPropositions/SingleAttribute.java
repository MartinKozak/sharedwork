/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.attributesPropositions;


import cz.vse.webz.backEnd.enums.AttributeType;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Martin Kozák
 */
public class SingleAttribute extends AbstractAttribute{
    
    private final Set<SingleProposition> propositionSet;
    
    public SingleAttribute(AttributeType type) {
        super("new numeric atribute",type);
        propositionSet = Collections.synchronizedSet(new HashSet<>());
    }
    
    public SingleProposition createProposition(){
        SingleProposition proposition = new SingleProposition(this);
        propositionSet.add(proposition);
        return proposition;
    }
    
    
    public void removeProposition(SingleProposition proposition){        
        propositionSet.remove(proposition);
    }
    
    public Set<SingleProposition> getPropositionSet(){
        return propositionSet;
    }
    
    /**
     * This element is no longer alive
     * also end live of all propositions
     */
    @Override
    public void endLive(){
        super.endLive();
        for (SingleProposition proposition: propositionSet){
            proposition.endLive();
        }
    }
    
    
    /***
     * TODO more clever function
     * @param propositionID
     * @return 
     */
    public SingleProposition getPropositionByID(String propositionID){
        
        SingleProposition desiredProposition = null;
        
        for (SingleProposition proposition : propositionSet) {
            if (proposition.getID().equalsIgnoreCase(propositionID)){
                desiredProposition = proposition;
                break;
            }
        }
        return desiredProposition;
    }
    
    
}

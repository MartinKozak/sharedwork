/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.conclusion;

import java.util.Set;

/**
 * Interface that contracts the class to use Literals in conclusions.
 *
 * @author Martin Kozák
 */
public interface IConclusionMaster{

    /**
     * Indicator that given master is still used.
     * @return 
     */
    public boolean isAlive();
    
     /**
     * Given master is no longer alive
     */
    public void endLive();
    
    /**
     * Get the ArrayList representation of conclusion
     * 
     * @return the List of all literals in conclusion form
     */
    public Set<Conclusion> getAllConclusions();
    
    /**
     * Add specific proposition into conclusion
     * 
     * @param element that is added to conclusion
     * @return Literal that contains selected proposition 
     */
    public Conclusion addConclusionElement(IConclusionElement element);
   
    
    /**
     * Delete specific Literal
     * 
     * @param conclusion that is to be deleted from conclusion
     */
    public void deleteConclusionElement(Conclusion conclusion);
    
    
}

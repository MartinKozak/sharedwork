/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.condition;

import cz.vse.webz.backEnd.enums.LiteralType;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Conjuction, that is used inside conditions of rules.
 *
 * @author Martin Kozák
 */
public class ConditionConjunction {

    private final Set<Condition> literalSet;
    private final IConditionMaster parentMaster;
    private final LiteralType type;

    public ConditionConjunction(IConditionMaster parentMaster) {
        this.parentMaster = parentMaster;
        this.type = LiteralType.condition; //Conjunction can be only in conditions
        literalSet = Collections.synchronizedSet(new HashSet<>());
    }


    /**
     * Add selected proposition into Conjunction
     *
     * @param proposition
     */
    public Condition addProposition(IConditionElement element) {
        Condition addedLiteral = new Condition(element, getParentMaster());
        literalSet.add(addedLiteral);
        return addedLiteral;
    }

    /**
     * Trys to delete selected literal.
     * 
     * @param literal 
     */
    public void deleteLiteral(Condition literal) {
        literalSet.remove(literal);
    }

    /**
     * *************************************************************************
     * GET/SETERS
     */
    /**
     * @return the literalList
     */
    public Set<Condition> getLiteralList() {
        return literalSet;
    }

    /**
     * @return the parentMaster
     */
    public IConditionMaster getParentMaster() {
        return parentMaster;
    }

}

package cz.vse.webz.backEnd.rules;

import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.IConjunctionMaster;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Complete rule - rule that have both conditions and conclusions. Applies for
 * compositional and logical rules
 *
 * @author Martin Kozák
 */
public class CompleteRule extends AbstractRule implements IConditionMaster, IConclusionMaster, IConjunctionMaster {

    private Context context;

    private final Set<Condition> conditions;
    private final Set<Conclusion> conclusions;

    private final Set<Conjunction> conjunctions;
    //private final ConditionConjunction mainDisjunction;

    private double conditionThreshold;
    /**
     * Constructor of Compositional rule.
     *
     * @param ID
     * @param type
     * @param context
     * @param comment
     */
    public CompleteRule(RuleType type) {
        super("new rule", type);
        this.context = null;
        this.conditions = Collections.synchronizedSet(new HashSet<>());
        this.conclusions = Collections.synchronizedSet(new HashSet<>());
        this.conjunctions = Collections.synchronizedSet(new HashSet<>());
        //mainDisjunction = addConjunctionToConditions();
        conditionThreshold = 0;
    }

    /**
     * @return the context
     */
    public Context getContext() {
        return context;
    }
    
    
    
    @Override
    public Set<Condition> getAllConditions() {
        return conditions;
    }
    
    /**
     *
     *
     * @return the main disjunctionArea
    public ConditionConjunction getMainConditionList() {
        return mainDisjunction;
    }
    */
    @Override
    public Condition addConditionElement(IConditionElement element) {
        Condition newCondition = new Condition(element, this);
        conditions.add(newCondition);
        return newCondition;
        
        /*
        Condition literal = mainDisjunction.addProposition(element);
        return literal;
        */
    }
    
    
    @Override
    public void deleteConditionLiteral(Condition literal) {
        conditions.remove(literal);
        /*
        conditions.stream().forEach((condition) -> {
            condition.deleteLiteral(literal);
        });
        */
    }


    
    public Set<Conjunction> getAllConjunctions() {
        return conjunctions;
    }
    
    public Conjunction addConjunction() {
        Conjunction conjuction = new Conjunction(this);
        conjunctions.add(conjuction);
        return conjuction;
    }
    
    public void deleteConjunction(Conjunction conjuction){
        conjunctions.remove(conjuction);
    }
    
    
    /*
    @Override
    public ConditionConjunction addConjunctionToConditions() {
        ConditionConjunction newList = new ConditionConjunction(this);
        conditions.add(newList);
        return newList;
    }
    */
    
    
    
    @Override
    public Set<Conclusion> getAllConclusions() {
        return conclusions;
    }

    
    @Override
    public Conclusion addConclusionElement(IConclusionElement element) {
        Conclusion newConclusion = new Conclusion(element, this);
        conclusions.add(newConclusion);
        return newConclusion;
    }

    
    @Override
    public void deleteConclusionElement(Conclusion conclusion) {
        conclusions.remove(conclusion);
    }

    


   

    
    
    
    
    
    
    
    ///************************ONLY useful for logical rule*************/
    /**
     * @return the conditionThreshold
     */
    public double getConditionThreshold() {
        return conditionThreshold;
    }

    /**
     * @param conditionThreshold the conditionThreshold to set
     */
    public void setConditionThreshold(double conditionThreshold) {
        this.conditionThreshold = conditionThreshold;
    }

}

package cz.vse.webz.backEnd.attributesPropositions;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.frontEnd.components.attrprop.SinglePropositionPanel;

/**
 * Propositions, in form of POJO
 *
 * @author Martin Kozák
 */
public class SingleProposition implements IConditionElement{

    private String ID;
    private String name; 
    private String comment;

    private final SingleAttribute parentAtribute;
    
    private boolean alive;
    
    private boolean linked;
    private SinglePropositionPanel panel;
    
    
    public SingleProposition(SingleAttribute parentAtribute) {
        this("new nominal proposition","","",parentAtribute);
    }
  
    
    public SingleProposition(String ID, String name, String comment, SingleAttribute parentAtribute) {
        this.ID = ID;
        this.name = name;        
        this.comment = comment;
        this.parentAtribute = parentAtribute;
        linked = false;
        
        alive = true;
    }

    /**
     * Indicator that given element is still used.
     *
     * @return
     */
    @Override
    public boolean isAlive(){
        return alive;
    }

    /**
     * This element is no longer alive
     */
    @Override
    public void endLive(){
        alive = false;
    }
    
    
    /***
     * Creates a link between panel and proposition.
     * 
     * @param panel 
     */
    public void linkWithPanel(SinglePropositionPanel panel) {
        if (!linked) {
            this.panel = panel;
            linked = true;
        } else {
            //error
        }
    }
    
    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @return the parentAtribute
     */
    public SingleAttribute getParentAtribute() {
        return parentAtribute;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public SinglePropositionPanel getPanel() {
        return panel;
    }
    

}
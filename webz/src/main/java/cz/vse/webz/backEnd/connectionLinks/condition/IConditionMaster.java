/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.condition;

import com.vaadin.ui.Panel;
import java.util.Set;

/**
 * Interface that contracts the class to use  in conditions.
 *
 * @author Martin Kozák
 */
public interface IConditionMaster{

   
    
    /**
     * Indicator that given element is still used.
     * @return 
     */
    public boolean isAlive();
    
     /**
     * Given master is no longer alive
     */
    public void endLive();
    
    
    
    
    public Set<Condition> getAllConditions();
   
   
    /**
     * Adds conjunction (AND) and returnd said conjuction
     * 
     * @return the list of literals in given conjunction
     *//*
    public ConditionConjunction addConjunctionToConditions();
   */
       
    /**
     * Adds selected proposition into conditionsList
     *
     * @param element that is to be added to condition
     * @param location where is said proposition
     * 
     * @return Literal that contains selected proposition 
     */
    public Condition addConditionElement(IConditionElement element);
    

    /**
     * Remove selected Literal from condition Must be literal, because
     * Proposition is too vague
     *
     * @param literal
     */
    public void deleteConditionLiteral(Condition literal);

    

}

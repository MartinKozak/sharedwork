/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd;

import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.rules.IntegrityConstraint;
import cz.vse.webz.backEnd.rules.Context;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.attributesPropositions.BinaryAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SingleAttribute;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.backEnd.rules.AprioriRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.windows.ShowXML;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

/**
 * "Master" class, each Base is represnted by instance of this class.
 *
 * @author Martin Kozák
 */
public class Base {

    /**
     * Variables
     */
    private final Globals globals;
    private final Set<AbstractAttribute> atributeSet;
    private final Set<AbstractRule> rulesSet;
    private final Set<Context> contextSet;
    private final Set<IntegrityConstraint> intConSet;

    /**
     * *
     * Public constructor
     */
    public Base() {
        this.globals = new Globals();

        this.atributeSet = Collections.synchronizedSet(new HashSet<>());
        this.rulesSet = Collections.synchronizedSet(new HashSet<>());
        this.contextSet = Collections.synchronizedSet(new HashSet<>());
        this.intConSet = Collections.synchronizedSet(new HashSet<>());
    }

    /**
     * *****************************Getters***********************************
     */
    /**
     * @return the atributeSet
     */
    public Set<AbstractAttribute> getAtributes() {
        return atributeSet;
    }

    /**
     * @return the rulesSet
     */
    public Set<AbstractRule> getRules() {
        return rulesSet;
    }

    /**
     * @return the contextSet
     */
    public Set<Context> getContexts() {
        return contextSet;
    }

    /**
     * @return the integrityConstrainsSet
     */
    public Set<IntegrityConstraint> getIntegrityConstrains() {
        return intConSet;
    }

    public Globals getGlobals() {
        return globals;
    }

    /**
     * *****************************Creates all*******************************
     */
    /**
     * Creates new atribute and adds it into Atribute collection
     *
     * @param type
     * @return
     */
    public AbstractAttribute createAtribute(AttributeType type) {
        AbstractAttribute atribute = null;
        switch (type) {
            case binary:
                atribute = new BinaryAttribute();
                break;
            case single:
                atribute = new SingleAttribute(type);
                break;
            case set:
                atribute = new SetAttribute(type);
                break;
            case numeric:
                atribute = new NumericAttribute();
                break;
        }
        atributeSet.add(atribute);
        return atribute;
    }

    /**
     * Creates new atribute and adds it into Atribute collection
     *
     * @param type
     * @return
     */
    public AbstractRule createRule(RuleType type) {
        AbstractRule rule = null;
        switch (type) {
            case compositional:
            case logical:
                rule = new CompleteRule(type);
                break;

            case apriori:
                rule = new AprioriRule();
                break;
        }
        rulesSet.add(rule);
        return rule;
    }

    /**
     *
     * @return
     */
    public Context createContext() {
        Context context = new Context();
        contextSet.add(context);
        return context;
    }

    public IntegrityConstraint createIntegrityConstraint() {
        IntegrityConstraint intCon = new IntegrityConstraint();
        intConSet.add(intCon);
        return intCon;
    }

    /**
     * *****************************Removes all*******************************
     */
    /**
     *
     * @param atribute
     */
    public void removeAtribute(AbstractAttribute atribute) {
        atributeSet.remove(atribute);
        atribute.endLive();
        Condition.checkConditions();
        Conclusion.checkConclusions();
    }

    public void removeRule(AbstractRule rule) {
        rulesSet.remove(rule);
        rule.endLive();
        Condition.checkConditions();
        Conclusion.checkConclusions();
    }

    public void removeContext(Context context) {
        contextSet.remove(context);
        //context.endLive();
        Condition.checkConditions();
        Conclusion.checkConclusions();
    }

    public void removeIntegrityConstraint(IntegrityConstraint intcon) {
        intConSet.remove(intcon);
        //
        Condition.checkConditions();
        Conclusion.checkConclusions();
    }

    /**
     * *****************************Examples*********************************
     */
    /**
     * Creates default example base. For testing purpouses.
     */
    public void createExampleBase() {
        createExampleBase("basic");
    }

    /**
     * Creates example base by the ID. For testing purpouses.
     *
     * @param typeID ID of the example base
     */
    public void createExampleBase(String typeID) {
        switch (typeID) {
            case "basic": {
                createBasic();
                break;
            }
            default:
                break;
        }
    }

    private void createBasic() {

        AbstractAttribute mabak = createAtribute(AttributeType.binary);
        mabak.setID("ma_bak");
        mabak.setName("Ma bakalarku");
        mabak.setComment("Bak prace pripravena");

        AbstractAttribute jePripraven = createAtribute(AttributeType.binary);
        jePripraven.setID("je_pripraven");
        jePripraven.setName("je pripraven na vse");
        jePripraven.setComment("Priprava byla provedena");

        AbstractRule rule = createRule(RuleType.compositional);
        rule.setID("spoj");
        rule.setComment("Spojení přípravy s bak");

        BinaryAttribute fullmabak = (BinaryAttribute) mabak;
        BinaryAttribute fullpripraven = (BinaryAttribute) jePripraven;
        CompleteRule fullrule = (CompleteRule) rule;
        fullrule.addConclusionElement(fullmabak);
        fullrule.addConditionElement(fullpripraven);

    }

      
    /**
     * go throu base and get all conditions
     *
     * @return
     */
    public Set<IConditionElement> getAllConditions() {
        Set conditionSet = Collections.synchronizedSet(new HashSet<>());
        for (AbstractAttribute atribute : atributeSet) {
            if (atribute instanceof BinaryAttribute) {
                conditionSet.add(atribute);
            } else if (atribute instanceof SingleAttribute) {
                SingleAttribute single = (SingleAttribute) atribute;
                conditionSet.addAll(single.getPropositionSet());
            } else if (atribute instanceof SetAttribute) {
                SetAttribute set = (SetAttribute) atribute;
                conditionSet.addAll(set.getPropositionSet());
            } else if (atribute instanceof NumericAttribute) {
                NumericAttribute numeric = (NumericAttribute) atribute;
                conditionSet.addAll(numeric.getPropositionSet());
            }
        }
        return conditionSet;
    }

    /**
     * go throu base and get all conclusions
     *
     * @return
     */
    public Set<IConclusionElement> getAllConclusions() {
        Set conditionSet = Collections.synchronizedSet(new HashSet<>());
        for (AbstractAttribute atribute : atributeSet) {
            if (atribute instanceof BinaryAttribute) {
                conditionSet.add(atribute);
            } else if (atribute instanceof SetAttribute) {
                SetAttribute set = (SetAttribute) atribute;
                conditionSet.addAll(set.getPropositionSet());
            }
        }
        return conditionSet;
    }

    
    /***
     * TODO more clever function
     * @param AtributeID
     * @return 
     */
    public AbstractAttribute getAttributeByID(String AtributeID){
        
        AbstractAttribute desiredAttribute = null;
        
        for (AbstractAttribute atribute : atributeSet) {
            if (atribute.getID().equalsIgnoreCase(AtributeID)){
                desiredAttribute = atribute;
                break;
            }
        }
        return desiredAttribute;
    }
    
}

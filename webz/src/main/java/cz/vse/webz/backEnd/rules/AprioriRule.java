/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.rules;

import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Martin Kozák
 */
public class AprioriRule extends AbstractRule implements IConclusionMaster {

   
    private final Set<Conclusion> conclusions;

    public AprioriRule() {
        super("new apriory rule", RuleType.apriori);
        this.conclusions = Collections.synchronizedSet(new HashSet<>());
    }

    @Override
    public Set<Conclusion> getAllConclusions() {
        return conclusions;
    }

    @Override
    public void deleteConclusionElement(Conclusion conclusion) {
        conclusions.remove(conclusion);
    }

    @Override
    public Conclusion addConclusionElement(IConclusionElement element) {
        Conclusion newConclusion = new Conclusion(element, this);
        conclusions.add(newConclusion);
        return newConclusion;
    }
}

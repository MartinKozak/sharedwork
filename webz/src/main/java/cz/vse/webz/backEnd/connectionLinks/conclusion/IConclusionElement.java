/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.conclusion;

import com.vaadin.ui.Panel;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Can be elements in conclusion. This class requires nothing,
 * and is implemented by...
 * 
 * @author Martin Kozák
 */
public interface IConclusionElement {
    public String getID();
    
    public Panel getPanel();
       
    
    /**
     * Indicator that given element is still used.
     * @return 
     */
    public boolean isAlive();
    
    /**
     * Given element is no longer alive
     */
    public void endLive();
}

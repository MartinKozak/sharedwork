/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.attributesPropositions;


import cz.vse.webz.backEnd.enums.AttributeType;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Martin Kozák
 */
public class SetAttribute extends AbstractAttribute{
    
    private final Set<SetProposition> propositionSet;
    
    public SetAttribute(AttributeType type) {
        super("new numeric atribute",type);
        propositionSet = Collections.synchronizedSet(new HashSet<>());
    }
    
    public SetProposition createProposition(){
        SetProposition proposition = new SetProposition(this);
        getPropositionSet().add(proposition);
        return proposition;
    }
    
    
    public void removeProposition(SetProposition proposition){        
        getPropositionSet().remove(proposition);
    }

    /**
     * @return the propositionSet
     */
    public Set<SetProposition> getPropositionSet() {
        return propositionSet;
    }
    
    /**
     * This element is no longer alive
     * also end live of all propositions
     */
    @Override
    public void endLive(){
        super.endLive();
        for (SetProposition proposition: propositionSet){
            proposition.endLive();
        }
    }
    
    
    /***
     * TODO more clever function
     * @param propositionID
     * @return 
     */
    public SetProposition getPropositionByID(String propositionID){
        
        SetProposition desiredProposition = null;
        
        for (SetProposition proposition : propositionSet) {
            if (proposition.getID().equalsIgnoreCase(propositionID)){
                desiredProposition = proposition;
                break;
            }
        }
        return desiredProposition;
    }
    
}

package cz.vse.webz.backEnd.rules;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.IConjunctionMaster;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Martin Kozák
 */
public class Conjunction implements IConditionMaster {

    private IConjunctionMaster master;

    private boolean alive;

    private boolean linked;

    private Panel panel;

    private final Set<Condition> conditions;



    private double conditionThreshold;

    /**
     * Constructor
     */
    public Conjunction(IConjunctionMaster master) {
        this.master = master;
        this.alive = true;
        this.linked = false;

        this.conditions = Collections.synchronizedSet(new HashSet<>());
        

        conditionThreshold = 0;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public void endLive() {
        this.alive = false;
    }

    
    
    public Panel getPanel() {
        return panel;
    }

    public void linkWithPanel(Panel panel) {
        if (!linked) {
            this.panel = panel;
            linked = true;
        } else {
            //error
        }
    }
    
    
    
    @Override
    public Set<Condition> getAllConditions() {
        return conditions;
    }

    @Override
    public void deleteConditionLiteral(Condition literal) {
        conditions.remove(literal);
    }

    @Override
    public Condition addConditionElement(IConditionElement element) {
        Condition newCondition = new Condition(element, this);
        conditions.add(newCondition);
        return newCondition;

    }

    ///************************ONLY useful for logical rule*************/
    /**
     * @return the conditionThreshold
     */
    public double getConditionThreshold() {
        return conditionThreshold;
    }

    /**
     * @param conditionThreshold the conditionThreshold to set
     */
    public void setConditionThreshold(double conditionThreshold) {
        this.conditionThreshold = conditionThreshold;
    }

    /**
     * @return the master
     */
    public IConjunctionMaster getParent() {
        return master;
    }

}

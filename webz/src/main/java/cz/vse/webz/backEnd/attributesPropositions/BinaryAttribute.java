/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.attributesPropositions;

import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.frontEnd.texts;

/**
 * Class that defines binary atribute
 *
 * @author Martin Kozák
 */
public class BinaryAttribute extends AbstractAttribute implements IConclusionElement, IConditionElement{

 
    
    
    /**
     * Empty constructor, creates default Binary atribute
     */
    public BinaryAttribute(){
         super(texts.tBinID,AttributeType.binary);
    }

    
    
}
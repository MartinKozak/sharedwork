/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.condition;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Simple object with link to Atribute, Proposition and boolean negativity Also
 * contains link to a parent Rule
 *
 * @author Martin Kozák
 */
public class Condition {

    private final IConditionMaster master;

    private final IConditionElement element;
    private boolean negation;

    private static final Set<Condition> conditionSet = Collections.synchronizedSet(new HashSet<>());

    public Condition(IConditionElement element, IConditionMaster master) {
        this.element = element;
        this.master = master;
        negation = false;
        conditionSet.add(this);
    }

    
    
    public String getID(){
        return element.getID();
    }
    
    
    /**
     * @return the positive
     */
    public boolean isNegation() {
        return negation;
    }

    /**
     * @param positive the positive to set
     */
    public void setNegation(boolean value) {
        this.negation = value;
    }

    /**
     * Switch the positivity of a Literal
     */
    public void switchPositivity() {
        this.negation = !(this.negation);
    }

    /**
     * @return the master
     */
    public IConditionMaster getMaster() {
        return master;
    }

    /**
     * @return the element
     */
    public IConditionElement getElement() {
        return element;
    }

    public String getNegativityCode(boolean positivity) {
        if (positivity) {
            if (negation) {
                return "1";
            } else {
                return "0";
            }
        } else {
            if (!negation) {
                return "1";
            } else {
                return "0";
            }
        }
    }

    /**
     * Checks if there is all right, if not correct it
     */
    public static void checkConditions() {
        synchronized (conditionSet) {
            Iterator i = conditionSet.iterator(); // Must be in the synchronized block
            while (i.hasNext()) {
                Condition condition = (Condition) i.next();
                if (!(condition.getElement().isAlive())) {
                    condition.master.deleteConditionLiteral(condition);
                    i.remove();
                } else if (!(condition.getMaster().isAlive())) {
                    i.remove();
                } else {
                    //All right
                }
            }
        }
    }
}

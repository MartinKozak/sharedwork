/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd;

import cz.vse.webz.backEnd.enums.globals.DefaultWeight;
import cz.vse.webz.backEnd.enums.globals.GlobalPriority;
import cz.vse.webz.backEnd.enums.globals.InferenceMechanism;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 *
 * @author Martin Kozák
 */
public class Globals {

    private String description;
    private String expert;
    private String knowledgeEngineer;
    private Date date;
    private InferenceMechanism inferenceMechanism;
    private int weightRange;
    private DefaultWeight defaultWeight;
    private GlobalPriority globalPriority;
    private double contextGlobalThreshold;
    private double conditionGlobalThreshold;

    public Globals() {
        description = "";
        expert = "";
        knowledgeEngineer = "";
        date = new Date();
        inferenceMechanism = InferenceMechanism.standard;
        weightRange = 1;
        defaultWeight = DefaultWeight.irrelevant;
        globalPriority = GlobalPriority.user;
        contextGlobalThreshold = 0.000;
        conditionGlobalThreshold = 0.900;
    }

    /*
     public void setGlobal(String description, String expert, String knowledgeEngineer, 
     Date date, InferenceMechanism inferenceMechanism, int weight_range,
     DefaultWeight defaultWeight, GlobalPriority globalPriority, 
     Float contextGlobalThreshold, Float conditionGlobalThreshold){
        
     this.description = description;
     this.expert = expert;
     this.knowledgeEngineer = knowledgeEngineer;
     }
     */
    /**
     * *********************************Getters*******************************
     */
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the expert
     */
    public String getExpert() {
        return expert;
    }

    /**
     * @return the knowledgeEngineer
     */
    public String getKnowledgeEngineer() {
        return knowledgeEngineer;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return the inferenceMechanism
     */
    public InferenceMechanism getInferenceMechanism() {
        return inferenceMechanism;
    }

    /**
     * @return the weight_range
     */
    public int getWeightRange() {
        return weightRange;
    }

    /**
     * @return the defaultWeight
     */
    public DefaultWeight getDefaultWeight() {
        return defaultWeight;
    }

    /**
     * @return the globalPriority
     */
    public GlobalPriority getGlobalPriority() {
        return globalPriority;
    }

    /**
     * @return the contextGlobalThreshold
     */
    public double getContextGlobalThreshold() {
        return contextGlobalThreshold;
    }

    /**
     * @return the conditionGlobalThreshold
     */
    public double getConditionGlobalThreshold() {
        return conditionGlobalThreshold;
    }

    public String getDateRepresentation() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate());
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        String dateString = String.format("%02d", day) + "." + String.format("%02d", month + 1) + "." + year;
        return dateString;
    }

    public void setDateFromXMLRepresentation(String dateString) {
        String[] dateParts = dateString.split("[.]");
        if (dateParts.length > 1) {
            try {
                int day = Integer.parseInt(dateParts[0]);
                int month = Integer.parseInt(dateParts[1]);
                int year = Integer.parseInt(dateParts[2]);

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                setDate(calendar.getTime());
            } catch (NumberFormatException ex) {
                //Do nothing, text can be blank
            }
        }
    }

    private String decimalString(double number) {
        String decimalFormat = String.format(new Locale("cs", "CZ"), "%.3f", number);
        return decimalFormat;
    }

    public String getContextGTRepresentation() {
        return decimalString(contextGlobalThreshold);
    }

    public String getConditionGTRepresentation() {
        return decimalString(conditionGlobalThreshold);
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param expert the expert to set
     */
    public void setExpert(String expert) {
        this.expert = expert;
    }

    /**
     * @param knowledgeEngineer the knowledgeEngineer to set
     */
    public void setKnowledgeEngineer(String knowledgeEngineer) {
        this.knowledgeEngineer = knowledgeEngineer;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @param inferenceMechanism the inferenceMechanism to set
     */
    public void setInferenceMechanism(InferenceMechanism inferenceMechanism) {
        this.inferenceMechanism = inferenceMechanism;
    }

    /**
     * @param weight_range the weight_range to set
     */
    public void setWeightRange(int weightRange) {
        this.weightRange = weightRange;
    }

    /**
     * @param defaultWeight the defaultWeight to set
     */
    public void setDefaultWeight(DefaultWeight defaultWeight) {
        this.defaultWeight = defaultWeight;
    }

    /**
     * @param globalPriority the globalPriority to set
     */
    public void setGlobalPriority(GlobalPriority globalPriority) {
        this.globalPriority = globalPriority;
    }

    /**
     * @param contextGlobalThreshold the contextGlobalThreshold to set
     */
    public void setContextGlobalThreshold(double contextGlobalThreshold) {
        this.contextGlobalThreshold = contextGlobalThreshold;
    }

    /**
     * @param conditionGlobalThreshold the conditionGlobalThreshold to set
     */
    public void setConditionGlobalThreshold(double conditionGlobalThreshold) {
        this.conditionGlobalThreshold = conditionGlobalThreshold;
    }
}

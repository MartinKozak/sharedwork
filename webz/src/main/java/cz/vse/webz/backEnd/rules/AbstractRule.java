/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.rules;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.enums.RuleType;

/**
 *
 * @author Martin Kozák
 */
public class AbstractRule {
   
    private String ID;
    private final RuleType type;
    private String comment;
    
    private boolean alive;
    
    private boolean linked;
    private Panel panel;

    private boolean negativeRule;
    
    public AbstractRule(String ID, RuleType type){
        this.ID = ID;
        this.type = type;
        this.comment = "";
        
        this.linked = false;
        this.alive = true;
        this.negativeRule = false;
    }

    /**
     * Indicator that given element is still used.
     *
     * @return
     */
    public boolean isAlive(){
        return alive;
    }

    /**
     * This element is no longer alive
     */
    public void endLive(){
        alive = false;
    }
    
    
    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the type
     */
    public RuleType getType() {
        return type;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    public Panel getPanel() {
        return panel;
    }

    public void linkWithPanel(Panel panel) {
        if (!linked) {
            this.panel = panel;
            linked = true;
        } else {
            //error
        }
    }

    /**
     * @return the negativeRule
     */
    public boolean isNegativeRule() {
        return negativeRule;
    }

    /**
     * @param negativeRule the negativeRule to set
     */
    public void setNegativeRule(boolean negativeRule) {
        this.negativeRule = negativeRule;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.connectionLinks.conclusion;

import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.utility.Settings;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Simple object with link to Atribute, Proposition and boolean negativity. Also
 * contains link to a parent Rule Unlike Literal contains weight
 *
 * @author Martin Kozák
 */
public class Conclusion {

    private final IConclusionMaster master;
    private final IConclusionElement element;
    private boolean negation;
    private double weight;
    
    
    private static final Set<Conclusion> conclusionSet = Collections.synchronizedSet(new HashSet<>());

    public Conclusion(IConclusionElement element, IConclusionMaster master) {
        this.element = element;
        this.master = master;
        this.weight = AplicationLayout.getInstance().getSettings().getDefaultWeight();
        negation = false;

        conclusionSet.add(this);
    }

    
    public String getID(){
        return element.getID();
    }
    
    /**
     * @return the positive
     */
    public boolean isNegation() {
        return negation;
    }

    /**
     * @param value the value to set to the negativity
     */
    public void setNegation(boolean value) {
        this.negation = value;
    }

    /**
     * Switch the positivity of a Literal
     */
    public void switchPositivity() {
        this.negation = !(this.negation);
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return the master
     */
    public IConclusionMaster getMaster() {
        return master;
    }

    /**
     * @return the element
     */
    public IConclusionElement getElement() {
        return element;
    }

    public String getNegativityCode(boolean positivity) {
        if (positivity) {
            if (negation) {
                return "1";
            } else {
                return "0";
            }
        } else {
            if (!negation) {
                return "1";
            } else {
                return "0";
            }
        }
    }

    /**
     * Checks if there is all right, if not correct it
     */
    public static void checkConclusions() {
        synchronized (conclusionSet) {
            Iterator i = conclusionSet.iterator(); // Must be in the synchronized block
            while (i.hasNext()) {
                Conclusion condition = (Conclusion) i.next();
                if (!(condition.getElement().isAlive())) {
                    condition.master.deleteConclusionElement(condition);
                    i.remove();
                } else if (!(condition.getMaster().isAlive())) {
                    i.remove();
                } else {
                    //All right
                }
            }
        }
    }

}

package cz.vse.webz.backEnd.attributesPropositions;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.enums.Scope;
import cz.vse.webz.backEnd.enums.AttributeType;

/**
 *
 *
 * @author Martin Kozák
 */
public abstract class AbstractAttribute {

    /*Protected, because it is used by subclasses
     It could have been package private, but for clarity this is better
     */
    private String ID;
    private final AttributeType type;
    private String name;
    private String comment;
    private Scope scope;

    private boolean alive;
    
    private boolean linked;
    private Panel panel;

    //Konstruktor
    public AbstractAttribute(String ID, AttributeType type) {        
        this.ID = ID;
        this.type = type;
        this.name = "";
        this.comment = "";
        this.scope = Scope.CASE;
        linked = false;        
        
        alive = true;
    }

    /***
     * Creates a link between panel and atribute object. 
     * 
     * @param panel 
     */
    public void linkWithPanel(Panel panel) {
        if (!isLinked()) {
            this.panel = panel;
            linked = true;
        } else {
            //error
        }
    }
    
    
    /**
     * Indicator that given element is still used.
     *
     * @return
     */
    public boolean isAlive(){
        return alive;
    }

    /**
     * This element is no longer alive
     */
    public void endLive(){
        alive = false;
    }
    
    
    
    /**
     * Only getter without setter (type is final)
     *
     * @return the type
     */
    public AttributeType getType() {
        return type;
    }

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the scope
     */
    public Scope getScope() {
        return scope;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param scope the scope to set
     */
    public void setScope(Scope scope) {
        this.scope = scope;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    public Panel getPanel() {
        return panel;
    }

    /**
     * @return the linked
     */
    public boolean isLinked() {
        return linked;
    }

    

}

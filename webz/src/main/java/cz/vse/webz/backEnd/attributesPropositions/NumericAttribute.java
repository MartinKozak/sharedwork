/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd.attributesPropositions;


import cz.vse.webz.backEnd.rules.Context;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.backEnd.enums.Scope;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 *
 * @author Martin Kozák
 */
public class NumericAttribute extends AbstractAttribute{
    
     private final Set<NumericProposition> propositionSet;
    
     private double fuzzyLower;
     private double fuzzyUpper;
     
    public NumericAttribute(){
        super("new numeric atribute", AttributeType.numeric);
        propositionSet = Collections.synchronizedSet(new HashSet<>());
    }
    
    
    /**
     * This element is no longer alive
     * also end live of all propositions
     */
    @Override
    public void endLive(){
        super.endLive();
        for (NumericProposition proposition: getPropositionSet()){
            proposition.endLive();
        }
    }

    /**
     * @return the propositionSet
     */
    public Set<NumericProposition> getPropositionSet() {
        return propositionSet;
    }
    
    
    public NumericProposition createProposition(){
        NumericProposition proposition = new NumericProposition(this);
        propositionSet.add(proposition);
        return proposition;
    }
    
    
    public void removeProposition(NumericProposition proposition){        
        propositionSet.remove(proposition);
    }

    
    
    /***Fuzzy Fields Acess*/
    /**
     * @return the fuzzyLower
     */
    public double getFuzzyLower() {
        return fuzzyLower;
    }

    /**
     * @param fuzzyLower the fuzzyLower to set
     */
    public void setFuzzyLower(double fuzzyLower) {
        this.fuzzyLower = fuzzyLower;
    }

    /**
     * @return the fuzzyUpper
     */
    public double getFuzzyUpper() {
        return fuzzyUpper;
    }

    /**
     * @param fuzzyUpper the fuzzyUpper to set
     */
    public void setFuzzyUpper(double fuzzyUpper) {
        this.fuzzyUpper = fuzzyUpper;
    }
    
    
    private String decimalString(double number){
        String decimalFormat = String.format(new Locale("cs", "CZ"), "%.3f", number);
        return decimalFormat;
    }
    
    public String getUpperGTRepresentation(){
        return decimalString(fuzzyUpper);
    }
    
    public String getLowerGTRepresentation(){
        return decimalString(fuzzyLower);
    }
 
    
    
     /***
     * TODO more clever function
     * @param propositionID
     * @return 
     */
    public NumericProposition getPropositionByID(String propositionID){
        
        NumericProposition desiredProposition = null;
        
        for (NumericProposition proposition : propositionSet) {
            if (proposition.getID().equalsIgnoreCase(propositionID)){
                desiredProposition = proposition;
                break;
            }
        }
        return desiredProposition;
    }
    
}

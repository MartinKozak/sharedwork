package cz.vse.webz.backEnd.attributesPropositions;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import java.util.Locale;

/**
 * Propositions, in form of POJO
 *
 * @author Martin Kozák
 */
public class NumericProposition implements IConditionElement{

    private String ID;
    private String name; 
    private String comment;

    private final NumericAttribute parentAtribute;
    
    private double fuzzyLower;
    private double crispLower;
    private double crispUpper;
    private double fuzzyUpper;
    
    private boolean linked;
    private Panel panel;
    
    
    
    private boolean alive;
    
    public NumericProposition(NumericAttribute parentAtribute) {
        this("new nominal proposition","","",parentAtribute);
    }
  
    
    public NumericProposition(String ID, String name, String comment, NumericAttribute parentAtribute) {
        this.ID = ID;
        this.name = name;        
        this.comment = comment;
        this.parentAtribute = parentAtribute;
        
        linked = false;
        
        alive = true;
    }

    
    
    /**
     * Indicator that given element is still used.
     *
     * @return
     */
    @Override
    public boolean isAlive(){
        return alive;
    }

    /**
     * This element is no longer alive
     */
    @Override
    public void endLive(){
        alive = false;
    }
    
    
    /***
     * Creates a link between panel and proposition.
     * 
     * @param panel 
     */
    public void linkWithPanel(Panel panel) {
        if (!linked) {
            this.panel = panel;
            linked = true;
        } else {
            //error
        }
    }
    
    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @return the parentAtribute
     */
    public NumericAttribute getParentAtribute() {
        return parentAtribute;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Panel getPanel() {
        return panel;
    }

    /**
     * @return the fuzzyLower
     */
    public double getFuzzyLower() {
        return fuzzyLower;
    }

    /**
     * @param fuzzyLower the fuzzyLower to set
     */
    public void setFuzzyLower(double fuzzyLower) {
        this.fuzzyLower = fuzzyLower;
    }

    /**
     * @return the crispLower
     */
    public double getCrispLower() {
        return crispLower;
    }

    /**
     * @param crispLower the crispLower to set
     */
    public void setCrispLower(double crispLower) {
        this.crispLower = crispLower;
    }

    /**
     * @return the crispUpper
     */
    public double getCrispUpper() {
        return crispUpper;
    }

    /**
     * @param crispUpper the crispUpper to set
     */
    public void setCrispUpper(double crispUpper) {
        this.crispUpper = crispUpper;
    }

    /**
     * @return the fuzzyUpper
     */
    public double getFuzzyUpper() {
        return fuzzyUpper;
    }

    /**
     * @param fuzzyUpper the fuzzyUpper to set
     */
    public void setFuzzyUpper(double fuzzyUpper) {
        this.fuzzyUpper = fuzzyUpper;
    }
    /***Fuzzy Fields Acess*/
    

    
    private String decimalString(double number){
        String decimalFormat = String.format(new Locale("cs", "CZ"), "%.3f", number);
        return decimalFormat;
    }
    
    public String getUpperFuzzyGTRepresentation(){
        return decimalString(fuzzyUpper);
    }
    
    public String getLowerFuzzyGTRepresentation(){
        return decimalString(fuzzyLower);
    }
    
    
    public String getUpperCrispGTRepresentation(){
        return decimalString(crispUpper);
    }
    
    public String getLowerCrispGTRepresentation(){
        return decimalString(crispLower);
    }
    
    
}
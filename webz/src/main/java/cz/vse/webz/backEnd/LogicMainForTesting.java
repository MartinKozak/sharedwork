/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.backEnd;

import cz.vse.webz.utility.XmlGenerator;

/**
 *
 * @author Martin Kozák
 */
public class LogicMainForTesting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {// TODO code application logic here
        Base base = new Base();
        //Onyl 4 testing
        base.createExampleBase();
       
        XmlGenerator xmlGenerator = new XmlGenerator();
        String xml = xmlGenerator.generateXML(base);
        
        
        
        System.out.println(xml);
    }
    
}

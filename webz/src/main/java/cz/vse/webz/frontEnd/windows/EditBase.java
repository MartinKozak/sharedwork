package cz.vse.webz.frontEnd.windows;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.Globals;
import cz.vse.webz.backEnd.enums.globals.DefaultWeight;
import cz.vse.webz.backEnd.enums.globals.GlobalPriority;
import cz.vse.webz.backEnd.enums.globals.InferenceMechanism;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;
/**
 *
 * @author Martin Kozák
 */
public class EditBase extends Window {

    FieldGroup binder;

    private TextArea description = new TextArea(wbDescription);
    private TextField expert = new TextField(wbExpert);
    private TextField knowledgeEngineer = new TextField(wbKnowledgeEngineer);

    private OptionGroup inferenceMechanism = new OptionGroup(wbInferenceMechanism);
    private OptionGroup defaultWeight = new OptionGroup(wbDefaultWeight);
    private OptionGroup globalPriority = new OptionGroup(wbGlobalPriority);

    private DateField date = new DateField(wbDate);

    private TextField weightRange = new TextField(wbWeightRange);
    private TextField contextGlobalThreshold = new TextField(wbContextGlobalThreshold);
    private TextField conditionGlobalThreshold = new TextField(wbConditionGlobalThreshold);

    private boolean save;

    public EditBase(BeanItem<Globals> bean) {
        super(wNameEditBase);

        setModal(true);
        setHeight("90%");
        addStyleName("window");
        
        GridLayout form = new GridLayout(3, 8);

        form.addComponent(description, 0, 0, 2, 0);
        description.setWidth("400px");
        description.setHeight("300px");

        form.addComponent(expert, 0, 1);
        form.addComponent(knowledgeEngineer, 0, 2);

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        form.addComponent(date, 0, 3);

        form.addComponent(weightRange, 0, 4);
        form.addComponent(contextGlobalThreshold, 0, 5);
        form.addComponent(conditionGlobalThreshold, 0, 6);

        inferenceMechanism.addItems(InferenceMechanism.values());
        inferenceMechanism.setData(bean.getBean().getInferenceMechanism());
        form.addComponent(inferenceMechanism, 1, 1, 1, 2);

        defaultWeight.addItems(DefaultWeight.values());
        defaultWeight.setData(bean.getBean().getDefaultWeight());
        form.addComponent(defaultWeight, 1, 3);

        globalPriority.addItems(GlobalPriority.values());
        globalPriority.setData(bean.getBean().getGlobalPriority());
        form.addComponent(globalPriority, 2, 1, 2, 3);

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 7);
        setContent(form);

        form.addComponent(new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        }), 2, 7);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

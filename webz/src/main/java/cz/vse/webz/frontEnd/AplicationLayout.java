package cz.vse.webz.frontEnd;

import com.vaadin.ui.HorizontalLayout;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.Base;
import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.utility.Settings;

/**
 * Layout containing the whole UI. Its singleton and its made up of two parts
 * Menu and canvas. (False singleton, constructor can be called)
 * This was needed for web server
 *
 * @author Martin Kozák
 */
public class AplicationLayout extends HorizontalLayout {

    private static AplicationLayout instance;
    
    private Base base;

    private final CanvasLayout canvasLayout;
    private final MenuLayout menuLayout;

    private final Settings settings;

    public AplicationLayout() {    
        super();
         
        setSizeFull();
        canvasLayout = new CanvasLayout(this);
        menuLayout = new MenuLayout(this);
        settings = new Settings();
        
        base = new Base();        
        
        //Onyl 4 testing
        //base.createExampleBase();
          
        Panel menuPanel = new Panel();
        menuPanel.setSizeFull();
        menuPanel.setWidth("275px");
        
        menuPanel.setCaption(mMenuTitle);       
        menuPanel.setContent(menuLayout);
        
        addComponent(menuPanel);
        
        
        Panel canvasPanel = new Panel();
        canvasPanel.setSizeFull();
        
        canvasPanel.setCaption(mCanvasTitle);
        canvasPanel.setContent(canvasLayout);

        addComponent(canvasPanel);
        setExpandRatio(canvasPanel, 1f);
        
        instance = this;
    }

    public static AplicationLayout getInstance() {
        return instance;
    }
    
    
    
    public Base getBase(){
        return base;
    }

    public void setBase(Base base){
        this.base = base;
    }

    /**
     * @return the canvasLayout
     */
    public CanvasLayout getCanvasLayout() {
        return canvasLayout;
    }

    /**
     * @return the menuLayout
     */
    public MenuLayout getMenuLayout() {
        return menuLayout;
    }
    
    /**
     * @return the settings
     */
    public Settings getSettings() {
        return settings;
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.components.attrprop;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.NumericProposition;
import cz.vse.webz.frontEnd.componentParts.ConditionElementButton;
import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.frontEnd.windows.attrprop.EditNumericProposition;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class NumericPropositionPanel extends AbsAtrPropPanel {

    private BeanItem<NumericProposition> beanSource;

    private HorizontalLayout numericPart;
    private Label fuzzyLower;
    private Label fuzzyUpper;

    private Label crispLower;
    private Label crispUpper;

    public NumericPropositionPanel(NumericProposition sourceProposition) {
        super();
        beanSource = new BeanItem<>(sourceProposition);

        sourceProposition.setID(pNumID);
        sourceProposition.setName(pNumName);
        sourceProposition.setComment(pNumComment);

        addConnectors();

        addNumericParts();

        super.getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setValue(pNumType);
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));

        fuzzyLower.setConverter(new StringToDoubleConverter());
        fuzzyLower.setPropertyDataSource(beanSource.getItemProperty("fuzzyLower"));

        crispLower.setConverter(new StringToDoubleConverter());
        crispLower.setPropertyDataSource(beanSource.getItemProperty("crispLower"));

        crispUpper.setConverter(new StringToDoubleConverter());
        crispUpper.setPropertyDataSource(beanSource.getItemProperty("crispUpper"));

        fuzzyUpper.setConverter(new StringToDoubleConverter());
        fuzzyUpper.setPropertyDataSource(beanSource.getItemProperty("fuzzyUpper"));
    }

    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkThisProposition(BeanItem<NumericProposition> beanSource) {
        super.getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setValue(pSetType);
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
        
        fuzzyLower.setPropertyDataSource(beanSource.getItemProperty("fuzzyLower"));
        crispLower.setPropertyDataSource(beanSource.getItemProperty("crispLower"));
        crispUpper.setPropertyDataSource(beanSource.getItemProperty("crispUpper"));
        fuzzyUpper.setPropertyDataSource(beanSource.getItemProperty("fuzzyUpper"));
        
    }
    
    public void relink(){
        linkThisProposition(beanSource);
    }
    
    
    
    
    private void addConnectors() {
        //parts of binary atribute and preposition
        ConditionElementButton conditionButton = new ConditionElementButton(beanSource.getBean());
        conditionButton.addStyleName("topPart");

        getObjectFieldLayout().addComponent(conditionButton);
        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditNumericProposition(beanSource);

        thisUI.addWindow(window);
    }

    @Override
    protected void handleDeleteClick() {
        super.handleDeleteClick();
        if (beanSource != null) {
            beanSource.getBean().getParentAtribute().removeProposition(beanSource.getBean());
        }
    }

    private void addNumericParts() {
        numericPart = new HorizontalLayout();
        numericPart.addStyleName("numericPart");
        numericPart.setSizeFull();

        fuzzyLower = new Label();
        fuzzyLower.setCaption(nFuzzyLower);
        fuzzyUpper = new Label();
        fuzzyUpper.setCaption(nFuzzyUpper);

        crispLower = new Label();
        crispLower.setCaption(nCrispLower);
        crispUpper = new Label();
        crispUpper.setCaption(nCrispUpper);

        VerticalLayout twoLinesWrap = new VerticalLayout();
        HorizontalLayout crispPart = new HorizontalLayout();
        crispPart.addComponent(crispLower);
        crispPart.addComponent(crispUpper);

        HorizontalLayout fuzzyPart = new HorizontalLayout();
        fuzzyPart.addComponent(fuzzyLower);
        fuzzyPart.addComponent(fuzzyUpper);

        twoLinesWrap.addComponent(crispPart);
        twoLinesWrap.addComponent(fuzzyPart);

        numericPart.addComponent(twoLinesWrap);

        getCentralPart().addComponent(numericPart);

    }

    @Override
    public void relinkAtribute() {
        super.relinkAtribute();
        fuzzyLower.setPropertyDataSource(beanSource.getItemProperty("fuzzyLower"));
        crispLower.setPropertyDataSource(beanSource.getItemProperty("crispLower"));
        crispUpper.setPropertyDataSource(beanSource.getItemProperty("crispUpper"));
        fuzzyUpper.setPropertyDataSource(beanSource.getItemProperty("fuzzyUpper"));
    }

}

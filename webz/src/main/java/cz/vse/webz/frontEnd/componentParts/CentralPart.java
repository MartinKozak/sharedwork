package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;
import cz.vse.webz.frontEnd.components.rules.AndPanel;

/**
 * Central part of every graphic panel
 *
 * @author Martin Kozák
 */
public class CentralPart extends VerticalLayout {

    private final AGraphicPanel masterPanel;

    //basic labels (texts of panel)
    private final Label ID;
    private final Label type;
    private final Label comment;

    //basic parts 
    private final GridLayout centerGrid;       //Inner grid (allways there)

    //parts of centerGrid
    private final HorizontalLayout topButtonPart;
    private final HorizontalLayout lowerButtonPart;

    private final VerticalLayout commentLayout;//Layout containing comment  

    private final Button toggleComment;
    private final Button editButton;
    private final Button deleteButton;

    //private booleans (flags)
    private boolean showComment; //flag comment on/off

    /**
     * Creates basic parts
     */
    public CentralPart(AGraphicPanel masterPanel) {
        super();
        this.masterPanel = masterPanel;

        setWidth("100%");

        //Central GRID Start
        centerGrid = new GridLayout(2, 2);
        //First column take over second column
        centerGrid.setColumnExpandRatio(0, 1);
        centerGrid.setColumnExpandRatio(1, 0);
        centerGrid.setWidth("100%");

        // TOP LEFT - NAME / ID
        // nameTextPart = new VerticalLayout();
        ID = new Label("New atribute");
        //nameTextPart.addComponent(ID);
        centerGrid.addComponent(ID, 0, 0);

        //DOWN LEFT TYPE
        type = new Label("Object Type");
        centerGrid.addComponent(type, 0, 1);

        //TOP RIGHT BUTTONS
        topButtonPart = new HorizontalLayout();

        editButton = new Button("Edit");
        editButton.addStyleName("editButton");
        editButton.addClickListener(e -> {
            masterPanel.handleEditClick();
        });

        deleteButton = new Button(new ThemeResource("../runo/icons/16/cancel.png"));
        deleteButton.setStyleName(BaseTheme.BUTTON_LINK);
        deleteButton.setSizeUndefined();
        deleteButton.addStyleName("cancelIcon");
        //exit.setHeight("20px");
        //exit.setWidth("10px");
        deleteButton.addClickListener(e -> {
            Window window;
            window = new DeleteWindow(masterPanel);
            getUI().addWindow(window);
        });

        topButtonPart.addComponent(editButton);
        topButtonPart.addComponent(deleteButton);
        centerGrid.addComponent(topButtonPart, 1, 0);

        //Align menu to the right
        centerGrid.setComponentAlignment(topButtonPart, Alignment.TOP_RIGHT);

        //DOWN RIGHT BUTTONS
        lowerButtonPart = new HorizontalLayout();

        toggleComment = new Button("Comment hidden");
        toggleComment.addStyleName("commentButton");
        toggleComment.setDescription("Comment is not displayed, select to display.");
        toggleComment.addClickListener(e -> {
            changeComment();
        });
        showComment = false; //set in default state

        lowerButtonPart.addComponent(toggleComment);

        centerGrid.addComponent(lowerButtonPart, 1, 1);

        addComponent(centerGrid);
        //Central grid end

        //Comment Layout
        commentLayout = new VerticalLayout();
        comment = new Label();
        commentLayout.addComponent(comment);
        commentLayout.setVisible(false);

        addComponent(commentLayout);

    }

    /**
     * @return the idLabel
     */
    public Label getIdLabel() {
        return ID;
    }

    /**
     * @return the typeLabel
     */
    public Label getTypeLabel() {
        return type;
    }

    /**
     * @return the commentLabel
     */
    public Label getCommentLabel() {
        return comment;
    }

    //*************************************************************************/
    //Getters/setter for child panels
    /**
     * ***********************************************************************
     */
    /**
     * @return the centerGrid
     */
    public GridLayout getCenterGrid() {
        return centerGrid;
    }

    /**
     * @return the topButtonPart
     */
    public HorizontalLayout getTopButtonPart() {
        return topButtonPart;
    }

    /**
     * @return the lowerButtonPart
     */
    public HorizontalLayout getLowerButtonPart() {
        return lowerButtonPart;
    }

    private void changeComment() {
        masterPanel.setDoubleClickSuppression(false);
        if (!showComment) {
            commentLayout.setVisible(true);
            toggleComment.setDescription("Comment is displayed, select to hide.");
            toggleComment.setCaption("Comment shown");
            showComment = true;
        } else {
            commentLayout.setVisible(false);
            toggleComment.setDescription("Comment is not displayed, select to display.");
            toggleComment.setCaption("Comment hidden");
            showComment = false;
        }

    }

    //Small inner class    
    private static class DeleteWindow extends Window {

        private HorizontalLayout buttonLayout = new HorizontalLayout();

        public DeleteWindow(AGraphicPanel masterPanel) {
            setCaption("Delete?");
            setModal(true);
            setResizable(false);
            setContent(buttonLayout);
            buttonLayout.addComponent(new Button("Delete", e -> {
                masterPanel.handleDeleteClick();
                this.close();
            }));
            buttonLayout.addComponent(new Button("Cancel", e -> {
                this.close();
            }));
        }
    }

    /**
     * *
     * Change the style into very simplistic and button
     *
     * @param andPanel required but not used to ensure it is andPanel
     */
    public void reAnd(AndPanel andPanel) {

        GridLayout andGrind = new GridLayout(2, 1);

        setWidth("100%");

        //Central GRID Start
        //First column take over second column
        andGrind.setColumnExpandRatio(0, 1);
        andGrind.setColumnExpandRatio(1, 0);
        andGrind.setWidth("100%");

        // TOP LEFT - NAME / ID
        // nameTextPart = new VerticalLayout();
        Label AND = new Label("AND");
        //nameTextPart.addComponent(ID);
        andGrind.addComponent(AND, 0, 0);

        //DOWN LEFT TYPE
        andGrind.addComponent(topButtonPart, 1, 0);

        //Align menu to the right
        andGrind.setComponentAlignment(topButtonPart, Alignment.TOP_RIGHT);

        removeComponent(centerGrid);
        addComponent(andGrind);
        //Central grid end

    }

}

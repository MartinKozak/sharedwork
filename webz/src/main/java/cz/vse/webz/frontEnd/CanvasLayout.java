package cz.vse.webz.frontEnd;

import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.DragAndDropWrapper.DragStartMode;
import com.vaadin.ui.DragAndDropWrapper.WrapperTargetDetails;
import com.vaadin.ui.DragAndDropWrapper.WrapperTransferable;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import cz.vse.webz.backEnd.Base;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.attributesPropositions.BinaryAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericProposition;
import cz.vse.webz.backEnd.attributesPropositions.SetAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetProposition;
import cz.vse.webz.backEnd.attributesPropositions.SingleAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SingleProposition;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.rules.AprioriRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.backEnd.connectionLinks.condition.IConjunctionMaster;
import cz.vse.webz.backEnd.rules.Conjunction;
import cz.vse.webz.frontEnd.components.attrprop.AbsAtrPropPanel;
import cz.vse.webz.frontEnd.components.attrprop.BinaryAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.NumericAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.NumericPropositionPanel;
import cz.vse.webz.frontEnd.components.attrprop.SetAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.SetPropositionPanel;
import cz.vse.webz.frontEnd.components.attrprop.SingleAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.SinglePropositionPanel;
import cz.vse.webz.frontEnd.components.rules.AprioriRulePanel;
import cz.vse.webz.frontEnd.components.rules.CompositionalRulePanel;
import cz.vse.webz.frontEnd.components.rules.LogicalRulePanel;
import cz.vse.webz.frontEnd.components.rules.AndPanel;

import cz.vse.webz.utility.Settings;
import cz.vse.webz.utility.Settings.Position;
import org.vaadin.hezamu.canvas.Canvas;

/**
 * Graphic view, that displays all components into canvas where user can
 * manipulate them
 *
 * @author Martin KozĂˇk
 */
public class CanvasLayout extends VerticalLayout {

    private final AplicationLayout aplicationView;
    private final AbsoluteLayout componentHolder;
    private final Canvas graphicCanvas;

    private Settings settings;

    private final String width = "5000px";
    private final String height = "5000px";

    public CanvasLayout(AplicationLayout aplicationView) {
        super();
        this.aplicationView = aplicationView;
        setSizeUndefined();

        //Create canvas
        componentHolder = new AbsoluteLayout();
        componentHolder.setWidth(width);
        componentHolder.setHeight(height);
        //wrap the canvas into Drag and drop wraper / so drag and drop is enabled
        DragAndDropWrapper canvasWrap = wrapCanvas(componentHolder);

        //Creates new canvas (needed to create lines)
        graphicCanvas = new Canvas();
        graphicCanvas.setWidth(width);
        graphicCanvas.setHeight(height);
        graphicCanvas.clear();

        componentHolder.addComponent(graphicCanvas);
        addComponent(canvasWrap);

    }

    /**
     *
     */
    public void createTestObjects() {
        AbstractAttribute atribute = aplicationView.getBase().createAtribute(AttributeType.binary);
        BinaryAttribute binary = (BinaryAttribute) atribute;
        Panel binaryPanel = createAtribute(binary);
        binary.linkWithPanel(binaryPanel);

        AbstractRule rule = aplicationView.getBase().createRule(RuleType.compositional);
        CompleteRule compositional = (CompleteRule) rule;
        Panel completePanel = createRule(compositional);
        compositional.linkWithPanel(completePanel);

        compositional.addConditionElement(binary);

        repaint();
    }

    /**
     * Creates new component of graphic, new atributePanel It is defined in
     * components.AtributePanel
     *
     * @param atribute
     * @return
     */
    public AbsAtrPropPanel createAtribute(AbstractAttribute atribute) {
        final AbsAtrPropPanel atributePanel;
        int move = 0;
        switch (atribute.getType()) {
            case binary: {
                BinaryAttribute binaryAtribute = (BinaryAttribute) atribute;
                atributePanel = new BinaryAttributePanel(binaryAtribute);
                break;
            }
            case single: {
                SingleAttribute singleAtribute = (SingleAttribute) atribute;
                atributePanel = new SingleAttributePanel(singleAtribute);
                break;
            }
            case set: {
                SetAttribute setAtribute = (SetAttribute) atribute;
                atributePanel = new SetAttributePanel(setAtribute);
                break;
            }
            case numeric: {
                NumericAttribute numericAtribute = (NumericAttribute) atribute;
                atributePanel = new NumericAttributePanel(numericAtribute);
                move = 50;
                break;
            }
            default: {
                //error
                atributePanel = null;

                break;
            }
        }
        Settings settings = aplicationView.getSettings();
        Position position = settings.getAtributePosition();
        
        //Settings.Position position = aplicationView.getSettings().getAtributePosition();
        addWrapComponent(atributePanel, position.left, position.top);
        settings.setAtributeTop(position.top + 80 + move);

        return atributePanel;
    }

    public SinglePropositionPanel createSingleProposition(SingleProposition proposition) {
        final SinglePropositionPanel propositionPanel = new SinglePropositionPanel(proposition);
        SingleAttribute parent = proposition.getParentAtribute();
        float parentLeft = componentHolder.getPosition(parent.getPanel().getParent()).getLeftValue();
        float parentTop = componentHolder.getPosition(parent.getPanel().getParent()).getTopValue();
        int number = parent.getPropositionSet().size();

        int left = (int) parentLeft;
        left += number * 360;
        int top = (int) parentTop;
        top -= 0;

        addWrapComponent(propositionPanel, (int) left, (int) top);
        return propositionPanel;
    }

    public SetPropositionPanel createSetProposition(SetProposition proposition) {
        final SetPropositionPanel propositionPanel = new SetPropositionPanel(proposition);

        SetAttribute parent = proposition.getParentAtribute();
        float parentLeft = componentHolder.getPosition(parent.getPanel().getParent()).getLeftValue();
        float parentTop = componentHolder.getPosition(parent.getPanel().getParent()).getTopValue();
        int number = parent.getPropositionSet().size();

        int left = (int) parentLeft;
        left += number * 360;
        int top = (int) parentTop;
        top -= 0;

        addWrapComponent(propositionPanel, (int) left, (int) top);
        return propositionPanel;
    }

    public NumericPropositionPanel createNumericProposition(NumericProposition proposition) {
        final NumericPropositionPanel propositionPanel = new NumericPropositionPanel(proposition);
        NumericAttribute parent = proposition.getParentAtribute();
        float parentLeft = componentHolder.getPosition(parent.getPanel().getParent()).getLeftValue();
        float parentTop = componentHolder.getPosition(parent.getPanel().getParent()).getTopValue();
        int number = parent.getPropositionSet().size();

        int left = (int) parentLeft;
        left += number * 360;
        int top = (int) parentTop;
        top -= 0;

        addWrapComponent(propositionPanel, (int) left, (int) top);
        return propositionPanel;
    }

    public Panel createRule(AbstractRule rule) {

        final Panel rulePanel;

        switch (rule.getType()) {
            case compositional: {
                CompleteRule completeRule = (CompleteRule) rule;
                rulePanel = new CompositionalRulePanel(completeRule);
                break;
            }
            case logical: {
                CompleteRule completeRule = (CompleteRule) rule;
                rulePanel = new LogicalRulePanel(completeRule);
                break;
            }
            case apriori: {
                AprioriRule aprioryRule = (AprioriRule) rule;
                rulePanel = new AprioriRulePanel(aprioryRule);
                break;
            }

            default: {
                //error
                rulePanel = null;

                break;
            }
        }

        settings = aplicationView.getSettings();
        Position position = settings.getRulePosition();
        addWrapComponent(rulePanel, position.left, position.top);
        settings.setRuleTop(position.top + 80);
        return rulePanel;

    }

    public AndPanel createAND(Conjunction conjunction) {

        final AndPanel andPanel = new AndPanel(conjunction);

        IConjunctionMaster parent = conjunction.getParent();
        float parentLeft = componentHolder.getPosition(parent.getPanel().getParent()).getLeftValue();
        float parentTop = componentHolder.getPosition(parent.getPanel().getParent()).getTopValue();

        int parentAnd = parent.getAllConjunctions().size() - 1;
        
        int left = (int) parentLeft;
        left -= 230;
        int top = (int) parentTop;
        top += 0 + parentAnd*45;

        addWrapComponent(andPanel, (int) left, (int) top);
        
        return andPanel;

    }

    /**
     * *
     * Clears all components from holder.
     */
    public void clearCanvas() {
        componentHolder.removeAllComponents();
        //needs to reatach graphicCanvas
        componentHolder.addComponent(graphicCanvas);
        graphicCanvas.clear();
        aplicationView.getSettings().defaultPosition();
    }

    /**
     * *
     * Wraps the component and add it to the canvas
     *
     * @param component
     * @param left
     * @param top
     */
    private void addWrapComponent(Panel component, int left, int top) {
        // Put the component in a D&D wrapper and allow dragging it
        final DragAndDropWrapper wrappedObject = new DragAndDropWrapper(component);
        //Whole object is shown on drag
        wrappedObject.setDragStartMode(DragStartMode.COMPONENT_OTHER);
        wrappedObject.setDragImageComponent(component);
        // Set the wrapper to wrap tightly around the component
        wrappedObject.setSizeUndefined();

        String position = "left: " + left + "px; top: " + top + "px;";
        // Add the wrapper, not the component, to the layout
        componentHolder.addComponent(wrappedObject, position);
    }

    /**
     * ***********************************************************************
     * Method (and class) that enables Drag and drop feature of canvas
     */
    private DragAndDropWrapper wrapCanvas(AbsoluteLayout canvas) {
        DragAndDropWrapper canvasDnDWrap;
        canvasDnDWrap = new DragAndDropWrapper(canvas);

        /* Select change style, without blue box*/
        //layoutWrapper.addStyleName("no-vertical-drag-hints");
        //layoutWrapper.addStyleName("no-horizontal-drag-hints");        
        canvasDnDWrap.addStyleName("no-box-drag-hints");

        // Handle moving components within the AbsoluteLayout
        canvasDnDWrap.setDropHandler(new CanvasMoveHandler());

        return canvasDnDWrap;
    }

    /**
     * *
     * Clears all links and repaint them
     */
    public void repaint() {
        graphicCanvas.clear();
        Base base = aplicationView.getBase();

        for (AbstractRule rule : base.getRules()) {
            switch (rule.getType()) {
                case apriori: {
                    AprioriRule aprioryRule = (AprioriRule) rule;
                    for (Conclusion conclusion : aprioryRule.getAllConclusions()) {
                        drawConclusionLink(conclusion.getElement().getPanel(), aprioryRule.getPanel());
                    }
                    break;
                }
                case logical:
                case compositional: {
                    CompleteRule completeRule = (CompleteRule) rule;
                    for (Condition literal : completeRule.getAllConditions()) {
                        //for (Condition literal : conjunction.getLiteralList()) {
                        drawConditionLink(literal.getElement().getPanel(), completeRule.getPanel());
                    }
                    for (Conjunction and : completeRule.getAllConjunctions()){
                        drawAndLink(and.getPanel(), completeRule.getPanel());
                        for (Condition literal : and.getAllConditions()) {
                            drawConditionLink(literal.getElement().getPanel(), and.getPanel());
                        }
                    }
                    //}
                    for (Conclusion conclusion : completeRule.getAllConclusions()) {
                        drawConclusionLink(conclusion.getElement().getPanel(), completeRule.getPanel());
                    }
                    break;
                }

            }

        }

        //only temporaly demostration
        //drawLink(atribute, rule);
    }

    /**
     * Creates link between atribute and rule
     *
     * @param start
     * @param rule
     */
    private void drawConclusionLink(Panel start, Panel end) {
        float moveL = componentHolder.getPosition(start.getParent()).getLeftValue() + 5;
        float moveT = componentHolder.getPosition(start.getParent()).getTopValue() + 15;

        float lineL = componentHolder.getPosition(end.getParent()).getLeftValue() + 345;
        float lineT = componentHolder.getPosition(end.getParent()).getTopValue() + 15;

        graphicCanvas.beginPath();

        //red color
        graphicCanvas.setStrokeStyle(settings.getConcR(), settings.getConcG(), settings.getConcB());

        graphicCanvas.moveTo(moveL, moveT);
        graphicCanvas.lineTo(lineL, lineT);
        graphicCanvas.stroke();

    }

    /**
     * Creates link between atribute and rule
     *
     * @param start
     * @param rule
     */
    private void drawConditionLink(Panel start, Panel end) {
        float moveL = componentHolder.getPosition(start.getParent()).getLeftValue() + 345;
        float moveT = componentHolder.getPosition(start.getParent()).getTopValue() + 15;

        float lineL = componentHolder.getPosition(end.getParent()).getLeftValue() + 5;
        float lineT = componentHolder.getPosition(end.getParent()).getTopValue() + 15;

        graphicCanvas.beginPath();

        //blue color
        graphicCanvas.setStrokeStyle(settings.getCondR(), settings.getCondG(), settings.getCondB());

        graphicCanvas.moveTo(moveL, moveT);
        graphicCanvas.lineTo(lineL, lineT);
        graphicCanvas.stroke();

    }

    
    /**
     * Creates link between atribute and rule
     *
     * @param start
     * @param rule
     */
    private void drawParentLink(Panel start, Panel end) {
        float moveL = componentHolder.getPosition(start.getParent()).getLeftValue() + 345;
        float moveT = componentHolder.getPosition(start.getParent()).getTopValue() + 15;

        float lineL = componentHolder.getPosition(end.getParent()).getLeftValue() + 5;
        float lineT = componentHolder.getPosition(end.getParent()).getTopValue() + 15;

        graphicCanvas.beginPath();

        //blue color
        graphicCanvas.setStrokeStyle(settings.getRelR(), settings.getRelG(), settings.getRelB());

        graphicCanvas.moveTo(moveL, moveT);
        graphicCanvas.lineTo(lineL, lineT);
        graphicCanvas.stroke();
    }
    
    
     private void drawAndLink(Panel and, Panel master) {
        float moveL = componentHolder.getPosition(and.getParent()).getLeftValue() + 142;
        float moveT = componentHolder.getPosition(and.getParent()).getTopValue() + 15;

        float lineL = componentHolder.getPosition(master.getParent()).getLeftValue() + 5;
        float lineT = componentHolder.getPosition(master.getParent()).getTopValue() + 15;

        graphicCanvas.beginPath();

        //blue color
        graphicCanvas.setStrokeStyle(settings.getRelR(), settings.getRelG(), settings.getRelB());

        graphicCanvas.moveTo(moveL, moveT);
        graphicCanvas.lineTo(lineL, lineT);
        graphicCanvas.stroke();
    }
    
    /**
     * Creates link between atribute and rule
     *
     * @param start
     * @param rule
     */
    private void drawLink(Panel start, Panel end) {
        float moveL = componentHolder.getPosition(start.getParent()).getLeftValue() + 150;
        float moveT = componentHolder.getPosition(start.getParent()).getTopValue() + 50;

        float lineL = componentHolder.getPosition(end.getParent()).getLeftValue() + 150;
        float lineT = componentHolder.getPosition(end.getParent()).getTopValue() + 50;

        graphicCanvas.beginPath();

        graphicCanvas.moveTo(moveL, moveT);
        graphicCanvas.lineTo(lineL, lineT);
        graphicCanvas.stroke();

    }

    /**
     * Class able to handle drops within canvas
     */
    class CanvasMoveHandler implements DropHandler {

        @Override
        public AcceptCriterion getAcceptCriterion() {
            return AcceptAll.get();
        }

        @Override
        public void drop(DragAndDropEvent event) {

            WrapperTransferable transferable;
            transferable = (WrapperTransferable) event.getTransferable();

            WrapperTargetDetails wrapTargetDetails;
            wrapTargetDetails = (WrapperTargetDetails) event.getTargetDetails();

            // Calculate the drag coordinate difference
            int xChange = wrapTargetDetails.getMouseEvent().getClientX()
                    - transferable.getMouseDownEvent().getClientX();
            int yChange = wrapTargetDetails.getMouseEvent().getClientY()
                    - transferable.getMouseDownEvent().getClientY();

            AbsoluteLayout.ComponentPosition componentPosition
                    = componentHolder.getPosition(transferable.getSourceComponent());

            float xFinal = componentPosition.getLeftValue() + xChange;
            float yFinal = componentPosition.getTopValue() + yChange;

            if (xFinal < 0) {
                xFinal = 0;
            }
            if (yFinal < 0) {
                yFinal = 0;
            }
            // Move the component in the absolute layout
            componentPosition.setLeftValue(xFinal);
            componentPosition.setTopValue(yFinal);

            repaint();
        }

    }

    private IConditionElement conditionElement;
    private IConditionMaster conditionMaster;

    private IConclusionElement conclusionElement;
    private IConclusionMaster conclusionMaster;

    public void conditionElementClick(IConditionElement element) {
        if (conditionMaster != null) {
            createConditionLink(element, conditionMaster);
            conditionMaster = null;
        } else {
            conditionElement = element;
        }
        repaint();
    }

    public void conditionMasterClick(IConditionMaster master) {

        if (conditionElement != null) {
            createConditionLink(conditionElement, master);
            conditionElement = null;
        } else {
            conditionMaster = master;
        }
        repaint();
    }

    private void createConditionLink(IConditionElement element, IConditionMaster master) {
        Base base = aplicationView.getBase();
        if (base.getAtributes().contains(element)) { //need to verify, if it wasn't deleted
            master.addConditionElement(element);
        } else if (element.isAlive()) { //must be used because of propositions
            master.addConditionElement(element);
        }
    }

    public void conclusionElementClick(IConclusionElement element) {
        if (conclusionMaster != null) {
            createConclusionLink(element, conclusionMaster);
            conclusionMaster = null;
        } else {
            conclusionElement = element;
        }
        repaint();
    }

    public void conclusionMasterClick(IConclusionMaster master) {

        if (conclusionElement != null) {
            createConclusionLink(conclusionElement, master);
            conclusionElement = null;
        } else {
            conclusionMaster = master;
        }
        repaint();
    }

    private void createConclusionLink(IConclusionElement element, IConclusionMaster master) {
        Base base = aplicationView.getBase();
        if (base.getAtributes().contains(element)) { //need to verify if it wasn't deleted
            master.addConclusionElement(element);
        } else if (element.isAlive()) {
            master.addConclusionElement(element);
        }
    }
    
}

package cz.vse.webz.frontEnd.windows.rules;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.ConditionConjunction;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.rules.Conjunction;
import cz.vse.webz.frontEnd.AplicationLayout;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class EditAND extends Window {

    private FieldGroup binder;

    private boolean save;

    private HorizontalLayout conclusionsPart;

    private BeanItem<Conjunction> bean;

    public EditAND(BeanItem<Conjunction> bean) {
        super(wNameEditAnd);
        this.bean = bean;

        setModal(true);
        setHeight("90%");
        //setWidth("50%");
        addStyleName("window");
        
        GridLayout form = new GridLayout(6, 6);

        Conjunction conjunction = bean.getBean();
        conclusionsPart = createRulePart(conjunction);
        form.addComponent(conclusionsPart);

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 5);
        setContent(form);

        Button discardButton = new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        });
        discardButton.setEnabled(false);

        form.addComponent(discardButton, 1, 5);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            checkTable();
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditAND.class.getName()).log(Level.SEVERE, null, ex);
        }
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    private TreeTable conditionsTreeTable;

    /**
     * *************************CONCLUSIONS/Conditions**************************
     */
    /**
     * *
     * private class that creates panel with all conditions and cocnlusions
     *
     * @param conclusionMaster
     * @return
     */
    private HorizontalLayout createRulePart(Conjunction conjunction) {

        HorizontalLayout rulePartLayout = new HorizontalLayout();

        BeanItemContainer conditionsDatasource = createConditionsDatasource(conjunction);
        conditionsTreeTable = new TreeTable(wTabUsedCond);
        conditionsTreeTable.setContainerDataSource(conditionsDatasource);//, conditionsDatasource);

        /**
         * Lambda overload
         */
        conditionsTreeTable.addGeneratedColumn(wTabDelete, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(final Table source, final Object itemId, Object columnId) {
                return new Button(wTabDeleteButton, e -> {
                    source.removeItem(itemId);
                });
            }
        });

        rulePartLayout.addComponent(conditionsTreeTable);
        conditionsTreeTable.setEditable(true);

        return rulePartLayout;
    }

    /**
     * *******************************CONDITIONS*****************************
     */
    private BeanItemContainer createConditionsDatasource(IConditionMaster rule) {

        BeanItemContainer<Condition> beanContainer;
        beanContainer = new BeanItemContainer<Condition>(Condition.class);

        beanContainer.removeContainerProperty("element");
        beanContainer.removeContainerProperty("master");

        Set<Condition> mainConditions = rule.getAllConditions();
        Object[] mainConditionsArray = mainConditions.toArray();


        Condition condition = null;
        Item item = null;

        for (int j = 0; j < mainConditionsArray.length; j++) {
            condition = (Condition) mainConditionsArray[j];
            item = beanContainer.addItem(condition);

        }

        return beanContainer;

    }

    private void checkTable() {
        Set<Condition> conditions = bean.getBean().getAllConditions();
        Object[] conditionArray = conditions.toArray();

        Condition current = null;
        for (int j = 0; j < conditionArray.length; j++) {
            current = (Condition) conditionArray[j];
            if (conditionsTreeTable.getItemIds().contains(current)) {
            } else {
                bean.getBean().deleteConditionLiteral(current);
            }
        }

    }

}

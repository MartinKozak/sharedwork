/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.windows;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import javax.imageio.ImageIO;

/**
 *
 * @author Martin Kozákk
 */
public class ShowXML extends Window {

    public ShowXML(String inString) {
        super("XML");        
        
        setModal(true);
        setHeight("90%");

        VerticalLayout layout = new VerticalLayout();
        String firstline = "<?xml version=\"1.0\" encoding=\"windows-1250\"?>\n";

        String xmlString = firstline + inString;
        
        Label xmlText = new Label(xmlString, ContentMode.PREFORMATTED);
        layout.addComponent(xmlText);
        setContent(layout);

        Button downloadButton = new Button("Download XML");       
        
        StreamResource myResource = createResource(xmlString);
        FileDownloader fileDownloader = new FileDownloader(myResource);
        fileDownloader.extend(downloadButton);

        layout.addComponent(downloadButton, 0);

    }

    private StreamResource createResource(String xmlText) {
        return new StreamResource(new StreamSource() {
            @Override
            public InputStream getStream() {

                String text = "My image";

                //BufferedImage bi = new BufferedImage(100, 30, BufferedImage.TYPE_3BYTE_BGR);
                //bi.getGraphics().drawChars(text.toCharArray(), 0, text.length(), 10, 20);

                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                   byte buf[] = xmlText.getBytes(Charset.forName("Cp1250"));
                    
                    
                    //ImageIO.write(bi, "xml", bos);
                    bos.write(buf);
                    
                    return new ByteArrayInputStream(bos.toByteArray());

                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }

            }
        }, "webz.xml");
    }

}

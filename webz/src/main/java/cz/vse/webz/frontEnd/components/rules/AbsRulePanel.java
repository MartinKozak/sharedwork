package cz.vse.webz.frontEnd.components.rules;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.CentralPart;
import cz.vse.webz.frontEnd.componentParts.AGraphicPanel;

import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.frontEnd.windows.rules.AbstractEditRule;

/**
 * Abstract Panel that combines features that are same across atribute and
 * proposition panels.
 *
 * @author Martin Kozák
 */
public abstract class AbsRulePanel extends AGraphicPanel {

    private BeanItem<AbstractRule> beanSource;

    private CentralPart centralPart;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public AbsRulePanel(AbstractRule sourceAtribute) {
        this();
        beanSource = new BeanItem<>(sourceAtribute);

        linkThisRule(beanSource);

        sourceAtribute.setID(gID);
        sourceAtribute.setComment(gComment);

    }

    public AbsRulePanel() {
        super();
        centralPart = super.getCentralPart();

        addStyleName("atribute");

        setWidth("350px");
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new AbstractEditRule(beanSource);

        thisUI.addWindow(window);
    }

    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkThisRule(BeanItem<AbstractRule> beanSource) {
        centralPart.getIdLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        centralPart.getTypeLabel().setPropertyDataSource(beanSource.getItemProperty("type"));
        centralPart.getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
    }

    public void relinkRule() {
        linkThisRule(beanSource);
    }

    @Override
    protected void handleDeleteClick() {
        if (beanSource != null) {
            AplicationLayout.getInstance().getBase().removeRule(beanSource.getBean());
        }
        super.handleDeleteClick();

    }

    /**
     * not implemented here
     */
    public void handleNegateClick() {
    }


}

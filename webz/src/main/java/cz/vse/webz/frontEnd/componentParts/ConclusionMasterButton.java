package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.frontEnd.AplicationLayout;
import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class ConclusionMasterButton extends Button{
    IConclusionMaster target;
    
    public ConclusionMasterButton(IConclusionMaster target) {
        super(cMasterConclusion);
        this.target = target;
        
        this.addStyleName("connectButton");
        
        this.addClickListener(e -> buttonClick());
    }
    
        
    /**
     * *
     * Handles the click on conclusion button by calling layout
     */    
    private void buttonClick() {
        AplicationLayout.getInstance().getCanvasLayout().conclusionMasterClick(target);
    }
}


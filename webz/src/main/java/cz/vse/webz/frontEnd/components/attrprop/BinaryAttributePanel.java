package cz.vse.webz.frontEnd.components.attrprop;

import cz.vse.webz.backEnd.attributesPropositions.BinaryAttribute;
import cz.vse.webz.frontEnd.componentParts.ConclusionElementButton;
import cz.vse.webz.frontEnd.componentParts.ConditionElementButton;
import static cz.vse.webz.frontEnd.texts.*;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class BinaryAttributePanel extends AbsAtrPropPanel {

    //private BeanItem<AbstractAtribute> beanSource;
    private BinaryAttribute sourceAtribute;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public BinaryAttributePanel(BinaryAttribute sourceAtribute) {
        super(sourceAtribute);
        this.sourceAtribute = sourceAtribute;

        addStyleName("atribute");

        setWidth("350px");
        sourceAtribute.setID(aBinID);
        sourceAtribute.setName(aBinName);
        sourceAtribute.setComment(aBinComment);

        addBinaryParts();
    }
    
    private void addBinaryParts(){
        //parts of binary atribute and preposition
        ConditionElementButton conditionButton = new ConditionElementButton(sourceAtribute);
        conditionButton.addStyleName("topPart");

        ConclusionElementButton conclusionButton = new ConclusionElementButton(sourceAtribute);
        
        getObjectFieldLayout().addComponent(conclusionButton, 0);
        getObjectFieldLayout().addComponent(conditionButton);
        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }

}

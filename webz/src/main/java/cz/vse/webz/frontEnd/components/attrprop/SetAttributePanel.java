/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.components.attrprop;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.attributesPropositions.SetAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetProposition;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.AddPrepositionButton;
import static cz.vse.webz.frontEnd.texts.*;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class SetAttributePanel extends AbsAtrPropPanel {

    private BeanItem<SetAttribute> beanSource;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public SetAttributePanel(SetAttribute sourceAtribute) {
        super(sourceAtribute);
        beanSource = new BeanItem<>(sourceAtribute);

        addStyleName("atribute");

        setWidth("350px");
        sourceAtribute.setID(aSetID);
        sourceAtribute.setName(aSetName);
        sourceAtribute.setComment(aSetComment);

        addPrepositionButton();
    }

    private void addPrepositionButton() {
        AddPrepositionButton addProposition;
        addProposition = new AddPrepositionButton(this);

        getCentralPart().getLowerButtonPart().addComponent(addProposition, 0);
    }

    @Override
    protected void handleAddPropositionClick() {
        SetAttribute setAtribute = beanSource.getBean();
        SetProposition setProposition = setAtribute.createProposition();
        Panel setPropositionPanel = AplicationLayout.getInstance().getCanvasLayout().createSetProposition(setProposition);
        setProposition.linkWithPanel(setPropositionPanel);
    }

    
      @Override
    protected void handleDeleteClick() {
        super.handleDeleteClick();

        beanSource.getBean().getPropositionSet().stream().forEach((eachProp) -> {
            
            HasComponents parent = eachProp.getPanel().getParent();
            if (parent instanceof DragAndDropWrapper) {
                ComponentContainer grandparent = (ComponentContainer) parent.getParent();
                grandparent.removeComponent(parent);
            } else {
                ComponentContainer containerParent = (ComponentContainer) parent;
                containerParent.removeComponent(eachProp.getPanel());
            }
        });
    }
}

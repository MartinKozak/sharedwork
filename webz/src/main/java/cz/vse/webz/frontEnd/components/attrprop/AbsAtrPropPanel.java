package cz.vse.webz.frontEnd.components.attrprop;


import cz.vse.webz.frontEnd.windows.attrprop.EditAttribute;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.CentralPart;
import cz.vse.webz.frontEnd.componentParts.NameTextpart;
import cz.vse.webz.frontEnd.componentParts.AGraphicPanel;
import static cz.vse.webz.frontEnd.texts.*;

/**
 * Abstract Panel that combines features
 * that are same across atribute and proposition panels. 
 *
 * @author Martin Kozák
 */
public abstract class AbsAtrPropPanel extends AGraphicPanel {

    private BeanItem<AbstractAttribute> beanSource;

    private CentralPart centralPart;
    private NameTextpart namePart;
    
    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public AbsAtrPropPanel(AbstractAttribute sourceAtribute) {
        this();        
        beanSource = new BeanItem<>(sourceAtribute);
        
        linkThisAtribute(beanSource);
      
        sourceAtribute.setID(gID);
        sourceAtribute.setName(gName);
        sourceAtribute.setComment(gComment);
        
    }

    public AbsAtrPropPanel() {
        super();        
        centralPart = super.getCentralPart();
        namePart = new NameTextpart(this);
        
        //replace just ID by NamePart
        centralPart.getCenterGrid().removeComponent(centralPart.getIdLabel());
        centralPart.getCenterGrid().addComponent(namePart, 0, 0);
        centralPart.getTopButtonPart().addComponent(namePart.getToggleName(), 0);
        
        addStyleName("atribute");
        
        setWidth("350px");
    }
            
    
     /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditAttribute(beanSource);

        thisUI.addWindow(window);
    }

    
    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkThisAtribute(BeanItem<AbstractAttribute> beanSource) {
        getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        centralPart.getTypeLabel().setPropertyDataSource(beanSource.getItemProperty("type"));
        centralPart.getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
    }

    public void relinkAtribute() {
        linkThisAtribute(beanSource);
    }

    
    
    
    
    @Override
    protected void handleDeleteClick() {
        if (beanSource != null){
            AplicationLayout.getInstance().getBase().removeAtribute(beanSource.getBean());
        }
        super.handleDeleteClick();
    }

    /**
     * @return the namePart
     */
    protected NameTextpart getNamePart() {
        return namePart;
    }
   

}

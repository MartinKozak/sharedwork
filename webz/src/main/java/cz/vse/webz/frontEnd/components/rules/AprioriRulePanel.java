package cz.vse.webz.frontEnd.components.rules;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.backEnd.rules.AprioriRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.backEnd.rules.Conjunction;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.AndButton;
import cz.vse.webz.frontEnd.componentParts.ConclusionMasterButton;
import cz.vse.webz.frontEnd.componentParts.NegateButton;
import cz.vse.webz.frontEnd.windows.rules.EditAprioryRule;

import static cz.vse.webz.frontEnd.texts.*;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class AprioriRulePanel extends AbsRulePanel {

    //private BeanItem<AbstractAtribute> beanSource;
    //private AprioriRule sourceRule;
    private BeanItem<AprioriRule> beanSource;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public AprioriRulePanel(AprioriRule sourceRule) {
        super(sourceRule);
        //  this.sourceRule = sourceRule;
        this.beanSource = new BeanItem<>(sourceRule);
        addStyleName("atribute");

        HorizontalLayout horLay = new HorizontalLayout(new NegateButton(this));
        getCentralPart().addComponent(horLay);

        setWidth("350px");
        sourceRule.setID(rAprID);
        sourceRule.setComment(rAprComment);

        linkRule(beanSource);

        addAprioriParts();
    }

    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkRule(BeanItem<AprioriRule> beanSource) {
        super.getCentralPart().getIdLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getCentralPart().getTypeLabel().setPropertyDataSource(beanSource.getItemProperty("type"));
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
    }

    public void relinkRule() {
        linkRule(beanSource);
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditAprioryRule(beanSource);

        thisUI.addWindow(window);
    }

    /**
     * *
     * Add parts of apriory rule
     */
    private void addAprioriParts() {
        ConclusionMasterButton conclusionButton = new ConclusionMasterButton(beanSource.getBean());

        getObjectFieldLayout().addComponent(conclusionButton);
        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }

    /**
     * not implemented here
     */
    public void handleNegateClick() {
        AprioriRule apriori = beanSource.getBean();

        Window window;
        window = new NegateWindow(this);
        getUI().addWindow(window);

    }

    private void generateNegative() {

        AbstractRule rule = AplicationLayout.getInstance().getBase().createRule(RuleType.apriori);
        Panel rulePanel = AplicationLayout.getInstance().getCanvasLayout().createRule(rule);
        rule.linkWithPanel(rulePanel);
        AprioriRule newAprioriRule = (AprioriRule) rule;

        String id = rnID + beanSource.getBean().getID();
        String comment = rnComment + beanSource.getBean().getComment();

        newAprioriRule.setID(id);
        newAprioriRule.setComment(comment);

        Object[] conclusions = beanSource.getBean().getAllConclusions().toArray();
        Conclusion conclusion;
        Conclusion newConclusion;
        for (int i = 0; i < conclusions.length; i++) {
            conclusion = (Conclusion) conclusions[i];
            newConclusion = newAprioriRule.addConclusionElement(conclusion.getElement());
            newConclusion.setWeight(conclusion.getWeight());
            newConclusion.setNegation(!conclusion.isNegation());
        }

        AprioriRulePanel aprRule = (AprioriRulePanel) rulePanel;
        aprRule.relinkRule();
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    //Small inner class    
    private static class NegateWindow extends Window {

        private VerticalLayout winLayout = new VerticalLayout();
        private HorizontalLayout buttonLayout = new HorizontalLayout();

        public NegateWindow(AprioriRulePanel masterPanel){
            setCaption(rtNegativeCaption);
            winLayout.addComponent(new Label());
            winLayout.addComponent(buttonLayout);
            setModal(true);
            setResizable(false);
            setContent(winLayout);

            setWidth("185px");

            buttonLayout.setSpacing(true);
            buttonLayout.addComponent(new Button("Negate", e -> {
                masterPanel.generateNegative();
                this.close();
            }));
            buttonLayout.addComponent(new Button("Cancel", e -> {
                this.close();
            }));
        }
    }

}

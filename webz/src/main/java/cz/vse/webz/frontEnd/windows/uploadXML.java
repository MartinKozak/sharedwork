/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.windows;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.ProgressListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.Base;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.utility.XmlGenerator;
import cz.vse.webz.utility.XmlParser;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jsoup.parser.Parser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * inspired by https://gist.github.com/canthony/3655917
 *
 * @author Martin Kozák
 */
public class uploadXML extends Window {

    private File tempFile;

    public uploadXML() {
        super("Upload XML");

        setModal(true);
        setHeight("90%");

        VerticalLayout windowLayout = new VerticalLayout();
        this.setContent(windowLayout);

        this.addCloseListener(e -> AplicationLayout.getInstance().getCanvasLayout().repaint());
        
        Upload upload = new Upload("Upload here", new Receiver() {

            @Override
            public OutputStream receiveUpload(String filename, String mimeType) {
                try {
                    /* Here, we'll stored the uploaded file as a temporary file. No doubt there's
                     a way to use a ByteArrayOutputStream, a reader around it, use ProgressListener (and
                     a progress bar) and a separate reader thread to populate a container *during*
                     the update.
                     This is quick and easy example, though.
                     */
                    tempFile = File.createTempFile("temp", ".xml");
                    return new FileOutputStream(tempFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        upload.addListener(new Upload.FinishedListener() {
            @Override
            public void uploadFinished(Upload.FinishedEvent finishedEvent) {
                try {

                    /* Let's build a container from the CSV File */
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    /**
                     * * because of DTD some variants of xml are not accepted
                     */
                    dbf.setNamespaceAware(true);
                    dbf.setFeature("http://xml.org/sax/features/namespaces", false);
                    dbf.setFeature("http://xml.org/sax/features/validation", false);
                    dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
                    dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

                    DocumentBuilder db = dbf.newDocumentBuilder();

                    Document doc = db.parse(tempFile);

                    XmlParser xmlParser = new XmlParser();
                    Base generatedBase = xmlParser.createBaseFromDOM(doc);

                    if (generatedBase != null) {
                        AplicationLayout.getInstance().setBase(generatedBase);

                        //temp 
                        XmlGenerator xmlGenerator = new XmlGenerator();
                        String xmlString = xmlGenerator.generateXML(generatedBase);
                        Window window = new ShowXML(xmlString);
                        getUI().addWindow(window);
                    }

                    tempFile.delete();

                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(uploadXML.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(uploadXML.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(uploadXML.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        });

        windowLayout.addComponent(upload);
    }

}

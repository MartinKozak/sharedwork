/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.frontEnd.AplicationLayout;
import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class ConclusionElementButton extends Button {
     IConclusionElement target;
    
    public ConclusionElementButton(IConclusionElement target) {
        super(cElementConclusion);
        this.target = target;
        
        this.addStyleName("connectButton");
        
        this.addClickListener(e -> buttonClick());
    }
    
        
    /**
     * *
     * Handles the click on conclusion button by calling layout
     */    
    private void buttonClick() {
        AplicationLayout.getInstance().getCanvasLayout().conclusionElementClick(target);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import cz.vse.webz.frontEnd.components.attrprop.AbsAtrPropPanel;
import cz.vse.webz.frontEnd.components.rules.AbsRulePanel;
import static cz.vse.webz.frontEnd.texts.*;


/**
 *
 * @author Martin Kozák
 */
public class AndButton extends Button {
     
    private AbsRulePanel masterPanel;
    
    public AndButton(AbsRulePanel masterPanel) {
        super(rAddANDButton);
        
        this.masterPanel = masterPanel;
        
        this.setDescription(rAddANDDescription);
        this.addStyleName("propositionButton");
        
        this.addClickListener(e -> buttonClick());
    }
    
        
    /**
     * *
     * Handles the click on conclusion button by calling layout
     */    
    private void buttonClick() {
        masterPanel.handleAddANDClick();
    }
}

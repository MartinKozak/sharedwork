package cz.vse.webz.frontEnd.windows.rules;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.rules.AprioriRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.frontEnd.AplicationLayout;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class EditAprioryRule extends Window {

    private FieldGroup binder;

    private TextField ID = new TextField(wTextID);

    private TextField priority = new TextField(wTextPriority);

    private TextField type = new TextField(wTextType);
    private TextArea comment = new TextArea(wTextComment);

    private CheckBox negativeRule = new CheckBox(wrNegative, false);

    private boolean save;

    private HorizontalLayout conclusionsPart;

    private BeanItem<AprioriRule> bean;

    public EditAprioryRule(BeanItem<AprioriRule> bean) {
        super(wNameEditRule);
        this.bean = bean;
        setModal(true);
        setHeight("90%");
        //setWidth("90%");
        addStyleName("window");
        
        GridLayout form = new GridLayout(6, 6);

        GridLayout infoPart = new GridLayout(2, 4);

        infoPart.addComponent(ID, 0, 0);
        infoPart.addComponent(comment, 0, 2, 1, 2);
        comment.setWidth("200px");
        comment.setHeight("100px");
        infoPart.addComponent(negativeRule, 0, 3);

        form.addComponent(infoPart);

        AprioriRule aprioriRule = bean.getBean();
        conclusionsPart = createRulePart(aprioriRule);
        form.addComponent(conclusionsPart);

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 5);
        setContent(form);

        Button discardButton = new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        });
        discardButton.setEnabled(false);

        form.addComponent(discardButton, 1, 5);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            checkTable();
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditAprioryRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    private Table conclusionsTable;

    /**
     * *************************CONCLUSIONS/Conditions**************************
     */
    /**
     * *
     * private class that creates panel with all conditions and cocnlusions
     *
     * @param conclusionMaster
     * @return
     */
    private HorizontalLayout createRulePart(AprioriRule rule) {

        HorizontalLayout rulePartLayout = new HorizontalLayout();

        Container conclusionsDatasource = createConclusionsDatasource(rule);
        conclusionsTable = new Table(wTabUsedConc, conclusionsDatasource);

        /**Lambda overload*/
        conclusionsTable.addGeneratedColumn(wTabDelete, new ColumnGenerator() {
            @Override
            public Object generateCell(final Table source, final Object itemId, Object columnId) {
                return new Button(wTabDeleteButton, e -> {
                    source.removeItem(itemId);
                });
            }
        });

        rulePartLayout.addComponent(conclusionsTable);
        conclusionsTable.setEditable(true);

        return rulePartLayout;
    }

    /**
     * *******************************CONCLUSIONS*****************************
     */
    private Container createConclusionsDatasource(IConclusionMaster rule) {

        Container container;
        container = new BeanItemContainer<Conclusion>(Conclusion.class);
        container.removeContainerProperty("element");
        container.removeContainerProperty("master");

        Set<Conclusion> conclusions = rule.getAllConclusions();
        Object[] conclusionArray = conclusions.toArray();

        Item item = null;
        Conclusion current = null;

        for (int j = 0; j < conclusionArray.length; j++) {
            current = (Conclusion) conclusionArray[j];
            item = container.addItem(current);
        }
        //      item.getItemProperty(wTabColumnID).setReadOnly(true);
//            item.getItemProperty(wTabColumnID).setValue(current.getElement().getID());
//            item.getItemProperty(wTabColumnValue).setValue(current.getWeight());
        return container;
    }

    private void checkTable() {
        Set<Conclusion> conclusions = bean.getBean().getAllConclusions();
        Object[] conclusionArray = conclusions.toArray();

        Conclusion current = null;
        for (int j = 0; j < conclusionArray.length; j++) {
            current = (Conclusion) conclusionArray[j];
            if (conclusionsTable.getItemIds().contains(current)) {
            } else {
                bean.getBean().deleteConclusionElement(current);
            }
        }

    }

}

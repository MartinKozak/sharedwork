package cz.vse.webz.frontEnd.components.attrprop;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetProposition;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.AddPrepositionButton;
import cz.vse.webz.frontEnd.componentParts.ConclusionElementButton;
import cz.vse.webz.frontEnd.componentParts.ConditionElementButton;
import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.frontEnd.windows.attrprop.EditSetProposition;


/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class SetPropositionPanel extends AbsAtrPropPanel {

    private BeanItem<SetProposition> beanSource;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public SetPropositionPanel(SetProposition sourceProposition) {
        super();
        beanSource = new BeanItem<>(sourceProposition);
        
        sourceProposition.setID(pSetID);
        sourceProposition.setName(pSetName);
        sourceProposition.setComment(pSetComment);

        addConnectors();
      
        super.getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setValue(pSetType);
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));   
    }

    
    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkThisProposition(BeanItem<SetProposition> beanSource) {
        super.getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setValue(pSetType);
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));   
    }
    
    public void relink(){
        linkThisProposition(beanSource);
    }
    
      
    private void addConnectors(){
        //parts of binary atribute and preposition
        ConditionElementButton conditionButton = new ConditionElementButton(beanSource.getBean());
        conditionButton.addStyleName("topPart");

        ConclusionElementButton conclusionButton = new ConclusionElementButton(beanSource.getBean());
        
        getObjectFieldLayout().addComponent(conclusionButton, 0);
        getObjectFieldLayout().addComponent(conditionButton);
        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }
    
    
    
    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditSetProposition(beanSource);

        thisUI.addWindow(window);
    }

    
    
    @Override
    protected void handleDeleteClick() {
        super.handleDeleteClick();
        if (beanSource != null){
            beanSource.getBean().getParentAtribute().removeProposition(beanSource.getBean());
        }
    }

}

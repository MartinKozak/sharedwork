/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 *
 * @author Martin Kozák
 */
public class NameTextpart extends VerticalLayout {

    //basic labels (texts of atributes)
    private final AGraphicPanel masterPanel;

    protected final Label ID;
    private final Label name;

    public final Button toggleName;  

    //private booleans (flags)
    private boolean showName; //flag whether is shown ID/name

    /**
     * Creates basic parts
     */
    public NameTextpart(AGraphicPanel masterPanel) {
        super();
        this.masterPanel = masterPanel;

        // TOP LEFT - NAME / ID*/        

        ID = new Label("New atribute");
        //nameTextPart.addComponent(ID);
        name = new Label("New atribute name");
        name.setVisible(false);
        addComponent(ID);
        addComponent(name);

        
        toggleName = new Button("ID");
        toggleName.addStyleName("nameButton");
        toggleName.setDescription("ID is disiplayed, select to display name.");
        toggleName.addClickListener(e -> {
            changeName();
        });        
        showName = true; //set in default state
    }

    /**
     * @return the nameLabel
     */
    public Label getNameLabel() {
        return name;
    }
    
    /**
     * @return the ID
     */
    public Label getIDLabel() {
        return ID;
    }
    
    private void changeName() {
        masterPanel.setDoubleClickSuppression(false);
        if (showName) {
            getNameLabel().setVisible(true);
            getIDLabel().setVisible(false);
            getToggleName().setDescription("Name is disiplayed, select to display ID.");
            getToggleName().setCaption("NAME");
            showName = false;
        } else {
            getNameLabel().setVisible(false);
            getIDLabel().setVisible(true);
            getToggleName().setDescription("ID is disiplayed, select to display name.");
            getToggleName().setCaption("ID");
            showName = true;
        }
    }

    
    
    
    /**
     * @return the toggleNameButton
     */
    public Button getToggleName() {
        return toggleName;
    }


}

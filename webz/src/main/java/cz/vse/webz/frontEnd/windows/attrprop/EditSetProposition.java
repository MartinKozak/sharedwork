package cz.vse.webz.frontEnd.windows.attrprop;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.SetProposition;
import java.util.logging.Level;
import java.util.logging.Logger;
import static cz.vse.webz.frontEnd.texts.*;
/**
 *
 * @author Martin Kozák
 */
public class EditSetProposition extends Window {

    FieldGroup binder;

    private TextField ID = new TextField(wTextID);

    private TextField name = new TextField(wTextName);
    private TextField type = new TextField(wTextType);
    private TextArea comment = new TextArea(wTextComment);

    private boolean save;

    public EditSetProposition(BeanItem<SetProposition> bean) {
        super(wNameEditProposition);

        setModal(true);
        setHeight("90%");

        GridLayout form = new GridLayout(2, 6);

        form.addComponent(ID, 0, 0);
        form.addComponent(name, 0, 1);
        form.addComponent(comment, 0, 2, 1, 2);
        comment.setWidth("400px");
        comment.setHeight("300px");

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 5);
        setContent(form);

        form.addComponent(new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        }), 1, 5);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditSetProposition.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
package cz.vse.webz.frontEnd.components.rules;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.backEnd.rules.Conjunction;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.AndButton;
import cz.vse.webz.frontEnd.componentParts.ConclusionMasterButton;
import cz.vse.webz.frontEnd.componentParts.ConditionMasterButton;
import cz.vse.webz.frontEnd.componentParts.NegateButton;
import cz.vse.webz.frontEnd.windows.rules.EditLogicalRule;

import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class LogicalRulePanel extends AbsRulePanel {

    private BeanItem<CompleteRule> beanSource;
    private HorizontalLayout horLay;

    public LogicalRulePanel(CompleteRule sourceRule) {
        super(sourceRule);
        this.beanSource = new BeanItem<>(sourceRule);

        horLay = new HorizontalLayout(new AndButton(this), new NegateButton(this));
        getCentralPart().addComponent(horLay);

        addStyleName("atribute");

        setWidth("350px");
        sourceRule.setID(rLogID);
        sourceRule.setComment(rLogComment);

        linkRule(beanSource);

        addCompleteParts();
    }

    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkRule(BeanItem<CompleteRule> beanSource) {
        super.getCentralPart().getIdLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getCentralPart().getTypeLabel().setPropertyDataSource(beanSource.getItemProperty("type"));
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
    }

    public void relinkRule() {
        linkRule(beanSource);
    }

    
    private ConditionMasterButton conditionButton;
    private ConclusionMasterButton conclusionButton;
    /**
     * *
     * Add parts of complete rule
     */
    private void addCompleteParts() {
        conditionButton = new ConditionMasterButton(beanSource.getBean());

        conclusionButton = new ConclusionMasterButton(beanSource.getBean());

        getObjectFieldLayout().addComponent(conditionButton, 0);
        getObjectFieldLayout().addComponent(conclusionButton);
        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditLogicalRule(beanSource, this);

        thisUI.addWindow(window);
    }

    @Override
    protected void handleAddANDClick() {
        Conjunction newConjuction = beanSource.getBean().addConjunction();
        Panel andPanel = AplicationLayout.getInstance().getCanvasLayout().createAND(newConjuction);
        newConjuction.linkWithPanel(andPanel);
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    @Override
    protected void handleDeleteClick() {
        super.handleDeleteClick();

        beanSource.getBean().getAllConjunctions().stream().forEach((eachProp) -> {

            HasComponents parent = eachProp.getPanel().getParent();
            if (parent instanceof DragAndDropWrapper) {
                ComponentContainer grandparent = (ComponentContainer) parent.getParent();
                grandparent.removeComponent(parent);
            } else {
                ComponentContainer containerParent = (ComponentContainer) parent;
                containerParent.removeComponent(eachProp.getPanel());
            }
        });
    }

    public void NegativityToggle(boolean value) {
        if (value) {
            this.getObjectFieldLayout().removeComponent(conditionButton);
            this.getCentralPart().removeComponent(horLay);
        } else {
            this.getObjectFieldLayout().addComponent(conditionButton, 0);
            this.getCentralPart().addComponent(horLay);
        }

    }
    
    
    
    /**
     * not implemented here
     */
    public void handleNegateClick() {
        CompleteRule complete = beanSource.getBean();
        int conditions = complete.getAllConditions().size();
        int conjunctions = complete.getAllConjunctions().size();

        String text = "";
        int variant = 777;

        if (conjunctions == 1 && conditions == 0) {
            variant = 1;
        }

        if (conjunctions == 0) {
            variant = 0;
        }

        if (conjunctions == 0 && conditions == 0){
            variant = 2;
        }
        
        if (conjunctions > 1) {
            variant = 3;
            text = rtNegativeErrOne;
        }

        if (conjunctions > 0 && conditions > 0) {
            variant = 4;
            text = rtNegativeErrTwo;
        }

        Window window;
        window = new NegateWindow(this, text, variant);
        getUI().addWindow(window);

    }

    private void generateNegative(int variant) {

        AbstractRule rule = AplicationLayout.getInstance().getBase().createRule(RuleType.logical);
        Panel rulePanel = AplicationLayout.getInstance().getCanvasLayout().createRule(rule);
        rule.linkWithPanel(rulePanel);
        CompleteRule newCompleteRule = (CompleteRule) rule;

        String id = rnID + beanSource.getBean().getID();
        String comment = rnComment + beanSource.getBean().getComment();
        Double conTresh = beanSource.getBean().getConditionThreshold();
        
        newCompleteRule.setID(id);
        newCompleteRule.setComment(comment);
        newCompleteRule.setConditionThreshold(conTresh);

        Object[] conclusions = beanSource.getBean().getAllConclusions().toArray();
        Conclusion conclusion;
        Conclusion newConclusion;
        for (int i = 0; i < conclusions.length; i++) {
            conclusion = (Conclusion) conclusions[i];
            newConclusion = newCompleteRule.addConclusionElement(conclusion.getElement());
            newConclusion.setWeight(conclusion.getWeight());
            newConclusion.setNegation(!conclusion.isNegation());
        }

        Object[] conditions = beanSource.getBean().getAllConditions().toArray();
        Condition condition;
        Condition newCondition;

        switch (variant) {
            case 0:
                Conjunction newConjuction = newCompleteRule.addConjunction();
                AndPanel andPanel = AplicationLayout.getInstance().getCanvasLayout().createAND(newConjuction);
                newConjuction.linkWithPanel(andPanel);
                
                for (int i = 0; i < conditions.length; i++) {
                    condition = (Condition) conditions[i];
                    newCondition = newConjuction.addConditionElement(condition.getElement());
                    newCondition.setNegation(!condition.isNegation());
                }
                break;
            case 1:
                Object[] oldConjunctions = beanSource.getBean().getAllConjunctions().toArray();                
                Conjunction oldConjunction = (Conjunction)oldConjunctions[0];
                conditions = oldConjunction.getAllConditions().toArray();
                for (int i = 0; i < conditions.length; i++) {
                    condition = (Condition) conditions[i];
                    newCondition = newCompleteRule.addConditionElement(condition.getElement());
                    newCondition.setNegation(!condition.isNegation());
                }
                break;
            case 2:
            case 3:
            case 4:
                break;
        }

        LogicalRulePanel logRule = (LogicalRulePanel) rulePanel;
        logRule.relinkRule();
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    //Small inner class    
    private static class NegateWindow extends Window {

        private VerticalLayout winLayout = new VerticalLayout();
        private HorizontalLayout buttonLayout = new HorizontalLayout();

        public NegateWindow(LogicalRulePanel masterPanel, String text, int variant) {
            setCaption(rtNegativeCaption);
            winLayout.addComponent(new Label(text));
            winLayout.addComponent(buttonLayout);
            setModal(true);
            setResizable(false);
            setContent(winLayout);
            if (variant > 2) {
                setWidth("220px");
            }
            if (variant > 3) {
                setWidth("260px");
            } else {
                setWidth("185px");
            }

            buttonLayout.setSpacing(true);
            buttonLayout.addComponent(new Button("Negate", e -> {
                masterPanel.generateNegative(variant);
                this.close();
            }));
            buttonLayout.addComponent(new Button("Cancel", e -> {
                this.close();
            }));
        }
    }

}

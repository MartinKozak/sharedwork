/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.frontEnd.AplicationLayout;
import static cz.vse.webz.frontEnd.texts.*;
/**
 *
 * @author Martin Kozák
 */
public class ConditionElementButton extends Button {

    IConditionElement target;
    
    public ConditionElementButton(IConditionElement target) {
        super(cElementCondition);
        this.target = target;
        
        this.addStyleName("connectButton");
        
        this.addClickListener(e -> buttonClick());
    }
    
        
    /**
     * *
     * Handles the click on condition button by calling layout
     */    
    private void buttonClick() {
        AplicationLayout.getInstance().getCanvasLayout().conditionElementClick(target);
    }
}

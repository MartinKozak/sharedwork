/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Window;
import cz.vse.webz.frontEnd.components.attrprop.AbsAtrPropPanel;
import cz.vse.webz.frontEnd.components.rules.AbsRulePanel;
import static cz.vse.webz.frontEnd.texts.*;


/**
 *
 * @author Martin Kozák
 */
public class NegateButton extends Button {
     
    private AbsRulePanel masterPanel;
    
    public NegateButton(AbsRulePanel masterPanel) {
        super("Negate");
        
        this.masterPanel = masterPanel;
        
        this.setDescription(rAddANDDescription);
        this.addStyleName("propositionButton");
        
        this.addClickListener(e -> buttonClick());
    }
    
        
    /**
     * *
     * Handles the click on conclusion button by calling layout
     */    
    private void buttonClick() {
         masterPanel.handleNegateClick();
    }
    
    
    
    
    
    
    
}



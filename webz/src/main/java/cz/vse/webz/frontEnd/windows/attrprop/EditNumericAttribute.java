package cz.vse.webz.frontEnd.windows.attrprop;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.NumericAttribute;
import cz.vse.webz.backEnd.enums.Scope;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class EditNumericAttribute extends Window {

    FieldGroup binder;

     private TextField ID = new TextField(wTextID);

    private TextField name = new TextField(wTextName);
    private TextField type = new TextField(wTextType);
    private TextArea comment = new TextArea(wTextComment);

    private OptionGroup scope = new OptionGroup(wTextScope);

    private TextField fuzzyLower = new TextField(wnFuzzyLower);
    private TextField fuzzyUpper = new TextField(wnFuzzyUpper);
        
//    private TextField propositionSet = new TextField("proposition Set");

    private boolean save;

    public EditNumericAttribute(BeanItem<NumericAttribute> bean) {
        super(wNameEditAtribute);

        setLocale(new Locale("cs", "CZ"));
        setModal(true);
        setHeight("90%");

        GridLayout form = new GridLayout(2, 6);

        form.addComponent(ID, 0, 0);
        form.addComponent(name, 0, 1);
        
        fuzzyLower.setConverter(new StringToDoubleConverter());        
        fuzzyUpper.setConverter(new StringToDoubleConverter());
        form.addComponent(fuzzyLower,0,2);
        form.addComponent(fuzzyUpper,1,2);
        
        form.addComponent(comment, 0, 3, 1, 3);
        comment.setWidth("400px");
        comment.setHeight("300px");

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        scope.addItems(Scope.values());
        scope.setData(bean.getBean().getScope());
        form.addComponent(scope, 0, 4);

       

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 5);
        setContent(form);

        form.addComponent(new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        }), 1, 5);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditNumericAttribute.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.frontEnd.AplicationLayout;
import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class ConditionMasterButton extends Button {

    IConditionMaster target;

    public ConditionMasterButton(IConditionMaster target) {
        super(cMasterCondition);
        this.target = target;

        this.addStyleName("connectButton");

        this.addClickListener(e -> buttonClick());
    }

    /**
     * *
     * Handles the click on condition button by calling layout
     */
    private void buttonClick() {
        AplicationLayout.getInstance().getCanvasLayout().conditionMasterClick(target);

    }
}

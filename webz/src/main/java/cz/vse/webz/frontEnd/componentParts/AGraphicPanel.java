/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import cz.vse.webz.frontEnd.AplicationLayout;

/**
 *
 * @author Martin Kozák
 */
public abstract class AGraphicPanel extends Panel {

    private final HorizontalLayout objectFieldLayout; //Layout of whole object (object + surrounding fields)

    private boolean doubleClickSuppressor; //flag that suppress doubleclick on button click

    private final CentralPart centralPart;

    public AGraphicPanel() {
        super();
        setWidthUndefined();
        objectFieldLayout = new HorizontalLayout();
        objectFieldLayout.setWidth("100%");

        centralPart = new CentralPart(this);

        objectFieldLayout.addComponent(centralPart);
        setContent(objectFieldLayout);

        //Listener for editing on doubleclick
        this.addClickListener(event -> {
            if ((event.isDoubleClick()) && (doubleClickSuppressor)) {
                handleEditClick();
            } else {
                doubleClickSuppressor = true;
            }
        });

    }

    protected void handleEditClick() {
    }

    protected void handleConditionClick() {
    }

    protected void handleConclusionClick() {
    }

    protected void handleAddPropositionClick() {
    }
    
    protected void handleAddANDClick() {   
    }
    

    /**
     * @param doubleClickSuppression the down to set
     */
    protected void setDoubleClickSuppression(boolean doubleClickSuppression) {
        this.doubleClickSuppressor = doubleClickSuppression;
    }

    /**
     * Removes the component because the component is wrapped, it needs to call
     * its grandparent to remove its parent
     */
    protected void handleDeleteClick() {

        HasComponents parent = this.getParent();
        if (parent instanceof DragAndDropWrapper) {
            ComponentContainer grandparent = (ComponentContainer) parent.getParent();
            grandparent.removeComponent(parent);
        } else {
            ComponentContainer containerParent = (ComponentContainer) parent;
            containerParent.removeComponent(this);
        }
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    /**
     * @return the centralPart
     */
    protected CentralPart getCentralPart() {
        return centralPart;
    }

    /**
     * @return the down
     */
    public boolean isSupressed() {
        return doubleClickSuppressor;
    }

    /**
     * @return the objectFieldLayout
     */
    protected HorizontalLayout getObjectFieldLayout() {
        return objectFieldLayout;
    }

}

/**
 * atr/prep getCenterGrid().removeComponent(getIdLabel());
 * getCenterGrid().addComponent(nameTextPart, 0, 0);
 */

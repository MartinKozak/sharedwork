/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.componentParts;

import com.vaadin.ui.Button;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.components.attrprop.AbsAtrPropPanel;
import static cz.vse.webz.frontEnd.texts.*;
/**
 *
 * @author Martin Kozák
 */
public class AddPrepositionButton extends Button {
     
    private AbsAtrPropPanel masterPanel;
    
    public AddPrepositionButton(AbsAtrPropPanel masterPanel) {
        super(aAddPrepositionButton);
        
        this.masterPanel = masterPanel;
        
        this.setDescription(aAddPrepositionButtonDescription);
        this.addStyleName("propositionButton");
        
        this.addClickListener(e -> buttonClick());
    }
    
        
    /**
     * *
     * Handles the click on conclusion button by calling layout
     */    
    private void buttonClick() {
        masterPanel.handleAddPropositionClick();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.windows;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import cz.vse.webz.frontEnd.AplicationLayout;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.utility.Settings;
/**
 *
 * @author Martin Kozák
 */
public class EditSettings extends Window {

    FieldGroup binder;

   
    private TextField atributeLeft = new TextField(sAttrLeft);
    private TextField atributeTop = new TextField(sAttrTop);
    
    private TextField ruleLeft = new TextField(sRuleLeft);
    private TextField ruleTop = new TextField(sRuleTop);
    
    
    private TextField condR = new TextField(sCondR);
    private TextField condG = new TextField(sCondG);
    private TextField condB = new TextField(sCondB);
    
    private TextField concR = new TextField(sConcR);
    private TextField concG = new TextField(sConcG);
    private TextField concB = new TextField(sConcB);
    
    private TextField relR = new TextField(sRelR);
    private TextField relG = new TextField(sRelG);
    private TextField relB = new TextField(sRelB);
    
    private boolean save;

    public EditSettings(BeanItem<Settings> bean) {
        super(wNameEditSettings);

        setModal(true);
        setHeight("90%");
        addStyleName("window");
        
        GridLayout form = new GridLayout(4, 13);
        
        form.setSpacing(true);
        
        
        form.addComponent(atributeLeft, 0, 0);        
        form.addComponent(atributeTop, 0, 1);
        form.addComponent(ruleLeft, 1, 0);
        form.addComponent(ruleTop, 1, 1);
        
        
        
        
        form.addComponent(condR, 3, 0);        
        form.addComponent(condG, 3, 1);
        form.addComponent(condB, 3, 2);
        
        form.addComponent(concR, 3, 4);        
        form.addComponent(concG, 3, 5);
        form.addComponent(concB, 3, 6);
        
        form.addComponent(relR, 3, 8);        
        form.addComponent(relG, 3, 9);
        form.addComponent(relB, 3, 10);
        
        
        binder = new FieldGroup();
        binder.setItemDataSource(bean);        
        binder.bindMemberFields(this);


        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 12);
        setContent(form);

        form.addComponent(new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        }), 3, 12);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            binder.commit();
            AplicationLayout.getInstance().getCanvasLayout().repaint();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditSettings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
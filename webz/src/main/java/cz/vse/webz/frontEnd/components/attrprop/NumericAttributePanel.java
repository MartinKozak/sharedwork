/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.components.attrprop;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericProposition;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.componentParts.AddPrepositionButton;
import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.frontEnd.windows.attrprop.EditNumericAttribute;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class NumericAttributePanel extends AbsAtrPropPanel {

    private BeanItem<NumericAttribute> beanSource;

    private HorizontalLayout numericPart;
    private Label fuzzyLower;
    private Label fuzzyUpper;

    public NumericAttributePanel(NumericAttribute sourceAtribute) {
        super(sourceAtribute);
        beanSource = new BeanItem<NumericAttribute>(sourceAtribute);

        addStyleName("atribute");

        setWidth("350px");
        sourceAtribute.setID(aNumID);
        sourceAtribute.setName(aNumName);
        sourceAtribute.setComment(aNumComment);

        addPrepositionButton();

        addNumericParts();

        linkAtribute(beanSource);
        
        fuzzyLower.setConverter(new StringToDoubleConverter());
        fuzzyUpper.setConverter(new StringToDoubleConverter());
        

    }

    
    private void addPrepositionButton() {
        AddPrepositionButton addProposition;
        addProposition = new AddPrepositionButton(this);

        getCentralPart().getLowerButtonPart().addComponent(addProposition, 0);
    }

    @Override
    protected void handleAddPropositionClick() {
        NumericAttribute numericAtribute = beanSource.getBean();
        NumericProposition numericProposition = numericAtribute.createProposition();
        Panel numericPropositionPanel = AplicationLayout.getInstance().getCanvasLayout().createNumericProposition(numericProposition);
        numericProposition.linkWithPanel(numericPropositionPanel);
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditNumericAttribute(beanSource);

        thisUI.addWindow(window);
    }

    @Override
    protected void handleDeleteClick() {
        super.handleDeleteClick();

        beanSource.getBean().getPropositionSet().stream().forEach((eachProp) -> {

            HasComponents parent = eachProp.getPanel().getParent();
            if (parent instanceof DragAndDropWrapper) {
                ComponentContainer grandparent = (ComponentContainer) parent.getParent();
                grandparent.removeComponent(parent);
            } else {
                ComponentContainer containerParent = (ComponentContainer) parent;
                containerParent.removeComponent(eachProp.getPanel());
            }
        });
    }

    private void addNumericParts() {
        numericPart = new HorizontalLayout();
        numericPart.addStyleName("numericPart");
        numericPart.setSizeFull();

        fuzzyLower = new Label();
        fuzzyLower.setCaption(nFuzzyLower);
        fuzzyUpper = new Label();
        fuzzyUpper.setCaption(nFuzzyUpper);

        numericPart.addComponent(fuzzyLower);
        numericPart.addComponent(fuzzyUpper);

        getCentralPart().addComponent(numericPart);
    }

     /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkAtribute(BeanItem<NumericAttribute> beanSource) {
        getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setPropertyDataSource(beanSource.getItemProperty("type"));
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
        fuzzyLower.setPropertyDataSource(beanSource.getItemProperty("fuzzyLower"));
        fuzzyUpper.setPropertyDataSource(beanSource.getItemProperty("fuzzyUpper"));
    }

    @Override
    public void relinkAtribute() {
        linkAtribute(beanSource);
    }

}

package cz.vse.webz.frontEnd.windows.rules;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.components.rules.CompositionalRulePanel;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class EditCompositionalRule extends Window {

    private FieldGroup binder;

    private TextField ID = new TextField(wTextID);

    private TextField priority = new TextField(wTextPriority);

    private TextField type = new TextField(wTextType);
    private TextArea comment = new TextArea(wTextComment);

    private CheckBox negativeRule = new CheckBox(wrNegative, false);

    private boolean save;

    private HorizontalLayout conclusionsPart;

    private BeanItem<CompleteRule> bean;
    
    public EditCompositionalRule(BeanItem<CompleteRule> bean, CompositionalRulePanel rulePanel) {
        super(wNameEditRule);
        this.bean = bean;

        setModal(true);
        setHeight("90%");
        //setWidth("90%");
        addStyleName("window");
        
        GridLayout form = new GridLayout(6, 6);

        GridLayout infoPart = new GridLayout(2, 4);

        infoPart.addComponent(ID, 0, 0);
        infoPart.addComponent(comment, 0, 2, 1, 2);
        comment.setWidth("200px");
        comment.setHeight("100px");
        
        
        if ((bean.getBean().getAllConjunctions().size() < 1)&&(bean.getBean().getAllConditions().size() < 2)){
            negativeRule.addValueChangeListener(e -> rulePanel.NegativityToggle(negativeRule.getValue()));
            infoPart.addComponent(negativeRule, 0, 3);
        }
        

        form.addComponent(infoPart);

        CompleteRule completeRule = (CompleteRule) bean.getBean();
        conclusionsPart = createRulePart(completeRule);
        form.addComponent(conclusionsPart);

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 5);
        setContent(form);

        Button discardButton = new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        });
        discardButton.setEnabled(false);

        form.addComponent(discardButton, 1, 5);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            checkTable();
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(EditCompositionalRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    private Table conclusionsTable;
    private TreeTable conditionsTreeTable;

    /**
     * *************************CONCLUSIONS/Conditions**************************
     */
    /**
     * *
     * private class that creates panel with all conditions and cocnlusions
     *
     * @param conclusionMaster
     * @return
     */
    private HorizontalLayout createRulePart(CompleteRule rule) {

        HorizontalLayout rulePartLayout = new HorizontalLayout();

        BeanItemContainer conditionsDatasource = createConditionsDatasource(rule);
        conditionsTreeTable = new TreeTable(wTabUsedCond);
        conditionsTreeTable.setContainerDataSource(conditionsDatasource);//, conditionsDatasource);

        /**
         * Lambda overload
         */
        conditionsTreeTable.addGeneratedColumn(wTabDelete, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(final Table source, final Object itemId, Object columnId) {
                return new Button(wTabDeleteButton, e -> {
                    source.removeItem(itemId);
                });
            }
        });
        
        rulePartLayout.addComponent(conditionsTreeTable);
        conditionsTreeTable.setEditable(true);

        Container conclusionsDatasource = createConclusionsDatasource(rule);
        conclusionsTable = new Table(wTabUsedConc, conclusionsDatasource);
        
        
        /**Lambda overload*/
        conclusionsTable.addGeneratedColumn(wTabDelete, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(final Table source, final Object itemId, Object columnId) {
                return new Button(wTabDeleteButton, e -> {
                    source.removeItem(itemId);
                });
            }
        });

        
        rulePartLayout.addComponent(conclusionsTable);
        conclusionsTable.setEditable(true);

        return rulePartLayout;
    }

    /**
     * *******************************CONCLUSIONS*****************************
     */
    private Container createConclusionsDatasource(IConclusionMaster rule) {

        Container container;
        container = new BeanItemContainer<Conclusion>(Conclusion.class);
        container.removeContainerProperty("element");
        container.removeContainerProperty("master");

        Set<Conclusion> conclusions = rule.getAllConclusions();
        Object[] conclusionArray = conclusions.toArray();

        Item item = null;
        Conclusion current = null;

        for (int j = 0; j < conclusionArray.length; j++) {
            current = (Conclusion) conclusionArray[j];
            item = container.addItem(current);
        }
        //      item.getItemProperty(wTabColumnID).setReadOnly(true);
//            item.getItemProperty(wTabColumnID).setValue(current.getElement().getID());
//            item.getItemProperty(wTabColumnValue).setValue(current.getWeight());
        return container;
    }

    /**
     * *******************************CONDITIONS*****************************
     */
    private BeanItemContainer createConditionsDatasource(IConditionMaster rule) {

        BeanItemContainer<Condition> beanContainer;
        beanContainer = new BeanItemContainer<Condition>(Condition.class);

        beanContainer.removeContainerProperty("element");
        beanContainer.removeContainerProperty("master");

        

        Set<Condition> mainConditions = rule.getAllConditions();
        Object[] mainConditionsArray = mainConditions.toArray();

        Set<Condition> conditions;
        Object[] conditionsArray;
        
        Condition condition = null;

        Item item = null;

        /*
        for (int i = 0; i < conjuctionsArray.length; i++) {
            conjuction = (ConditionConjunction) conjuctionsArray[i];
            conditions = conjuction.getLiteralList();
            conditionsArray = conditions.toArray();
          */  
            for (int j = 0; j < mainConditionsArray.length; j++) {
                condition = (Condition) mainConditionsArray[j];
                item = beanContainer.addItem(condition);     
                
            }
            
        //}

        return beanContainer;

    }

     private void checkTable() {
        Set<Conclusion> conclusions = bean.getBean().getAllConclusions();
        Object[] conclusionArray = conclusions.toArray();

        Conclusion conclusion = null;
        for (int j = 0; j < conclusionArray.length; j++) {
            conclusion = (Conclusion) conclusionArray[j];
            if (conclusionsTable.getItemIds().contains(conclusion)) {
            } else {
                bean.getBean().deleteConclusionElement(conclusion);
            }
        }

        Set<Condition> conditions = bean.getBean().getAllConditions();
        Object[] conditionArray = conditions.toArray();

        Condition condition = null;
        for (int j = 0; j < conditionArray.length; j++) {
            condition = (Condition) conditionArray[j];
            if (conditionsTreeTable.getItemIds().contains(condition)) {
            } else {
                bean.getBean().deleteConditionLiteral(condition);
            }
        }

    }

     
    
}

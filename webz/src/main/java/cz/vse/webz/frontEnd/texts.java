package cz.vse.webz.frontEnd;

/**
 *
 * @author Martin Kozák
 */
public class texts {
    private texts(){};
    /**void**/
    public static String newName = "text";
    
    /**MyUI - g(enral)*/
    public static String gMainTitle = "WEBZ";
    
    
    /***MenuView* - m(enu)*/

    public static String mMenuTitle = "Menu";
    public static String mCanvasTitle = "Canvas";
    
    public static String mBaseSeparator = "Base";
    
    public static String mNewBaseButton = "New Base";
    public static String mTemplateButton = "Base from Template";
    public static String mImportButton = "Import base from XML";
    public static String mExportButton = "Export base to XML";
    public static String mPropertiesButton = "Edit base properties";
    
    public static String mAttributesSeparator = "Attributes";
    
    public static String mBinaryButton = "New binary attribute";
    public static String mSingleButton = "New single nominal attribute";
    public static String mSetButton = "New set nominal attribute";
    public static String mNumericButton = "New numeric attribute";
    
    public static String mRulesSeparator = "Rules";
    
    public static String mAprioriButton = "New apriori rule";
    public static String mLogicalButton = "New logical rule";
    public static String mCompositionalButton = "New compositional rule";
    public static String mContextButton = "New context";
    
    public static String mSettingsSeparator = "Settings";
    
    public static String mSettingsButton = "Settings";
    public static String mIntConButton = "Edit integrity constrains";
    
    
    
    
    /****BackEnd temporal**** -t(emporal)*/
    public static String tBinID = "Temp_bin_ID";
    
    
    
    
    
    
    /**** Generic - both atribute/ proposition and rule
    /****Generic parts - g(eneric)*/
    public static String gID       = "generic ID";
    public static String gName     = "generic Name";
    public static String gType     = "generic Type";
    public static String gComment  = "generic Commnet";
        
    /*commentButton*/
    public static String gCommentButtoSnhow = "Comment shown";
    public static String gCommentButtonHdie = "Comment hidden";
    
    /* Edit button*/
    public static String gEditButtno = "Edit";
    public static String gEditButtonDescription = "Edit";
    
    
    
    
    
    
    /***Connector**** - c(onnector) */
    public static String cElementConclusion = "conclusion";
    public static String cElementCondition  = "condition";   
    public static String cMasterConclusion  = "conclusions";
    public static String cMasterCondition   = "conditions";    
    
    
    /****Attribute**** - a(tribute)*/    
    
    
    /**ID button**/
    public static String aIDButtonID = "ID";
    public static String aIDButtonName = "Name";   
    
    /* Proposition button*/
    public static String aAddPrepositionButton = "Add proposition";
    public static String aAddPrepositionButtonDescription = "Click to add proposition to this atribute.";     
    
    
    
    /****Attribute**** - bin(ary) */
    public static String aBinID      = "new binary attr ID";
    public static String aBinName    = "new binary attribute Name";
    public static String aBinComment = "This is a binary attribute, it can be true, false or somethnig between.";
    
    
    /****Attribute**** - set */
    public static String aSetID      = "new set attr ID";
    public static String aSetName    = "new set attribute Name";
    public static String aSetComment = "This is a set attribute, it is a set of propositions that act like binary attributes.";
    
    /****Proposition**** - set */
    public static String pSetID      = "new set prop ID";
    public static String pSetName    = "new set proposition Name";
    public static String pSetType    = "Set proposition";
    public static String pSetComment = "This is a set proposition. It act like binary attributes.";
    
    
    
    /****Atribute**** - sin(gle) */
    public static String aSinID      = "new single attr ID";
    public static String aSinName    = "new single Name";
    public static String aSinComment = "This is a single attribute, it is a set of possibilities.";
    
    /****Proposition**** - set */
    public static String pSinID      = "new single prop ID";
    public static String pSinName    = "new single proposition Name";
    public static String pSinType    = "Single proposition";
    public static String pSinComment = "This is a single proposition. It is one of the possibilities of single attribute.";
    
    
    
    /****Numeric parts (fuzzy) - n(numeric */
    public static String nFuzzyLower = "Fuzzy lower:";
    public static String nFuzzyUpper = "Fuzzy upper:";
    public static String nCrispLower = "Crisp lower:";
    public static String nCrispUpper = "Crisp upper:";
            
        
    
    /****Atribute**** - num(eric) */
    public static String aNumID      = "new numeric attr ID";
    public static String aNumName    = "new numeric atribute Name";
    public static String aNumComment = "This is a numeric attribute, it has its range and represents range value.";
    
    /****Proposition**** - num(eric) */
    public static String pNumID      = "new numeric prop ID";
    public static String pNumName    = "new numeric proposition Name";
    public static String pNumType    = "Numeric proposition";
    public static String pNumComment = "This is a numeric proposition. It represents part of range of numeric attribute.";
    
    
    
    /****Rules****/
    
    /***Com(positional rule*/
    public static String rComID       = "compositional rule ID";
    public static String rComComment  = "This is a compositional rule. It is basic type of rule. ";
    
    /***Com(positional rule*/
    public static String rLogID       = "logical rule ID";
    public static String rLogComment  = "This is a logical rule. It has only logical values of conclusion. ";
    
    /***Com(positional rule*/
    public static String rAprID       = "apriori rule ID";
    public static String rAprComment  = "This is a apriori rule. It has only conclusion. ";
    
    
    
    /* Proposition button*/
    public static String rAddANDButton = "AND";
    public static String rAddANDDescription = "Click to add AND panel to this rule.";     
    
    
    public static String rtNegativeCaption = "Create negative rule?";
    public static String rtNegativeErrOne = "Too many conjunctions (AND). Negative rule only with conclusions will be generated. Continue?";
    public static String rtNegativeErrTwo = "Conjunctions (AND) with conditions! Negative rule with only conclusions will be generated. Continue?";
    
    public static String rnID       = "N_";
    public static String rnComment  = "Negative rule. ";
    
    
   /****Windows - w(indow) */
    
    
    public static String wTextID        = "ID";    
    public static String wTextComment   = "comment";
    public static String wTextType      = "type";
    
    public static String wTextName      = "name";
    public static String wTextScope     = "scope";
    public static String wTextPriority  = "priority";
    public static String wTextThreshold = "Condition threshold";
    
    public static String wrNegative   = "Make negative rule";
    
    
    public static String wnFuzzyLower = "fuzzyLower:";
    public static String wnFuzzyUpper = "fuzzyUpper:";
    public static String wnCrispLower = "crispLower:";
    public static String wnCrispUpper = "crispUpper:";
    
    
    public static String wButtonSave    = "Save and close";
    public static String wButtonDiscard = "Discard";
    
    
    
    public static String wTabUsedCond = "Used conditions";
    public static String wTabUsedConc = "Used conclusions";
    
    public static String wTabDelete         = "Delete";
    public static String wTabDeleteButton   = "Delete";
    
    
    public static String wNameEditBase          = "Edit Global base properties";
    public static String wNameEditSettings      = "Setttings";    
    
    public static String wNameEditAtribute      = "Edit atribute";
    public static String wNameEditProposition   = "Edit proposition";
    public static String wNameEditRule          = "Edit rule";
    
    public static String wNameEditAnd           = "Edit AND";
    
    
    
    /***Base Window***/
    
    public static String wbDescription              = "Description of base";
    public static String wbExpert                   = "Expert";
    public static String wbKnowledgeEngineer        = "Knowledge engineer";
    public static String wbDate                     = "Date";

    public static String wbInferenceMechanism       = "Inference mechanism";
    public static String wbDefaultWeight            = "Default weight";
    public static String wbGlobalPriority           = "Global Priority";

    public static String wbWeightRange              = "Weight range";
    public static String wbContextGlobalThreshold   = "Global context threshold";
    public static String wbConditionGlobalThreshold = "Global condition threshold";
    
    
    
    /*
    public static String wTabColumnID       = "ID";
    public static String wTabColumnValue    = "Value";
    public static String wTabColumnNegative = "Negative";
    public static String wTabColumnRemove   = "Remove";
    
    public static String wTabColumnInvis  = "ID";
     */
    
    
    /****Settings - s(ettings) */
    
   public static String sAttrLeft    = "Attribute LEFT position";
   public static String sAttrTop     = "Attribute TOP position";
   public static String sRuleLeft   = "Rule LEFT position";
   public static String sRuleTop    = "Rule LEFT position";
    
   public static String sCondR  = "Red color part of Condition link";
   public static String sCondG  = "Green color part of Condition link";
   public static String sCondB  = "Blue color part of Condition link";
   
   public static String sConcR  = "Red color part of Conclusion link";
   public static String sConcG  = "Green color part of Conclusion link";
   public static String sConcB  = "Blue color part of Conclusion link";
   
   public static String sRelR  = "Red color part of Conjunction link";
   public static String sRelG  = "Green color part of Conjunction link";
   public static String sRelB  = "Blue color part of Conjunction link";
   
    
    
    
    
}

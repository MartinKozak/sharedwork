package cz.vse.webz.frontEnd.windows.rules;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.frontEnd.AplicationLayout;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.webz.frontEnd.texts.*;

/**
 *
 * @author Martin Kozák
 */
public class AbstractEditRule extends Window {

    private FieldGroup binder;

    private GridLayout form;
        
     private TextField ID = new TextField(wTextID);

    private TextField priority = new TextField(wTextPriority);

    private TextField type = new TextField(wTextType);
    private TextArea comment = new TextArea(wTextComment);

    private CheckBox negativeRule = new CheckBox(wrNegative, false);
    
    
    private boolean save;
    

    public AbstractEditRule(BeanItem<AbstractRule> bean) {
        super(wNameEditRule);

        setModal(true);
        setHeight("90%");
        //setWidth("90%");
        
        form = new GridLayout(6, 6);

        GridLayout infoPart = new GridLayout(2, 4);

        infoPart.addComponent(ID, 0, 0);
        infoPart.addComponent(comment, 0, 2, 1, 2);
        comment.setWidth("200px");
        comment.setHeight("100px");
        infoPart.addComponent(negativeRule, 0, 3);

        form.addComponent(infoPart);

        binder = new FieldGroup();
        binder.setItemDataSource(bean);
        binder.bindMemberFields(this);

        form.addComponent(new Button(wButtonSave, e -> {
            save = true;
            this.close();
        }), 0, 5);
        setContent(form);

        Button discardButton = new Button(wButtonDiscard, e -> {
            save = false;
            this.close();
        });
        discardButton.setEnabled(false);

        form.addComponent(discardButton, 1, 5);
        setContent(form);
        save = true;

        this.addCloseListener(e -> exitFN());
    }

    private void exitFN() {
        if (save) {
            saveIt();
        }
    }

    private void saveIt() {
        try {
            binder.commit();
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(AbstractEditRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        AplicationLayout.getInstance().getCanvasLayout().repaint();
    }

    /**
     * @return the form
     */
    public GridLayout getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(GridLayout form) {
        this.form = form;
    }

   
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.frontEnd.components.attrprop;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.attributesPropositions.SingleProposition;
import cz.vse.webz.frontEnd.windows.attrprop.EditSingleProposition;
import cz.vse.webz.frontEnd.componentParts.ConditionElementButton;
import static cz.vse.webz.frontEnd.texts.*;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class SinglePropositionPanel extends AbsAtrPropPanel {

    private BeanItem<SingleProposition> beanSource;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public SinglePropositionPanel(SingleProposition sourceAtribute) {
        super();
        beanSource = new BeanItem<>(sourceAtribute);

        sourceAtribute.setID(pSinID);
        sourceAtribute.setName(pSinName);
        sourceAtribute.setComment(pSinComment);

        addConnectors();

        super.getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setValue(pSinType);
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));
    }

    /**
     * Links AtributePanel and atribute itself, represented by BeanItem
     *
     * @param beanSource
     */
    protected void linkThisProposition(BeanItem<SingleProposition> beanSource) {
        super.getNamePart().getIDLabel().setPropertyDataSource(beanSource.getItemProperty("ID"));
        super.getNamePart().getNameLabel().setPropertyDataSource(beanSource.getItemProperty("name"));
        super.getCentralPart().getTypeLabel().setValue(pSetType);
        super.getCentralPart().getCommentLabel().setPropertyDataSource(beanSource.getItemProperty("comment"));   
    }
    
    public void relink(){
        linkThisProposition(beanSource);
    }
    
    
    private void addConnectors() {
        //parts of binary atribute and preposition
        ConditionElementButton conditionButton = new ConditionElementButton(beanSource.getBean());
        conditionButton.addStyleName("topPart");

        getObjectFieldLayout().addComponent(conditionButton);
        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditSingleProposition(beanSource);

        thisUI.addWindow(window);
    }

    @Override
    protected void handleDeleteClick() {
        super.handleDeleteClick();
        if (beanSource != null) {
            beanSource.getBean().getParentAtribute().removeProposition(beanSource.getBean());
        }
    }

}

package cz.vse.webz.frontEnd;

import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.Base;
import cz.vse.webz.backEnd.Globals;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.frontEnd.windows.EditBase;
import static cz.vse.webz.frontEnd.texts.*;
import cz.vse.webz.frontEnd.windows.EditSettings;
import cz.vse.webz.frontEnd.windows.ShowXML;
import cz.vse.webz.frontEnd.windows.uploadXML;
import cz.vse.webz.utility.Settings;
import cz.vse.webz.utility.XmlGenerator;

/**
 * Menu layout.
 *
 * @author Martin Kozák
 */
public class MenuLayout extends VerticalLayout {

    private final AplicationLayout aplicationView;
    private final CanvasLayout canvasView;

    public MenuLayout(AplicationLayout aplicationView) {
        super();
        this.aplicationView = aplicationView;
        canvasView = aplicationView.getCanvasLayout();

        createMenu();
        
        setMargin(false);
    }

    
    /**
     * Create menu
     *
     * @return menu itself
     */
    private void createMenu() {

        this.addComponent(new Label(mBaseSeparator));

        this.addComponent(
                new MenuButton(mNewBaseButton, (e -> {
                    createNewBase();
                })));

        this.addComponent(
                new MenuButton(mTemplateButton, e -> {
                    loadTemplate();
                }));

        this.addComponent(
                new MenuButton(mImportButton, e -> {
                    importBase();
                }));

        this.addComponent(
                new MenuButton(mExportButton, e -> {
                    exportXML();
                }));

        this.addComponent(new Label("<br />", ContentMode.HTML));

        this.addComponent(
                new MenuButton(mPropertiesButton, e -> {
                    showProperties();
                }));

        this.addComponent(new Label("<hr />", ContentMode.HTML));

        this.addComponent(new Label(mAttributesSeparator));

        this.addComponent(new MenuButton(mBinaryButton, e -> {
            createAtribute(AttributeType.binary);
        }));

        this.addComponent(new MenuButton(mSingleButton, e -> {
            createAtribute(AttributeType.single);
        }));

        this.addComponent(new MenuButton(mSetButton, e -> {
            createAtribute(AttributeType.set);
        }));

        this.addComponent(new MenuButton(mNumericButton, e -> {
            createAtribute(AttributeType.numeric);
        }));

        this.addComponent(new Label("<hr />", ContentMode.HTML));

        this.addComponent(new Label(mRulesSeparator));

        this.addComponent(
                new MenuButton(mAprioriButton, e -> {
                    createRule(RuleType.apriori);
                }));

        this.addComponent(
                new MenuButton(mLogicalButton, e -> {
                    createRule(RuleType.logical);
                }));

        this.addComponent(
                new MenuButton(mCompositionalButton, e -> {
                    createRule(RuleType.compositional);
                }));

        this.addComponent(new Label("<hr />", ContentMode.HTML));
        /*
         this.addComponent(
         new MenuButton(mContextButton, e -> {
         createContext();
         },false));
         */
        this.addComponent(new Label("<hr />", ContentMode.HTML));

        this.addComponent(new Label(mSettingsSeparator));

        this.addComponent(
                new MenuButton(mSettingsButton, e -> {
                    showSettings();
                }));

        /*this.addComponent(
         new MenuButton(mIntConButton, e -> {
         createIntCon();
         },false));
         */
        this.addComponent(new Label("<hr />", ContentMode.HTML));

    }

    /**
     * *
     * Clears canvasView and sets a new base
     */
    private void createNewBase() {
        aplicationView.setBase(new Base());
        canvasView.clearCanvas();
    }

    private void loadTemplate() {
        canvasView.createTestObjects();
    }

    private void importBase() {
        Window window = new uploadXML();
        getUI().addWindow(window);
    }

    private void exportXML() {
        XmlGenerator xmlGenerator = new XmlGenerator();
        String xmlString = xmlGenerator.generateXML(aplicationView.getBase());
        Window window = new ShowXML(xmlString);
        getUI().addWindow(window);
    }

    private void showProperties() {
        UI thisUI = getUI();
        BeanItem<Globals> bean = new BeanItem<>(aplicationView.getBase().getGlobals());
        Window window = new EditBase(bean);
        thisUI.addWindow(window);
    }

    private void createAtribute(AttributeType atributeType) {
        AbstractAttribute atribute = aplicationView.getBase().createAtribute(atributeType);
        Panel atributePanel = canvasView.createAtribute(atribute);
        atribute.linkWithPanel(atributePanel);
    }

    private void createRule(RuleType ruleType) {
        AbstractRule rule = aplicationView.getBase().createRule(ruleType);
        Panel rulePanel = canvasView.createRule(rule);
        rule.linkWithPanel(rulePanel);
    }

    private void createContext() {
//        Context context = aplicationView.getBase().createContext();

    }

    private void createIntCon() {
        //      IntegrityConstraint intCon = aplicationView.getBase().createIntegrityConstraint();
    }

    private void showSettings() {
        UI thisUI = getUI();
        BeanItem<Settings> bean = new BeanItem<>(aplicationView.getSettings());
        Window window = new EditSettings(bean);
        thisUI.addWindow(window);
    }

    //Small inner button class
    private class MenuButton extends Button {

        public MenuButton(String caption, Button.ClickListener listener) {
            this(caption, listener, true);
        }

        public MenuButton(String caption, Button.ClickListener listener, boolean enable) {
            super(caption, listener);
            this.addStyleName("menuButton");
            setEnabled(enable);
        }

    }

    /**
     * ******************* Buttons ****************************
     */
}

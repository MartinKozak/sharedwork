package cz.vse.webz.frontEnd.components.rules;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import cz.vse.webz.backEnd.rules.Conjunction;
import cz.vse.webz.frontEnd.componentParts.ConditionMasterButton;
import cz.vse.webz.frontEnd.windows.rules.EditAND;

import static cz.vse.webz.frontEnd.texts.*;

/**
 * Atribute Panel that represents the actual panel. extends AGraphicAtribute
 * which is a class made purely for generating graphic parts
 *
 * @author Martin Kozák
 */
public class AndPanel extends AbsRulePanel {

    private BeanItem<Conjunction> beanSource;

    //Optional parts
    //  private final VerticalLayout topConditionPart;
    //  private final VerticalLayout lowerConclusionPart;
    public AndPanel(Conjunction source) {
        super();
        this.beanSource = new BeanItem<>(source);

        //change the style into simplistic AND
        super.getCentralPart().reAnd(this);

        addStyleName("AND");

        setWidth("170px");

        addANDParts();
    }

    /**
     * *
     * Add parts of complete rule
     */
    private void addANDParts() {
        ConditionMasterButton conditionButton = new ConditionMasterButton(beanSource.getBean());

        getObjectFieldLayout().addComponent(conditionButton, 0);

        getObjectFieldLayout().setExpandRatio(getCentralPart(), 0.1f);
    }

    /**
     * *
     * Opens the window editing the atribute
     */
    @Override
    protected void handleEditClick() {
        UI thisUI = getUI();
        Window window;

        window = new EditAND(beanSource);

        thisUI.addWindow(window);
    }

    
    @Override
    protected void handleDeleteClick() {
        if (beanSource != null) {
            beanSource.getBean().getParent().deleteConjunction(beanSource.getBean());
        }
        super.handleDeleteClick();

    }

   
}

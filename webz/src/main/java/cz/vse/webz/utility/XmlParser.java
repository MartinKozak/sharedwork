package cz.vse.webz.utility;

import com.vaadin.ui.Panel;
import cz.vse.webz.backEnd.Base;
import cz.vse.webz.backEnd.Globals;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.attributesPropositions.BinaryAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericProposition;
import cz.vse.webz.backEnd.attributesPropositions.SetAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetProposition;
import cz.vse.webz.backEnd.attributesPropositions.SingleAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SingleProposition;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionElement;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionElement;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.IConjunctionMaster;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.enums.Scope;
import cz.vse.webz.backEnd.enums.globals.DefaultWeight;
import cz.vse.webz.backEnd.enums.globals.GlobalPriority;
import cz.vse.webz.backEnd.enums.globals.InferenceMechanism;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.backEnd.rules.AprioriRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.backEnd.rules.Conjunction;
import cz.vse.webz.frontEnd.AplicationLayout;
import cz.vse.webz.frontEnd.CanvasLayout;
import cz.vse.webz.frontEnd.components.attrprop.AbsAtrPropPanel;
import cz.vse.webz.frontEnd.components.attrprop.BinaryAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.NumericAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.NumericPropositionPanel;
import cz.vse.webz.frontEnd.components.attrprop.SetAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.SetPropositionPanel;
import cz.vse.webz.frontEnd.components.attrprop.SingleAttributePanel;
import cz.vse.webz.frontEnd.components.attrprop.SinglePropositionPanel;
import cz.vse.webz.frontEnd.components.rules.AprioriRulePanel;
import cz.vse.webz.frontEnd.components.rules.CompositionalRulePanel;
import cz.vse.webz.frontEnd.components.rules.LogicalRulePanel;
import cz.vse.webz.frontEnd.windows.ShowXML;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Martin Kozák
 */
public class XmlParser {

    private Base base;
    private AplicationLayout aplication;
    private CanvasLayout canvas;

    /**
     * *
     * ProzatĂ­m jen zobrazĂ­
     *
     * @param domBase
     */
    public Base createBaseFromDOM(Document domBase) {

        base = new Base();
        aplication = AplicationLayout.getInstance();
        canvas = aplication.getCanvasLayout();
        canvas.clearCanvas();

        fillGlobals(domBase, base);

        fillAtributes(domBase, base);

        fillAprioryRules(domBase, base);
        fillLogicalRules(domBase, base);
        fillCompositionalRules(domBase, base);

        return base;

    }

    private void fillAprioryRules(Document doc, Base base) {

        NodeList aprioryRules = doc.getElementsByTagName("apriori_rule");

        Element ruleElement;
        for (int i = 0; i < aprioryRules.getLength(); i++) {

            ruleElement = (Element) aprioryRules.item(i);

            AbstractRule rule = base.createRule(RuleType.apriori);
            Panel rulePanel = canvas.createRule(rule);
            rule.linkWithPanel(rulePanel);

            fillBasicRuleInfo(ruleElement, rule);

            AprioriRulePanel aprRule = (AprioriRulePanel) rulePanel;
            aprRule.relinkRule();

            AprioriRule aprrule = (AprioriRule) rule;
            fillConclusions(ruleElement, aprrule);
            
        }

    }

    private void fillLogicalRules(Document doc, Base base) {

        NodeList rules = doc.getElementsByTagName("logical_rule");

        Element ruleElement;
        for (int i = 0; i < rules.getLength(); i++) {

            ruleElement = (Element) rules.item(i);

            AbstractRule rule = base.createRule(RuleType.logical);
            Panel rulePanel = canvas.createRule(rule);
            rule.linkWithPanel(rulePanel);

            fillBasicRuleInfo(ruleElement, rule);

            LogicalRulePanel logRule = (LogicalRulePanel) rulePanel;
            logRule.relinkRule();

            CompleteRule comrule = (CompleteRule) rule;
            fillConditions(ruleElement, comrule);
            fillConclusions(ruleElement, comrule);
        }
    }

    private void fillCompositionalRules(Document doc, Base base) {

        NodeList rules = doc.getElementsByTagName("compositional_rule");

        Element ruleElement;
        for (int i = 0; i < rules.getLength(); i++) {
            ruleElement = (Element) rules.item(i);

            AbstractRule rule = base.createRule(RuleType.compositional);
            Panel rulePanel = canvas.createRule(rule);
            rule.linkWithPanel(rulePanel);

            fillBasicRuleInfo(ruleElement, rule);

            CompositionalRulePanel copRule = (CompositionalRulePanel) rulePanel;
            copRule.relinkRule();

            CompleteRule comrule = (CompleteRule) rule;
            fillConditions(ruleElement, comrule);
            fillConclusions(ruleElement, comrule);
        }

    }

    private void fillConclusions(Element ruleElement, IConclusionMaster master) {

        NodeList conclusions = ruleElement.getElementsByTagName("conclusion");

        Element conjunction;

        Conclusion conclusion = null;

        for (int i = 0; i < conclusions.getLength(); i++) {
            conjunction = (Element) conclusions.item(i);

            String id = parseText(conjunction, "id_attribute");
            String idProp = parseText(conjunction, "id_proposition");
            String negation = parseText(conjunction, "negation");


            IConclusionElement proposition;

            AbstractAttribute attribute = base.getAttributeByID(id);
            if (attribute == null) {
                return;
            }
            switch (attribute.getType()) {
                case binary:
                    BinaryAttribute binAtribute = (BinaryAttribute) attribute;
                    conclusion = master.addConclusionElement(binAtribute);
                    break;
                case set:
                    SetAttribute setAttribute = (SetAttribute) attribute;
                    proposition = setAttribute.getPropositionByID(idProp);
                    if (proposition == null) {
                        return;
                    }
                    conclusion = master.addConclusionElement(proposition);
                    break;
            }

            if (negation.equalsIgnoreCase("1")) {
                conclusion.setNegation(true);
            }
            
            
            try {
                String text = parseText(conjunction, "weight");

                NumberFormat format = NumberFormat.getInstance(new Locale("cs", "CZ"));
                Number number = format.parse(text);
                double weight = number.doubleValue();
                
                conclusion.setWeight(weight);
                
            } catch (IllegalArgumentException ex) {
                // Do nothing, can be wrong
            } catch (ParseException ex) {
            // Also do nothing, will not be used
                //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
            }
            

        }
    }

    private void fillConditions(Element ruleElement, CompleteRule rule) {

        NodeList conjunctions = ruleElement.getElementsByTagName("conjunction");

        Element conjunction;
        NodeList literals;
        Element literal;

        for (int i = 0; i < conjunctions.getLength(); i++) {
            conjunction = (Element) conjunctions.item(i);
            literals = conjunction.getElementsByTagName("literal");
            if (literals.getLength() == 1) {
                literal = (Element) literals.item(0);
                fillDisjunction(literal, rule);
            } else {
                fillConjunction(literals, rule);
            }
        }
    }

    private void fillDisjunction(Element disjunction, IConditionMaster master) {

        Condition condition = null;

        String id = parseText(disjunction, "id_attribute");
        String idProp = parseText(disjunction, "id_proposition");
        String negation = parseText(disjunction, "negation");
        IConditionElement proposition;

        AbstractAttribute attribute = base.getAttributeByID(id);
        if (attribute == null) {
            return;
        }
        switch (attribute.getType()) {
            case binary:
                BinaryAttribute binAtribute = (BinaryAttribute) attribute;
                condition = master.addConditionElement(binAtribute);
                break;
            case set:
                SetAttribute setAttribute = (SetAttribute) attribute;
                proposition = setAttribute.getPropositionByID(idProp);
                if (proposition == null) {
                    return;
                }
                condition = master.addConditionElement(proposition);
                break;
            case single:
                SingleAttribute sinAttribute = (SingleAttribute) attribute;
                proposition = sinAttribute.getPropositionByID(idProp);
                if (proposition == null) {
                    return;
                }
                condition = master.addConditionElement(proposition);
                break;
            case numeric:
                NumericAttribute numAttribute = (NumericAttribute) attribute;
                proposition = numAttribute.getPropositionByID(idProp);
                if (proposition == null) {
                    return;
                }
                condition = master.addConditionElement(proposition);
                break;
        }
        if (negation.equalsIgnoreCase("1")) {
            condition.setNegation(true);
        }

    }

    private void fillConjunction(NodeList literals, IConjunctionMaster rule) {

        Conjunction newConjuction = rule.addConjunction();
        Panel andPanel = AplicationLayout.getInstance().getCanvasLayout().createAND(newConjuction);
        newConjuction.linkWithPanel(andPanel);

        Element literal;

        for (int i = 0; i < literals.getLength(); i++) {
            literal = (Element) literals.item(i);
            fillDisjunction(literal, newConjuction);
        }
    }

    private void fillBasicRuleInfo(Element ruleElement, AbstractRule rule) {

        String id = parseText(ruleElement, "id");
        rule.setID(id);

        String comment = parseText(ruleElement, "comment");
        rule.setComment(comment);

    }

    /**
     * *
     * Private function that takes atributes out of document and fill it into
     * base
     *
     * @param doc document from which it takes atributes
     * @param base base that the atribute is filled into
     */
    private void fillAtributes(Document doc, Base base) {
        Set<AbstractAttribute> atributeSet = base.getAtributes();

        NodeList atributes = doc.getElementsByTagName("attribute");

        Element atributeElement;
        for (int i = 0; i < atributes.getLength(); i++) {

            atributeElement = (Element) atributes.item(i);
            //if unreadable, create binary atribute
            AttributeType type = AttributeType.binary;
            try {
                String typeString = parseText(atributeElement, "type");
                if (typeString.equalsIgnoreCase("multiple")) {
                    type = AttributeType.set;
                } else {
                    type = AttributeType.valueOf(parseText(atributeElement, "type"));
                }

            } catch (IllegalArgumentException ex) {
                System.err.println("Type written badly!");
            }

            AbstractAttribute atribute = base.createAtribute(type);
            AbsAtrPropPanel atributePanel = canvas.createAtribute(atribute);
            atribute.linkWithPanel(atributePanel);

            switch (type) {
                case binary:
                    fillBasicAtributeInfo(atributeElement, atribute);
                    atributePanel.relinkAtribute();
                    break;
                case set:
                    fillBasicAtributeInfo(atributeElement, atribute);
                    atributePanel.relinkAtribute();
                    fillPropositions(atributeElement, atribute);
                    break;
                case single:
                    fillBasicAtributeInfo(atributeElement, atribute);
                    atributePanel.relinkAtribute();
                    fillPropositions(atributeElement, atribute);
                    break;
                case numeric:
                    NumericAttribute numAtribute = (NumericAttribute) atribute;
                    NumericAttributePanel numPanel = (NumericAttributePanel) atributePanel;
                    fillBasicAtributeInfo(atributeElement, numAtribute);
                    fillNumericAtributeInfo(atributeElement, numAtribute);
                    numPanel.relinkAtribute();
                    fillPropositions(atributeElement, atribute);
                    break;
            }
        }
    }

    private void fillBasicAtributeInfo(Element atributeElement, AbstractAttribute atribute) {

        String id = parseText(atributeElement, "id");
        atribute.setID(id);

        String name = parseText(atributeElement, "name");
        atribute.setName(name);

        String comment = parseText(atributeElement, "comment");
        atribute.setComment(comment);

        String text = parseText(atributeElement, "scope");
        if (text.equalsIgnoreCase("environment")) {
            atribute.setScope(Scope.environmental);
        }

    }

    private void fillNumericAtributeInfo(Element atributeElement, NumericAttribute numAtribute) {
        /*
         NodeList legals = atributeElement.getElementsByTagName("legal_values");
         Node legal = legals.item(0);
         Element legVal = (Element)legal;
         */

        String text;
        try {
            text = parseText(atributeElement, "lower_bound");
            NumberFormat format = NumberFormat.getInstance(new Locale("cs", "CZ"));
            Number number = format.parse(text);
            Double lower = number.doubleValue();
            numAtribute.setFuzzyLower(lower);

            text = parseText(atributeElement, "upper_bound");
            number = format.parse(text);
            Double upper = number.doubleValue();
            numAtribute.setFuzzyUpper(upper);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        } catch (ParseException ex) {
            Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * *
     * Private function that takes atributes out of document and fill it into
     * base
     *
     * @param doc document from which it takes atributes
     * @param base base that the atribute is filled into
     */
    private void fillPropositions(Element atributeElement, AbstractAttribute attribute) {
        NodeList propositions = atributeElement.getElementsByTagName("proposition");

        Element propositionElement;
        for (int i = 0; i < propositions.getLength(); i++) {

            propositionElement = (Element) propositions.item(i);
            //if unreadable, create binary atribute

            switch (attribute.getType()) {
                case set:
                    SetAttribute setAttribute = (SetAttribute) attribute;
                    SetProposition setProp = setAttribute.createProposition();
                    SetPropositionPanel setPanel = canvas.createSetProposition(setProp);
                    setProp.linkWithPanel(setPanel);
                    fillSetPropositionInfo(propositionElement, setProp);
                    setPanel.relink();
                    break;
                case single:
                    SingleAttribute singleAttribute = (SingleAttribute) attribute;
                    SingleProposition sinProp = singleAttribute.createProposition();
                    SinglePropositionPanel sinPanel = canvas.createSingleProposition(sinProp);
                    sinProp.linkWithPanel(sinPanel);
                    fillSinPropositionInfo(propositionElement, sinProp);
                    sinPanel.relink();
                    break;
                case numeric:
                    NumericAttribute numAtribute = (NumericAttribute) attribute;
                    NumericProposition numProp = numAtribute.createProposition();
                    NumericPropositionPanel numPanel = canvas.createNumericProposition(numProp);
                    numProp.linkWithPanel(numPanel);
                    fillNumPropositionInfo(propositionElement, numProp);
                    numPanel.relink();
                case binary:
                default:
                    //error
                    break;
            }
        }
    }

    private void fillNumPropositionInfo(Element propositionElement, NumericProposition proposition) {
        String id = parseText(propositionElement, "id");
        proposition.setID(id);

        String name = parseText(propositionElement, "name");
        proposition.setName(name);

        String comment = parseText(propositionElement, "comment");
        proposition.setComment(comment);

        String text;
        NumberFormat format;
        Number number;
        try {
            text = parseText(propositionElement, "fuzzy_lower_bound");
            format = NumberFormat.getInstance(new Locale("cs", "CZ"));
            number = format.parse(text);
            Double lower = number.doubleValue();
            proposition.setFuzzyLower(lower);

            text = parseText(propositionElement, "crisp_lower_bound");
            format = NumberFormat.getInstance(new Locale("cs", "CZ"));
            number = format.parse(text);
            Double lowerCrisp = number.doubleValue();
            proposition.setCrispLower(lowerCrisp);

            text = parseText(propositionElement, "crisp_upper_bound");
            format = NumberFormat.getInstance(new Locale("cs", "CZ"));
            number = format.parse(text);
            Double upperCrisp = number.doubleValue();
            proposition.setCrispUpper(upperCrisp);

            text = parseText(propositionElement, "fuzzy_upper_bound");
            number = format.parse(text);
            Double upper = number.doubleValue();
            proposition.setFuzzyUpper(upper);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        } catch (ParseException ex) {
            Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void fillSinPropositionInfo(Element propositionElement, SingleProposition proposition) {
        String id = parseText(propositionElement, "id");
        proposition.setID(id);

        String name = parseText(propositionElement, "name");
        proposition.setName(name);

        String comment = parseText(propositionElement, "comment");
        proposition.setComment(comment);
    }

    private void fillSetPropositionInfo(Element propositionElement, SetProposition proposition) {
        String id = parseText(propositionElement, "id");
        proposition.setID(id);

        String name = parseText(propositionElement, "name");
        proposition.setName(name);

        String comment = parseText(propositionElement, "comment");
        proposition.setComment(comment);
    }

    private void fillGlobals(Document doc, Base base) {

        Element globals = (Element) doc.getElementsByTagName("global").item(0);

        Globals baseGlobals = base.getGlobals();

        String text;

        text = parseText(globals, "description");
        if (text != null) {
            baseGlobals.setDescription(text);
        }

        text = parseText(globals, "expert");
        if (text != null) {
            baseGlobals.setExpert(text);
        }

        text = parseText(globals, "knowledge_engineer");
        if (text != null) {
            baseGlobals.setKnowledgeEngineer(text);
        }

        text = parseText(globals, "date");
        baseGlobals.setDateFromXMLRepresentation(text);

        try {
            text = parseText(globals, "inference_mechanism");
            InferenceMechanism im = InferenceMechanism.valueOf(text);
            baseGlobals.setInferenceMechanism(im);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        }

        text = parseText(globals, "weight_range");
        try {
            int weightRange = Integer.parseInt(text);
            baseGlobals.setWeightRange(weightRange);
        } catch (NumberFormatException ex) {
            //do nothing, it might be wrong
        }

        try {
            text = parseText(globals, "default_weight");
            DefaultWeight dw = DefaultWeight.valueOf(text);
            baseGlobals.setDefaultWeight(dw);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        }

        try {
            text = parseText(globals, "global_priority");
            GlobalPriority gp = GlobalPriority.valueOf(text);
            baseGlobals.setGlobalPriority(gp);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        }

        try {
            text = parseText(globals, "context_global_threshold");

            NumberFormat format = NumberFormat.getInstance(new Locale("cs", "CZ"));
            Number number = format.parse(text);
            double cgt = number.doubleValue();

            baseGlobals.setContextGlobalThreshold(cgt);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        } catch (ParseException ex) {
            // Also do nothing, will not be used
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            text = parseText(globals, "condition_global_threshold");

            NumberFormat format = NumberFormat.getInstance(new Locale("cs", "CZ"));
            Number number = format.parse(text);
            double cgt = number.doubleValue();

            baseGlobals.setConditionGlobalThreshold(cgt);
        } catch (IllegalArgumentException ex) {
            // Do nothing, can be wrong
        } catch (ParseException ex) {
            // Also do nothing, will not be used
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String parseText(Element target, String name) {

        NodeList globalItem = target.getElementsByTagName(name);
        String text = "";
        if (globalItem.getLength() != 0) {
            text = globalItem
                    .item(0)
                    .getTextContent();
        }
        return text;
    }

}

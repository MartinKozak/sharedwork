package cz.vse.webz.utility;

import cz.vse.webz.backEnd.Base;
import cz.vse.webz.backEnd.Globals;
import cz.vse.webz.backEnd.attributesPropositions.AbstractAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericAttribute;
import cz.vse.webz.backEnd.attributesPropositions.NumericProposition;
import cz.vse.webz.backEnd.attributesPropositions.SetAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SetProposition;
import cz.vse.webz.backEnd.attributesPropositions.SingleAttribute;
import cz.vse.webz.backEnd.attributesPropositions.SingleProposition;
import cz.vse.webz.backEnd.connectionLinks.conclusion.Conclusion;
import cz.vse.webz.backEnd.connectionLinks.conclusion.IConclusionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.Condition;
import cz.vse.webz.backEnd.connectionLinks.condition.IConditionMaster;
import cz.vse.webz.backEnd.connectionLinks.condition.IConjunctionMaster;
import cz.vse.webz.backEnd.enums.RuleType;
import cz.vse.webz.backEnd.rules.AbstractRule;
import cz.vse.webz.backEnd.rules.AprioriRule;
import cz.vse.webz.backEnd.rules.CompleteRule;
import cz.vse.webz.backEnd.enums.AttributeType;
import cz.vse.webz.backEnd.enums.Scope;
import cz.vse.webz.backEnd.rules.Conjunction;
import java.io.StringWriter;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author Martin Kozákk
 */
public class XmlGenerator {

    /**
     * *
     * Only public method used to generate the XML
     *
     * @param base
     * @return
     */
    public String generateXML(Base base) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document xmlDocument = documentBuilder.newDocument();

            fillXMLfromBase(xmlDocument, base);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            transformer.setOutputProperty(OutputKeys.ENCODING, "Windows-1252");

            transformer.setOutputProperty(OutputKeys.METHOD, "html");
            xmlDocument.setXmlStandalone(true);

            DOMSource source = new DOMSource(xmlDocument);

            StringWriter writer = new StringWriter();

            StreamResult result = new StreamResult(writer);

            transformer.transform(source, result);

            String output = writer.getBuffer().toString();

            return output;

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
        //this line is accessed only in some sort of error
        return "Error parsing XML";
    }

    /**
     * Fills the XML document with elements acc ording to the base. This method
     * uses separate method for each part.
     *
     * @param xmlDocument document, taht is to be filled
     * @param base the source of elements
     */
    private void fillXMLfromBase(Document xmlDocument, Base base) {

        // root elements
        Element baseElement = xmlDocument.createElement("base");
        xmlDocument.appendChild(baseElement);

        //baseElement.appendChild(xmlDocument.createElement("<?xml version=\"1.0\" encoding=\"windows-1250\"?>"));
        baseElement.appendChild(xmlDocument.createComment("******************** Global properties ********************"));
        baseElement.appendChild(generateGlobal(base, xmlDocument));

        baseElement.appendChild(xmlDocument.createComment("******************** Attributes and propositions ********************"));
        baseElement.appendChild(generateAtributes(base, xmlDocument));

        baseElement.appendChild(xmlDocument.createComment("******************** Contexts ********************"));
        baseElement.appendChild(generateContexts(base, xmlDocument));

        baseElement.appendChild(xmlDocument.createComment("******************** Rules ********************"));
        baseElement.appendChild(generateRules(base, xmlDocument));

        baseElement.appendChild(xmlDocument.createComment("******************** Integrity constraints ********************"));
        baseElement.appendChild(generateIntegrityConstrains(base, xmlDocument));
    }

    private Element generateGlobal(Base base, Document XML) {
        Element global = XML.createElement("global");

        Globals globals = base.getGlobals();

        Element description = XML.createElement("description");
        description.appendChild(XML.createTextNode(globals.getDescription()));
        global.appendChild(description);

        Element expert = XML.createElement("expert");
        expert.appendChild(XML.createTextNode(globals.getExpert()));
        global.appendChild(expert);

        Element knowledgeEngineer = XML.createElement("knowledge_engineer");
        knowledgeEngineer.appendChild(XML.createTextNode(globals.getKnowledgeEngineer()));
        global.appendChild(knowledgeEngineer);

        Element date = XML.createElement("date");
        date.appendChild(XML.createTextNode(globals.getDateRepresentation()));
        global.appendChild(date);

        Element inferenceMechanism = XML.createElement("inference_mechanism");
        inferenceMechanism.appendChild(XML.createTextNode(globals.getInferenceMechanism().toString()));
        global.appendChild(inferenceMechanism);

        Element weightRange = XML.createElement("weight_range");
        weightRange.appendChild(XML.createTextNode("" + globals.getWeightRange()));
        global.appendChild(weightRange);

        Element defaultWeight = XML.createElement("default_weight");
        defaultWeight.appendChild(XML.createTextNode(globals.getDefaultWeight().toString()));
        global.appendChild(defaultWeight);

        Element globalPriority = XML.createElement("global_priority");
        globalPriority.appendChild(XML.createTextNode(globals.getGlobalPriority().toString()));
        global.appendChild(globalPriority);

        Element contextGlobalThreshold = XML.createElement("context_global_threshold");
        contextGlobalThreshold.appendChild(XML.createTextNode(globals.getContextGTRepresentation()));
        global.appendChild(contextGlobalThreshold);

        Element conditionGlobalThreshold = XML.createElement("condition_global_threshold");
        conditionGlobalThreshold.appendChild(XML.createTextNode(globals.getConditionGTRepresentation()));
        global.appendChild(conditionGlobalThreshold);

        return global;
    }

    private Node generateAtributes(Base base, Document XML) {

        Element atributes = XML.createElement("attributes");

        for (AbstractAttribute atribute : base.getAtributes()) {
            Element atributeRoot = XML.createElement("attribute");
            atributes.appendChild(atributeRoot);

            Element id = XML.createElement("id");
            id.appendChild(XML.createTextNode(atribute.getID()));
            atributeRoot.appendChild(id);

            Element name = XML.createElement("name");
            name.appendChild(XML.createTextNode(atribute.getName()));
            atributeRoot.appendChild(name);

            Element type = XML.createElement("type");
            if (atribute.getType() == AttributeType.set) {
                type.appendChild(XML.createTextNode("multiple"));
            } else {
                type.appendChild(XML.createTextNode(atribute.getType().toString()));
            }
            atributeRoot.appendChild(type);

            Element comment = XML.createElement("comment");
            comment.appendChild(XML.createTextNode(atribute.getComment()));

            if (atribute.getScope() == (Scope.environmental)) {
                Element scope = XML.createElement("scope");
                scope.appendChild(XML.createTextNode("environment"));
                atributeRoot.appendChild(scope);
            }

            switch (atribute.getType()) {
                case binary:
                    atributeRoot.appendChild(comment);
                    break;
                case set: {
                    SetAttribute setAtribute = (SetAttribute) atribute;

                    atributeRoot.appendChild(comment);

                    Element propositions = XML.createElement("propositions");
                    atributeRoot.appendChild(propositions);
                    for (SetProposition proposition : setAtribute.getPropositionSet()) {
                        Element propositionRoot = XML.createElement("proposition");
                        propositions.appendChild(propositionRoot);

                        Element idProp = XML.createElement("id");
                        idProp.appendChild(XML.createTextNode(proposition.getID()));
                        propositionRoot.appendChild(idProp);

                        Element nameProp = XML.createElement("name");
                        nameProp.appendChild(XML.createTextNode(proposition.getName()));
                        propositionRoot.appendChild(nameProp);

                        Element commentProp = XML.createElement("comment");
                        commentProp.appendChild(XML.createTextNode(proposition.getComment()));
                        propositionRoot.appendChild(commentProp);

                    }
                    break;
                }
                case single: {
                    SingleAttribute singleAtribute = (SingleAttribute) atribute;

                    atributeRoot.appendChild(comment);

                    Element propositions = XML.createElement("propositions");
                    atributeRoot.appendChild(propositions);
                    for (SingleProposition proposition : singleAtribute.getPropositionSet()) {
                        Element propositionRoot = XML.createElement("proposition");
                        propositions.appendChild(propositionRoot);

                        Element idProp = XML.createElement("id");
                        idProp.appendChild(XML.createTextNode(proposition.getID()));
                        propositionRoot.appendChild(idProp);

                        Element nameProp = XML.createElement("name");
                        nameProp.appendChild(XML.createTextNode(proposition.getName()));
                        propositionRoot.appendChild(nameProp);

                        Element commentProp = XML.createElement("comment");
                        commentProp.appendChild(XML.createTextNode(proposition.getComment()));
                        propositionRoot.appendChild(commentProp);
                    }
                    break;
                }

                case numeric: {
                    NumericAttribute numericAtribute = (NumericAttribute) atribute;

                    Element legalValues = XML.createElement("legal_values");

                    Element lowerBound = XML.createElement("lower_bound");
                    lowerBound.appendChild(XML.createTextNode(numericAtribute.getLowerGTRepresentation()));
                    Element upperBound = XML.createElement("upper_bound");
                    upperBound.appendChild(XML.createTextNode(numericAtribute.getUpperGTRepresentation()));
                    legalValues.appendChild(lowerBound);
                    legalValues.appendChild(upperBound);
                    atributeRoot.appendChild(legalValues);

                    atributeRoot.appendChild(comment);

                    Element propositions = XML.createElement("propositions");
                    atributeRoot.appendChild(propositions);
                    for (NumericProposition proposition : numericAtribute.getPropositionSet()) {
                        Element propositionRoot = XML.createElement("proposition");
                        propositions.appendChild(propositionRoot);

                        Element idProp = XML.createElement("id");
                        idProp.appendChild(XML.createTextNode(proposition.getID()));
                        propositionRoot.appendChild(idProp);

                        Element nameProp = XML.createElement("name");
                        nameProp.appendChild(XML.createTextNode(proposition.getName()));
                        propositionRoot.appendChild(nameProp);

                        //weight function <>8,000</<crisp_lower_bound>6,000</crisp_lower_bound>>
                        Element weightFunction = XML.createElement("weight_function");

                        Element fuzzyLowerBound = XML.createElement("fuzzy_lower_bound");
                        fuzzyLowerBound.appendChild(XML.createTextNode(proposition.getLowerFuzzyGTRepresentation()));

                        Element crispLowerBound = XML.createElement("crisp_lower_bound");
                        crispLowerBound.appendChild(XML.createTextNode(proposition.getLowerCrispGTRepresentation()));

                        Element crispUpperBound = XML.createElement("crisp_upper_bound");
                        crispUpperBound.appendChild(XML.createTextNode(proposition.getUpperCrispGTRepresentation()));

                        Element fuzzyUpperBound = XML.createElement("fuzzy_upper_bound");
                        fuzzyUpperBound.appendChild(XML.createTextNode(proposition.getUpperFuzzyGTRepresentation()));

                        weightFunction.appendChild(fuzzyLowerBound);
                        weightFunction.appendChild(crispLowerBound);
                        weightFunction.appendChild(crispUpperBound);
                        weightFunction.appendChild(fuzzyUpperBound);
                        propositionRoot.appendChild(weightFunction);

                        Element commentProp = XML.createElement("comment");
                        commentProp.appendChild(XML.createTextNode(proposition.getComment()));
                        propositionRoot.appendChild(commentProp);
                    }
                    break;
                }
            }

        }
        return atributes;
    }

    private Element generateContexts(Base base, Document XML) {
        Element contexts = XML.createElement("contexts");
        return contexts;
    }

    private Element generateRules(Base base, Document XML) {
        Element rules = XML.createElement("rules");

        rules.appendChild(XML.createComment("******************** Apriori rules ********************"));
        Element aprioryRules = XML.createElement("apriori_rules");
        rules.appendChild(aprioryRules);

        rules.appendChild(XML.createComment("******************** Logical rules ********************"));
        Element logicalRules = XML.createElement("logical_rules");
        rules.appendChild(logicalRules);

        rules.appendChild(XML.createComment("******************** Compositional rules ********************"));
        Element compositionalRules = XML.createElement("compositional_rules");
        rules.appendChild(compositionalRules);

        //for each rule, add into proper category
        for (AbstractRule rule : base.getRules()) {

            switch (rule.getType()) {
                case apriori:
                    AprioriRule aprioryRule = (AprioriRule) rule;

                    Element aprioryRuleRoot = XML.createElement("apriori_rule");
                    aprioryRules.appendChild(aprioryRuleRoot);

                    Element aprioryId = XML.createElement("id");
                    aprioryId.appendChild(XML.createTextNode(aprioryRule.getID()));
                    aprioryRuleRoot.appendChild(aprioryId);

                    Element aprioryConclusion = generateConclusions(aprioryRule, XML, true);
                    aprioryRuleRoot.appendChild(aprioryConclusion);

                    Element aprioryComment = XML.createElement("comment");
                    aprioryComment.appendChild(XML.createTextNode(aprioryRule.getComment()));
                    aprioryRuleRoot.appendChild(aprioryComment);

                    //aprioryRules.appendChild(XML.createTextNode(aprioryRule.getType().toString()));
                    if (aprioryRule.isNegativeRule()) {
                        Element negAprioryRuleRoot = XML.createElement("apriori_rule");
                        aprioryRules.appendChild(negAprioryRuleRoot);

                        Element negAprioryId = XML.createElement("id");
                        negAprioryId.appendChild(XML.createTextNode(aprioryRule.getID() + " negate"));
                        negAprioryRuleRoot.appendChild(negAprioryId);

                        Element negAprioryConclusion = generateConclusions(aprioryRule, XML, false);
                        negAprioryRuleRoot.appendChild(negAprioryConclusion);

                        Element negAprioryComment = XML.createElement("comment");
                        negAprioryComment.appendChild(XML.createTextNode(aprioryRule.getComment()));
                        negAprioryRuleRoot.appendChild(negAprioryComment);

                        //aprioryRules.appendChild(XML.createTextNode(aprioryRule.getType().toString()));
                    }

                    break;

                case logical:
                    CompleteRule logicalRule = (CompleteRule) rule;
                    Element logicalRuleRoot = XML.createElement("logical_rule");
                    logicalRules.appendChild(logicalRuleRoot);

                    Element logicalId = XML.createElement("id");
                    logicalId.appendChild(XML.createTextNode(logicalRule.getID()));
                    logicalRuleRoot.appendChild(logicalId);

                    Element logicalThreshhold = XML.createElement("condition_threshold");
                    logicalThreshhold.appendChild(XML.createTextNode(String.format(new Locale("cs", "CZ"), "%.3f", logicalRule.getConditionThreshold())));
                    logicalRuleRoot.appendChild(logicalThreshhold);

                    Element logicalCondition = generateConditions(logicalRule, logicalRule, XML, true);
                    logicalRuleRoot.appendChild(logicalCondition);

                    Element logicalConclusion = generateConclusions(logicalRule, XML, true);
                    logicalRuleRoot.appendChild(logicalConclusion);

                    Element logicalComment = XML.createElement("comment");
                    logicalComment.appendChild(XML.createTextNode(logicalRule.getComment()));
                    logicalRuleRoot.appendChild(logicalComment);

                    if (logicalRule.isNegativeRule()) {
                        Element negLogicalRuleRoot = XML.createElement("logical_rule");
                        logicalRules.appendChild(negLogicalRuleRoot);

                        Element neglogicalId = XML.createElement("id");
                        neglogicalId.appendChild(XML.createTextNode(logicalRule.getID() + " negate"));
                        negLogicalRuleRoot.appendChild(neglogicalId);

                        Element neglogicalThreshhold = XML.createElement("condition_threshold");
                        neglogicalThreshhold.appendChild(XML.createTextNode(String.format(new Locale("cs", "CZ"), "%.3f", logicalRule.getConditionThreshold())));
                        negLogicalRuleRoot.appendChild(neglogicalThreshhold);

                        Element neglogicalCondition = generateConditions(logicalRule, logicalRule, XML, false);
                        negLogicalRuleRoot.appendChild(neglogicalCondition);

                        Element neglogicalConclusion = generateConclusions(logicalRule, XML, false);
                        negLogicalRuleRoot.appendChild(neglogicalConclusion);

                        Element neglogicalComment = XML.createElement("comment");
                        neglogicalComment.appendChild(XML.createTextNode(logicalRule.getComment()));
                        negLogicalRuleRoot.appendChild(neglogicalComment);
                    }

                    break;

                case compositional:

                    CompleteRule completeRule = (CompleteRule) rule;

                    Element ruleRoot = XML.createElement("compositional_rule");
                    compositionalRules.appendChild(ruleRoot);

                    Element id = XML.createElement("id");
                    id.appendChild(XML.createTextNode(completeRule.getID()));
                    ruleRoot.appendChild(id);

                    Element condition = generateConditions(completeRule, completeRule, XML, true);
                    ruleRoot.appendChild(condition);

                    Element conclusion = generateConclusions(completeRule, XML, true);
                    ruleRoot.appendChild(conclusion);

                    Element comment = XML.createElement("comment");
                    comment.appendChild(XML.createTextNode(completeRule.getComment()));
                    ruleRoot.appendChild(comment);

                    if (rule.isNegativeRule()) {
                        Element negRuleRoot = XML.createElement("compositional_rule");
                        compositionalRules.appendChild(negRuleRoot);

                        Element negid = XML.createElement("id");
                        negid.appendChild(XML.createTextNode(completeRule.getID() + " negate"));
                        negRuleRoot.appendChild(negid);

                        Element negcondition = generateConditions(completeRule, completeRule, XML, false);
                        negRuleRoot.appendChild(negcondition);

                        Element negconclusion = generateConclusions(completeRule, XML, false);
                        negRuleRoot.appendChild(negconclusion);

                        Element negcomment = XML.createElement("comment");
                        negcomment.appendChild(XML.createTextNode(completeRule.getComment()));
                        negRuleRoot.appendChild(negcomment);
                    }
            }

        }

        return rules;
    }

    private Element generateConditions(IConditionMaster ruleCondition, IConjunctionMaster ruleConjuction, Document XML, boolean positivity) {
        Element condition = XML.createElement("condition");

        for (Condition literal : ruleCondition.getAllConditions()) {
            Element conjuctionRoot = XML.createElement("conjunction");
            condition.appendChild(conjuctionRoot);

            Element literalRoot = XML.createElement("literal");
            conjuctionRoot.appendChild(literalRoot);

            Element literalID = XML.createElement("id_attribute");
            literalRoot.appendChild(literalID);

            if (literal.getElement() instanceof NumericProposition) { // object is proposition
                NumericProposition proposition = (NumericProposition) literal.getElement();

                literalID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));
                
                Element literalPropositionID = XML.createElement("id_proposition");
                literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                literalRoot.appendChild(literalPropositionID);

            } else if (literal.getElement() instanceof SingleProposition) { // object is proposition
                SingleProposition proposition = (SingleProposition) literal.getElement();

                literalID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));
                
                Element literalPropositionID = XML.createElement("id_proposition");
                literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                literalRoot.appendChild(literalPropositionID);

            } else if (literal.getElement() instanceof SetProposition) {
                SetProposition proposition = (SetProposition) literal.getElement();

                literalID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));

                Element literalPropositionID = XML.createElement("id_proposition");
                literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                literalRoot.appendChild(literalPropositionID);

            } else { //Object is Binary atribute
                literalID.appendChild(XML.createTextNode(literal.getElement().getID()));
            }

            Element literalNegativity = XML.createElement("negation");
            literalNegativity.appendChild(XML.createTextNode(literal.getNegativityCode(positivity)));
            literalRoot.appendChild(literalNegativity);
        }

        for (Conjunction conjuction : ruleConjuction.getAllConjunctions()) {
            Element conjuctionRoot = XML.createElement("conjunction");
            if (!conjuction.getAllConditions().isEmpty()) {
                condition.appendChild(conjuctionRoot);
            }
            for (Condition literal : conjuction.getAllConditions()) {
                Element literalRoot = XML.createElement("literal");
                conjuctionRoot.appendChild(literalRoot);

                Element literalID = XML.createElement("id_attribute");
                literalRoot.appendChild(literalID);

                if (literal.getElement() instanceof NumericProposition) { // object is proposition
                    NumericProposition proposition = (NumericProposition) literal.getElement();

                    literalID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));
                    
                    Element literalPropositionID = XML.createElement("id_proposition");
                    literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                    literalRoot.appendChild(literalPropositionID);

                } else if (literal.getElement() instanceof SingleProposition) { // object is proposition
                    SingleProposition proposition = (SingleProposition) literal.getElement();

                    literalID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));
                    
                    Element literalPropositionID = XML.createElement("id_proposition");
                    literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                    literalRoot.appendChild(literalPropositionID);

                } else if (literal.getElement() instanceof SetProposition) {
                    SetProposition proposition = (SetProposition) literal.getElement();

                    literalID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));

                    Element literalPropositionID = XML.createElement("id_proposition");
                    literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                    literalRoot.appendChild(literalPropositionID);

                } else { //Object is Binary atribute
                    literalID.appendChild(XML.createTextNode(literal.getElement().getID()));
                }

                Element literalNegativity = XML.createElement("negation");
                literalNegativity.appendChild(XML.createTextNode(literal.getNegativityCode(positivity)));
                literalRoot.appendChild(literalNegativity);
            }
        }
        return condition;
    }

    private Element generateConclusions(IConclusionMaster rule, Document XML, boolean positivity) {
        Element conclusions = XML.createElement("conclusions");

        boolean showWeight = false;

        for (Conclusion conclusion : rule.getAllConclusions()) {
            Element conclusionRoot = XML.createElement("conclusion");
            conclusions.appendChild(conclusionRoot);

            Element conclusionID = XML.createElement("id_attribute");
            conclusionRoot.appendChild(conclusionID);

            if (conclusion.getElement() instanceof SetProposition) {
                SetProposition proposition = (SetProposition) conclusion.getElement();

                conclusionID.appendChild(XML.createTextNode(proposition.getParentAtribute().getID()));

                Element literalPropositionID = XML.createElement("id_proposition");
                literalPropositionID.appendChild(XML.createTextNode(proposition.getID()));
                conclusionRoot.appendChild(literalPropositionID);

            } else { //Object is Binary atribute
                conclusionID.appendChild(XML.createTextNode(conclusion.getElement().getID()));
            }

            Element conclusionNegativity = XML.createElement("negation");
            conclusionNegativity.appendChild(XML.createTextNode(conclusion.getNegativityCode(positivity)));
            conclusionRoot.appendChild(conclusionNegativity);

            //Sort out the logical rule
            if ((rule instanceof CompleteRule)) {
                CompleteRule completeRule = (CompleteRule) rule;
                if (completeRule.getType() == RuleType.logical) {
                    showWeight = false;
                } else {
                    showWeight = true;
                }
            } else {
                showWeight = true;
            }
            if (showWeight) {
                Element conclusionWeight = XML.createElement("weight");
                conclusionWeight.appendChild(XML.createTextNode(String.format(new Locale("cs", "CZ"), "%.3f", conclusion.getWeight())));
                conclusionRoot.appendChild(conclusionWeight);
            }
        }

        return conclusions;
    }

    private Element generateIntegrityConstrains(Base base, Document XML) {
        Element integrity_constraints = XML.createElement("integrity_constraints");
        return integrity_constraints;

    }

}

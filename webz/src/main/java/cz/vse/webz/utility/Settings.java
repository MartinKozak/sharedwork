/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.webz.utility;

/**
 *
 * @author Martin Kozák
 */
public class Settings {

    private int atributeLeft;
    private int atributeTop;

    private int ruleLeft;
    private int ruleTop;

    private double defaultWeight;

    private int condR;
    private int condG;
    private int condB;

    private int concR;
    private int concG;
    private int concB;

    private int relR;
    private int relG;
    private int relB;

    public Settings() {
        this.defaultWeight = 0.5;

        defaultPosition();
        defaultColor();
    }

    public void defaultPosition() {
        this.ruleTop = 50;
        this.ruleLeft = 650;
        this.atributeTop = 50;
        this.atributeLeft = 50;
    }

    public void defaultColor() {
        this.setCondR(255);
        this.setCondG(0);
        this.setCondB(0);

        this.setConcR(0);
        this.setConcG(0);
        this.setConcB(255);

        this.setRelR(0);
        this.setRelG(255);
        this.setRelB(0);
}

public Position getAtributePosition(){
        Position position = new Position(getAtributeLeft(), getAtributeTop());
        return position;
    }
    
    
    public Position getRulePosition(){
        Position position = new Position(getRuleLeft(), getRuleTop());
        return position;
    }

    
    
    public double getDefaultWeight(){
        return defaultWeight;
    }
    
    
    
    /*******Getters and Setters *******************/
    
    
    
    /**
     * @return the atributeLeft
     */
    public int getAtributeLeft() {
        return atributeLeft;
    }

    /**
     * @param atributeLeft the atributeLeft to set
     */
    public void setAtributeLeft(int atributeLeft) {
        this.atributeLeft = atributeLeft;
    }

    /**
     * @return the atributeTop
     */
    public int getAtributeTop() {
        return atributeTop;
    }

    /**
     * @param atributeTop the atributeTop to set
     */
    public void setAtributeTop(int atributeTop) {
        this.atributeTop = atributeTop;
    }

    /**
     * @return the ruleLeft
     */
    public int getRuleLeft() {
        return ruleLeft;
    }

    /**
     * @param ruleLeft the ruleLeft to set
     */
    public void setRuleLeft(int ruleLeft) {
        this.ruleLeft = ruleLeft;
    }

    /**
     * @return the ruleTop
     */
    public int getRuleTop() {
        return ruleTop;
    }

    /**
     * @param ruleTop the ruleTop to set
     */
    public void setRuleTop(int ruleTop) {
        this.ruleTop = ruleTop;
    

}

    /**
     * @return the condR
     */
    public int getCondR() {
        return condR;
    }

    /**
     * @param condR the condR to set
     */
    public void setCondR(int condR) {
        this.condR = condR;
    }

    /**
     * @return the condG
     */
    public int getCondG() {
        return condG;
    }

    /**
     * @param condG the condG to set
     */
    public void setCondG(int condG) {
        this.condG = condG;
    }

    /**
     * @return the condB
     */
    public int getCondB() {
        return condB;
    }

    /**
     * @param condB the condB to set
     */
    public void setCondB(int condB) {
        this.condB = condB;
    }

    /**
     * @return the concR
     */
    public int getConcR() {
        return concR;
    }

    /**
     * @param concR the concR to set
     */
    public void setConcR(int concR) {
        this.concR = concR;
    }

    /**
     * @return the concG
     */
    public int getConcG() {
        return concG;
    }

    /**
     * @param concG the concG to set
     */
    public void setConcG(int concG) {
        this.concG = concG;
    }

    /**
     * @return the concB
     */
    public int getConcB() {
        return concB;
    }

    /**
     * @param concB the concB to set
     */
    public void setConcB(int concB) {
        this.concB = concB;
    }

    /**
     * @return the relR
     */
    public int getRelR() {
        return relR;
    }

    /**
     * @param relR the relR to set
     */
    public void setRelR(int relR) {
        this.relR = relR;
    }

    /**
     * @return the relG
     */
    public int getRelG() {
        return relG;
    }

    /**
     * @param relG the relG to set
     */
    public void setRelG(int relG) {
        this.relG = relG;
    }

    /**
     * @return the relB
     */
    public int getRelB() {
        return relB;
    }

    /**
     * @param relB the relB to set
     */
    public void setRelB(int relB) {
        this.relB = relB;
    }
    
    
    /***
     * Simple class representing 2D position
     */
    public class Position {

    public int left;
    public int top;

    public Position(int left, int top) {
        this.left = left;
        this.top = top;
    }
    
    public class Color {
        public int R;
        public int G;
        public int B;
        
        public Color(int R, int G, int B){
            this.R = R;
            this.G = G;
            this.B = B;
        }
        
    }
    
}
}

package cz.vse.webz.utility;

import com.vaadin.data.Container.Hierarchical;
import com.vaadin.data.util.BeanItemContainer;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/** Extension of BeanItemContainer that implements Hierarchical */
public class HierarchicalBeanItemContainer<T>
       extends BeanItemContainer<T> implements Hierarchical{
    // The contained bean type uses this property to store
    // the parent relationship.
   /**
     * Set of IDs of those contained Items that can't have children.
     */
    private final HashSet<Object> noChildrenAllowed = new HashSet<Object>();

    /**
     * Mapping from Item ID to parent Item ID.
     */
    private final HashMap<Object, Object> parent = new HashMap<Object, Object>();

    /**
     * Mapping from Item ID to parent Item ID for items included in the filtered
     * container.
     */
    private HashMap<Object, Object> filteredParent = null;

    /**
     * Mapping from Item ID to a list of child IDs.
     */
    private final HashMap<Object, LinkedList<Object>> children = new HashMap<Object, LinkedList<Object>>();

    /**
     * Mapping from Item ID to a list of child IDs when filtered
     */
    private HashMap<Object, LinkedList<Object>> filteredChildren = null;

    /**
     * List that contains all root elements of the container.
     */
    private final LinkedList<Object> roots = new LinkedList<Object>();

    /**
     * List that contains all filtered root elements of the container.
     */
    private LinkedList<Object> filteredRoots = null;

    /**
     * Determines how filtering of the container is done.
     */
    private boolean includeParentsWhenFiltering = true;

    /**
     * Counts how many nested contents change disable calls are in progress.
     * 
     * Pending events are only fired when the counter reaches zero again.
     */
    private int contentChangedEventsDisabledCount = 0;

    private boolean contentsChangedEventPending;

    
    public HierarchicalBeanItemContainer(Class<T> type) {
        super(type);
    }

    /*
     * Can the specified Item have any children? Don't add a JavaDoc comment
     * here, we use the default documentation from implemented interface.
     */
    @Override
    public boolean areChildrenAllowed(Object itemId) {
        if (noChildrenAllowed.contains(itemId)) {
            return false;
        }
        return containsId(itemId);
    }

    /*
     * Gets the IDs of the children of the specified Item. Don't add a JavaDoc
     * comment here, we use the default documentation from implemented
     * interface.
     */
    @Override
    public Collection<?> getChildren(Object itemId) {
        LinkedList<Object> c;

        if (filteredChildren != null) {
            c = filteredChildren.get(itemId);
        } else {
            c = children.get(itemId);
        }

        if (c == null) {
            return null;
        }
        return Collections.unmodifiableCollection(c);
    }

    /*
     * Gets the ID of the parent of the specified Item. Don't add a JavaDoc
     * comment here, we use the default documentation from implemented
     * interface.
     */
    @Override
    public Object getParent(Object itemId) {
        if (filteredParent != null) {
            return filteredParent.get(itemId);
        }
        return parent.get(itemId);
    }

    /*
     * Is the Item corresponding to the given ID a leaf node? Don't add a
     * JavaDoc comment here, we use the default documentation from implemented
     * interface.
     */
    @Override
    public boolean hasChildren(Object itemId) {
        if (filteredChildren != null) {
            return filteredChildren.containsKey(itemId);
        } else {
            return children.containsKey(itemId);
        }
    }

    /*
     * Is the Item corresponding to the given ID a root node? Don't add a
     * JavaDoc comment here, we use the default documentation from implemented
     * interface.
     */
    @Override
    public boolean isRoot(Object itemId) {
        // If the container is filtered the itemId must be among filteredRoots
        // to be a root.
        if (filteredRoots != null) {
            if (!filteredRoots.contains(itemId)) {
                return false;
            }
        } else {
            // Container is not filtered
            if (parent.containsKey(itemId)) {
                return false;
            }
        }

        return containsId(itemId);
    }

    /*
     * Gets the IDs of the root elements in the container. Don't add a JavaDoc
     * comment here, we use the default documentation from implemented
     * interface.
     */
    @Override
    public Collection<?> rootItemIds() {
        if (filteredRoots != null) {
            return Collections.unmodifiableCollection(filteredRoots);
        } else {
            return Collections.unmodifiableCollection(roots);
        }
    }

    /**
     * <p>
     * Sets the given Item's capability to have children. If the Item identified
     * with the itemId already has children and the areChildrenAllowed is false
     * this method fails and <code>false</code> is returned; the children must
     * be first explicitly removed with
     * {@link #setParent(Object itemId, Object newParentId)} or
     * {@link com.vaadin.data.Container#removeItem(Object itemId)}.
     * </p>
     * 
     * @param itemId
     *            the ID of the Item in the container whose child capability is
     *            to be set.
     * @param childrenAllowed
     *            the boolean value specifying if the Item can have children or
     *            not.
     * @return <code>true</code> if the operation succeeded, <code>false</code>
     *         if not
     */
    @Override
    public boolean setChildrenAllowed(Object itemId, boolean childrenAllowed) {

        // Checks that the item is in the container
        if (!containsId(itemId)) {
            return false;
        }

        // Updates status
        if (childrenAllowed) {
            noChildrenAllowed.remove(itemId);
        } else {
            noChildrenAllowed.add(itemId);
        }

        return true;
    }

    /**
     * <p>
     * Sets the parent of an Item. The new parent item must exist and be able to
     * have children. (<code>canHaveChildren(newParentId) == true</code>). It is
     * also possible to detach a node from the hierarchy (and thus make it root)
     * by setting the parent <code>null</code>.
     * </p>
     * 
     * @param itemId
     *            the ID of the item to be set as the child of the Item
     *            identified with newParentId.
     * @param newParentId
     *            the ID of the Item that's to be the new parent of the Item
     *            identified with itemId.
     * @return <code>true</code> if the operation succeeded, <code>false</code>
     *         if not
     */
    @Override
    public boolean setParent(Object itemId, Object newParentId) {

        // Checks that the item is in the container
        if (!containsId(itemId)) {
            return false;
        }

        // Gets the old parent
        final Object oldParentId = parent.get(itemId);

        // Checks if no change is necessary
        if ((newParentId == null && oldParentId == null)
                || ((newParentId != null) && newParentId.equals(oldParentId))) {
            return true;
        }

        // Making root?
        if (newParentId == null) {
            // The itemId should become a root so we need to
            // - Remove it from the old parent's children list
            // - Add it as a root
            // - Remove it from the item -> parent list (parent is null for
            // roots)

            // Removes from old parents children list
            final LinkedList<Object> l = children.get(oldParentId);
            if (l != null) {
                l.remove(itemId);
                if (l.isEmpty()) {
                    children.remove(oldParentId);
                }

            }

            // Add to be a root
            roots.add(itemId);

            // Updates parent
            parent.remove(itemId);

            if (hasFilters()) {
                // Refilter the container if setParent is called when filters
                // are applied. Changing parent can change what is included in
                // the filtered version (if includeParentsWhenFiltering==true).
                doFilterContainer(hasFilters());
            }

            fireItemSetChange();

            return true;
        }

        // We get here when the item should not become a root and we need to
        // - Verify the new parent exists and can have children
        // - Check that the new parent is not a child of the selected itemId
        // - Updated the item -> parent mapping to point to the new parent
        // - Remove the item from the roots list if it was a root
        // - Remove the item from the old parent's children list if it was not a
        // root

        // Checks that the new parent exists in container and can have
        // children
        if (!containsId(newParentId) || noChildrenAllowed.contains(newParentId)) {
            return false;
        }

        // Checks that setting parent doesn't result to a loop
        Object o = newParentId;
        while (o != null && !o.equals(itemId)) {
            o = parent.get(o);
        }
        if (o != null) {
            return false;
        }

        // Updates parent
        parent.put(itemId, newParentId);
        LinkedList<Object> pcl = children.get(newParentId);
        if (pcl == null) {
            // Create an empty list for holding children if one were not
            // previously created
            pcl = new LinkedList<Object>();
            children.put(newParentId, pcl);
        }
        pcl.add(itemId);

        // Removes from old parent or root
        if (oldParentId == null) {
            roots.remove(itemId);
        } else {
            final LinkedList<Object> l = children.get(oldParentId);
            if (l != null) {
                l.remove(itemId);
                if (l.isEmpty()) {
                    children.remove(oldParentId);
                }
            }
        }

        if (hasFilters()) {
            // Refilter the container if setParent is called when filters
            // are applied. Changing parent can change what is included in
            // the filtered version (if includeParentsWhenFiltering==true).
            doFilterContainer(hasFilters());
        }

        fireItemSetChange();

        return true;
    }
    
    private boolean hasFilters() {
        return (filteredRoots != null);
    }
}
 package cz.vse.webz;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import cz.vse.webz.frontEnd.AplicationLayout;
import static cz.vse.webz.frontEnd.texts.*;
import java.util.Locale;

/**
 * Main class for vaadin project
 * 
 * @author Martin Kozák
 */
@Theme("mytheme")
@Widgetset("cz.vse.webz.MyAppWidgetset")
public class MainUI extends UI {

    
    

    
    /**
     * Basic init method, starts first view.
     * 
     * @param vaadinRequest 
     */
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        
        this.setLocale(new Locale("cs", "CZ"));
        
        getPage().setTitle(gMainTitle);

        AplicationLayout applayout = new AplicationLayout();
        setContent(applayout);
        
    }
        
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MainUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }



}
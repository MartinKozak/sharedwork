package _1245_Tetroid;

import java.awt.Point;
import java.util.Collection;


/*******************************************************************************
 * Instance rozhraní {@code IArkMaster} slouží ke komunikaci se hrou a reakcím
 * arkanoidu na situace ve hře.
 *
 * @author    Tetroids
 */
public interface IArkMaster
{

/*******************************************************************************
 * Vrací pozici lodi.
 */
    public abstract Point getShipPosition();

/*******************************************************************************
 * Vrací délku lodi.
 */
    public abstract int getShipLength();

   

/*******************************************************************************
 * Vrací kolekci míčků.
 */
    public abstract Collection<IPositioned> getBallSet();

/*******************************************************************************
 * Vrací kolekci powerupů.
 */
    public abstract Collection<IPositioned> getPowerUpSet();



/*******************************************************************************
 * Vrací počet životů lodi.
 */
    public abstract int getLives();

    
/*******************************************************************************
 * Změní pozici lodi.
 */
    public abstract void moveShip(boolean direction);

    
/*******************************************************************************
 * Informace o úspěšném zničení kostky, je zde kvůli tvorbě powerupů
 */  
    public abstract void infoBrickDestroy(Point position);
    
}

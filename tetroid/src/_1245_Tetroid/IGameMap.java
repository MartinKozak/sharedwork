package _1245_Tetroid;

import java.awt.Point;

/**
 * Třída implementující {@code IGameMap} realizuje zobrazování mapy
 * a práci s mapou.
 *
 * @author Tetroids
 */
public interface IGameMap 
{
    /***************************************************************************
     * Zeptá se instance třídy implementující IGameMap, jestli je pozice volná.
     *
     * @param position dotazovaná pozice
     * 
     * @return true - pozice je volná, false - pozice není volná
     */
    public abstract boolean isFree(Point position);
    
    /***************************************************************************
     * Zeptá se instance třídy implementující IGameMap, z jaké stany
     * došlo k zásahu.
     *
     * @param position pozice zásahu
     * 
     * @return číslo dle strany zásahu
     */
    public abstract int getHitDirection(Point position);
    

    /***************************************************************************
     * Informuje instanci třídy implementující IGameMap, že došlo k pohybu
     * instance třídy implementující IPositioned.
     *
     * @param object objekt který se pohnul a je jej třeba překreslit
     * 
     */
    public abstract void infoMove(IPositioned object);
      
            
    /***************************************************************************
     * Informuje instanci třídy implementující IGameMap,
     * že došlo k dopadu tetris bloku.    
     *       
     */
    public abstract void infoImpact();  
    
    
    /***************************************************************************
     * Překrreslí blok tetrisu na nové pozice
     * 
     * @param brickOne pozice první cihly
     * @param brickTwo pozice druhé cihly
     * @param brickThree pozice třetí cihly
     * @param brickFour pozice čtvrté cihly
     */
    public abstract void refreshTetPosition(Point brickOne, Point brickTwo,
            Point brickThree, Point brickFour);
    
}

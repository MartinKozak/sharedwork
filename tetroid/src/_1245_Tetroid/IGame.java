package _1245_Tetroid;

import java.awt.Image;
import javax.swing.Icon;

/**
 * Třída implementující {@code IGame} se stará o hlavní chod hry.
 * 
 * @author Tetroids
 */
public interface IGame 
{

    /***************************************************************************
     * Spustí hru
     */
    public abstract void start();
    
    /***************************************************************************
     * Vypne hru
     */
    public abstract void stop();
    
    /***************************************************************************
     * Zjistí, zda hra probíhá.
     */
    public abstract boolean isAlive();
    
    /***************************************************************************
     * Vrátí obrázek mapy hry
     * 
     * @return mapa hry
     */
    public abstract Icon getGameMap();
    
    /***************************************************************************
     * Vrátí rychlost arkanoid části.
     * 
     * @return rychlost arkanoid části
     */
    public abstract int getArkSpeed();
    
    /***************************************************************************
     * Vrátí rychlost tetris části.
     * 
     * @return tetris části
     */
    public abstract int getTetSpeed();
    
    /***************************************************************************
     * Vrátí počet životů arkanoid části
     * 
     * @return počet životů arkanoid části 
     */
    public abstract int getArkLives();
    
    /***************************************************************************
     * Vrátí obrázek následujícího tvaru
     * 
     * @return obrázek následujícího tvaru tetrisu
     */
    public abstract Icon getNextShape();
    
    
    /***************************************************************************
     * Vrátí čas do dalšího posunu mapy.
     * 
     * @return čas do posunu mapy.
     */
    public abstract int getMapTimer();
    
    
    /***************************************************************************
     * Vrátí procento čistého území.
     * 
     * @return Procento čistého území
     */
    public abstract int getCleanPercent();
}

package _1245_Tetroid;

import java.awt.Image;
import javax.swing.Icon;

/**
 * Instance rozhraní {@code ITetMaster} slouží ke komunikaci se hrou a reakcím
 * tetrisu na situace ve hře.
 * 
 * @author Tetroids
 */
public interface ITetMaster 
{
    /**************************************************************************
     * Vrátí obrázek následujícího tvaru
     * 
     * @return obrázek následujícího tvaru
     */
    public abstract Icon getNextShape();
}

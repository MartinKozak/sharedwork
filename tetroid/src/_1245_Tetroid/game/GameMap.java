package _1245_Tetroid.game;

import _1245_Tetroid.IArkMaster;
import _1245_Tetroid.IGameMap;
import _1245_Tetroid.IGameTimer;
import _1245_Tetroid.IPositioned;
import _1245_Tetroid.IReactable;
import _1245_Tetroid.ITetMaster;
import _1245_Tetroid.ReactEvent;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.Icon;





/*******************************************************************************
 * Instance třídy představují herní mapu
 *
 * @author  Martin KOZÁK
 * @version 0.00 — mm/20yy
 */
public class GameMap implements IGameMap, IReactable
{
//== CONSTANT CLASS ATTRIBUTES =================================================

    private static final GameMap INSTANCE = new GameMap();        

//== VARIABLE CLASS ATTRIBUTES =================================================
    
    private final Game GAME = Game.getInstance();
    
    private /*final*/ IArkMaster arkMaster;

    private /*final*/ ITetMaster tetMaster;

    private /*final*/ IGameTimer gameTimer;
    
//== STATIC INITIALIZER (CLASS CONSTRUCTOR) ====================================
//== CONSTANT INSTANCE ATTRIBUTES ==============================================
//== VARIABLE INSTANCE ATTRIBUTES ==============================================
    
    private ArrayList<Brick> brickMap = new ArrayList<>(); //tohle chce jinak
            /* záměrem je vytvořit dvojrozměrné pole cihliček */
    
    private Canvas paintMap = new Canvas();
    
    
    
    private Collection<IPositioned> balls = new ArrayList<>();

    private Collection<IPositioned> powerUps = new ArrayList<>();
    
    
    private Point brickOnePosition;
    
    private Point brickTwoPosition;
    
    private Point brickThreePosition;
    
    private Point brickFourPosition;
    
    
//== CLASS GETTERS AND SETTERS =================================================
//== OTHER NON-PRIVATE CLASS METHODS ===========================================

//##############################################################################
//== CONSTUCTORS AND FACTORY METHODS ===========================================

    /***************************************************************************
     * Vrátí instanci herní mapy
     *
     * @return instance herní mapy
     */
    public static GameMap getInstance(){
        return INSTANCE;
    }

    /***************************************************************************
     * Privátní konstruktor zabraňující vytvoření instance
     */
    private GameMap() {/* ... */}



//== ABSTRACT METHODS ==========================================================
//== INSTANCE GETTERS AND SETTERS ==============================================

    /***************************************************************************
     * Zeptá se instance třídy implementující IGameMap, jestli je pozice volná.
     *
     * @param position dotazovaná pozice
     *
     * @return true - pozice je volná, false - pozice není volná
     */
    @Override
    public boolean isFree(Point position)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /***************************************************************************
     * Zeptá se instance třídy implementující IGameMap, z jaké stany
     * došlo k zásahu.
     *
     * @param position pozice zásahu
     *
     * @return číslo dle strany zásahu
     */
    @Override
    public int getHitDirection(Point position)
    {
        Brick target = new Brick(true, 1, Color.BLACK); //prozatím vytvářím kostku, později jí najde na mapě
        if (target.hitKill())
        {
            arkMaster.infoBrickDestroy(position); //v budoucnu to bude střed kostky
        }
            
        return 0; //prozatím toto, do budoucna bude vracet směrový int
    }


    /**************************************************************************
     * Vrátí obrázek mapy.
     *
     * @return obrázek mapy.
     *
     */
    Icon getMapImage()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    int getPercentage()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

//== OTHER NON-PRIVATE INSTANCE METHODS ========================================

    
    /***************************************************************************
     * 
     * @param reactEvent 
     */
    @Override
    public void react(ReactEvent reactEvent) {
        /*
         * if ReactEvent = mapEvent
         * {
         *      makeMove();
         * }
         */
    }
    
    /***************************************************************************
     * Informuje instanci třídy implementující IGameMap, že došlo k pohybu
     * instance třídy implementující IPositioned.
     *
     * @param object objekt který se pohnul a je jej třeba překreslit
     *
     */
    @Override
    public void infoMove(IPositioned object)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /***************************************************************************
     * Informuje instanci třídy implementující IGameMap,
     * že došlo k dopadu tetris bloku.
     *
     */
    @Override
    public void infoImpact()
    {
        /*
        Brick on(brickOnePosition).setSolid = true;
        Brick on(brickOnePosition).setEndurance = 1;
        ...
        
        
        */
    }


    /***************************************************************************
     * Překrreslí blok tetrisu na nové pozice
     *
     * @param brickOne pozice první cihly
     * @param brickTwo pozice druhé cihly
     * @param brickThree pozice třetí cihly
     * @param brickFour pozice čtvrté cihly
     */
    @Override
    public void refreshTetPosition(Point brickOne, Point brickTwo,
    Point brickThree, Point brickFour)
    {
        
        /*
        brickOnePosition   = brickOne;
        brickTwoPosition   = brickTwo;
        brickThreePosition = brickThree;
        brickFourPosition  = brickFour;          
         */
        
    }
    /***************************************************************************
     * Inicializace po spuštění
     */
    void inicialize()
    {
        RandomBuild.makeRandom();
        gameTimer.addReactant(this);
    }
    
//== PRIVATE AND AUXILIARY CLASS METHODS =======================================
//== PRIVATE AND AUXILIARY INSTANCE METHODS ====================================
//== EMBEDDED TYPES AND INNER CLASSES ==========================================
//== TESTING CLASSES AND METHODS ===============================================
}

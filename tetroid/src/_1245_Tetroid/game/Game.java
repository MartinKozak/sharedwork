/* The file is saved in UTF-8 codepage.
 * Check: «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package _1245_Tetroid.game;


import _1245_Tetroid.IArkMaster;
import _1245_Tetroid.IGame;
import _1245_Tetroid.IGameControl;
import _1245_Tetroid.IGameTimer;
import _1245_Tetroid.ITetMaster;
import java.awt.Image;
import javax.swing.Icon;


/*******************************************************************************
 * Hlavní třída hry.
 *
 * @author  Martin KOZÁK
 */
public class Game implements IGame
{
//== CONSTANT CLASS ATTRIBUTES =================================================

    private static final Game INSTANCE = new Game();
    
//== VARIABLE CLASS ATTRIBUTES =================================================
//== STATIC INITIALIZER (CLASS CONSTRUCTOR) ====================================
//== CONSTANT INSTANCE ATTRIBUTES ==============================================

    private final GameMap gameMap = GameMap.getInstance();

    private /*final*/ IArkMaster arkMaster;

    private /*final*/ ITetMaster tetMaster;

    private /*final*/ IGameTimer gameTimer;

    private /*final*/ IGameControl gameControl;
    
    //== VARIABLE INSTANCE ATTRIBUTES ==============================================

    private boolean alive;
   
//== CLASS GETTERS AND SETTERS =================================================
//== OTHER NON-PRIVATE CLASS METHODS ===========================================

//##############################################################################
//== CONSTUCTORS AND FACTORY METHODS ===========================================

    /***************************************************************************
     * Vrátí herní instanci
     *
     * @return jediná instance hry
     */
    public static Game getInstance(){
        return INSTANCE;
    }


    /***************************************************************************
     * Privátní konstruktor zabraňující vytvoření instance
     */
    private Game(){/* ... */}


//== ABSTRACT METHODS ==========================================================
//== INSTANCE GETTERS AND SETTERS ==============================================


    /***************************************************************************
     * Vrátí obrázek mapy hry
     *
     * @return mapa hry
     */
    @Override
    public Icon getGameMap() {
        return gameMap.getMapImage();
    }


    /***************************************************************************
     * Vrátí rychlost arkanoid části.
     *
     * @return rychlost arkanoid části
     */
    @Override
    public int getArkSpeed() {
        return gameTimer.getArkPeriod();
    }


    /***************************************************************************
     * Vrátí rychlost tetris části.
     *
     * @return tetris části
     */
    @Override
    public int getTetSpeed() {
        return gameTimer.getTetPeriod();
    }


    /***************************************************************************
     * Vrátí počet životů arkanoid části
     *
     * @return počet životů arkanoid části
     */
    @Override
    public int getArkLives() {
        return arkMaster.getLives();
    }


    /***************************************************************************
     * Vrátí obrázek následujícího tvaru
     *
     * @return obrázek následujícího tvaru tetrisu
     */
    @Override
    public Icon getNextShape() {
        return tetMaster.getNextShape();
    }


    /***************************************************************************
     * Vrátí čas do dalšího posunu mapy.
     *
     * @return čas do posunu mapy.
     */
    @Override
    public int getMapTimer() {
        return gameTimer.getMapPeriod();
    }
    
    
    /***************************************************************************
     * Vrátí procento čistého území.
     * 
     * @return Procento čistého území
     */
    @Override
    public int getCleanPercent()
    {
        return gameMap.getPercentage();
    }


    /***************************************************************************
     * Zjistí, zda hra probíhá.
     */
    @Override
    public boolean isAlive()
    {
       return alive; 
    }
    
    
//== OTHER NON-PRIVATE INSTANCE METHODS ========================================


    /***************************************************************************
     * Spustí hru
     */
    @Override
    public void start() 
    {
        gameMap.inicialize();
        alive = true;        
    }


    /***************************************************************************
     * Vypne hru
     */
    @Override
    public void stop() {
        alive = false;
    }


//== PRIVATE AND AUXILIARY CLASS METHODS =======================================
//== PRIVATE AND AUXILIARY INSTANCE METHODS ====================================
//== EMBEDDED TYPES AND INNER CLASSES ==========================================
//== TESTING CLASSES AND METHODS ===============================================
//
//    /*************************************************************************
//     * Testing method.
//     */
//    public static void test()
//    {
//        Game1 inst = new Game1();
//    }
//    /** @param args Command line arguments - not used. */
//    public static void main(String[] args)  {  test();  }
}

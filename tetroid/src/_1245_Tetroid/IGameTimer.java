package _1245_Tetroid;






/**
 *Třída implementující {@code IGameTimer} realizuje
 * zasílání zpráv v závislosti na tikání časovače.
 * 
 * @author Tetroids
 */
public interface IGameTimer
{
    /***************************************************************************
     * Přidá instanci třídy implementující IReactable jako posluchače třídy
     * implementující rozhraní IGameTimer.
     *
     */
    public abstract void addReactant(IReactable reactant);
    
    /***************************************************************************
     * Odebere instanci třídy implementující IReactable z posluchačů třídy
     * implementující rozhraní IGameTimer.
     *
     */
    public abstract void removeReactant(IReactable reactant);

    
    /***************************************************************************
     * Nastaví periodu pro Arkanoid.
     *
     */
    public abstract void setArkPeriod(int period);
    
    /***************************************************************************
     * Vratí velikost periody pro Arkanoid.
     *
     */
    public abstract int getArkPeriod();
    
    /***************************************************************************
     * Nastaví periodu pro Tetris.
     *
     */
    public abstract void setTetPeriod(int period);
    
    /***************************************************************************
     * Vratí velikost periody pro Tetris.
     *
     */
    public abstract int getTetPeriod();
    
    /***************************************************************************
     * Nastaví periodu pro Mapu.
     *
     */
    public abstract void setMapPeriod(int period);
    
    /***************************************************************************
     * Vratí velikost periody pro Mapu.
     *
     */
    public abstract int getMapPeriod();
}
